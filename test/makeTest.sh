#!/bin/bash
REGRESSION_FAILED=101
CGDL_COMPILE_FAILED=102
CPP_COMPILE_FAILED=103
YELLOW='\033[01;33m'
RED='\033[01;31m'
GREEN='\033[01;32m'
GRAY='\033[01;37m'
RESET='\033[00;00m'

if [[ $# -eq 0 ]]
then
	test_files=`find -name *.cgdl`
elif [[ $# -eq 1 ]]
then
	test_files=$1
fi

for file in $test_files
do
	### compile cgdl->cpp
	echo -n -e "$RESET"
	echo -n -e "$GRAY"
	echo "============================================="
	echo "compiling $file"
	echo "============================================="
	echo -n -e "$RESET"
	echo -n -e "$RED"
	../kernel/cgdlc $file 1>/dev/null
	echo -n -e "$RESET"
	if [[ $? -ne 0 ]]
	then
		echo -n -e "$RED"
		echo  -e "\tcgdl compile failed"
		echo -n -e "$RESET"
		exit $CGDL_COMPILE_FAILED
	else
		echo -n -e "$GREEN"
		echo -e "\tcgdl compile passed"
	fi
	### if prev .cppref exist, compare for regression
	if [[ -r "${file%.cgdl}.cppref" ]]
	then
		echo -n -e "$YELLOW"
		echo -e "\treference file ${file%.cgdl}.cppref exist"
		echo -n -e "$RESET"
		cmp ${file%.cgdl}.cpp ${file%.cgdl}.cppref &> /dev/null
		if [[ $? -ne 0 ]]
		then
			echo -n -e "$YELLOW"
			echo -e "\tregression failed"
			echo  -e "\tgenerated file ${file%.cgdl}.cpp differs from reference file"
			echo -n -e "$RESET"
			#exit $REGRESSION_FAILED
			read -p "press any key to continue" -n 1 -s
		else
			echo -n -e "$GREEN"
			echo -e "\tregression passed"
			echo -n -e "$RESET"
		fi
	fi
	# compile cpp file
	echo -e "\n"
	echo -e "\t-------------------------------------"
	echo -e "\tcompiling ${file%.cgdl}.cpp"
	echo -e "\t-------------------------------------"
	echo -n -e "$RED"
	g++ -Wall -g -O2 -I ../cpplib ${file%.cgdl}.cpp -o ${file%.cgdl} &> /dev/null
	echo -n -e "$RESET"
	if [[ $? -ne 0 ]]
	then
		echo -n -e "$RED"
		echo  -e "\tcpp compilation failed"
		echo -n -e "$RESET"
		exit $CPP_COMPILE_FAILED
	fi
	echo -n -e "$GREEN"
	echo  -e "\tcongratulation your game ${file%.cgdl} is ready  "
	echo -n -e "$RESET"
	### if .out file is there compare the result too
	if [[ -r "${file%.cgdl}.outref" ]]
	then
		echo -e "\t-------------------------------------"
		echo -e "\texecuting game ${file%.cgdl}"
		echo -e "\t-------------------------------------"
		eval ${file%.cgdl} > ${file%.cgdl}.out
		echo -ne "$YELLOW"
		echo -e "\tresult file ${file%.cgdl}.outref exist"
		echo -n -e "$RESET"
		cmp ${file%.cgdl}.outref ${file%.cgdl}.out &> /dev/null
		if [[ $? -ne 0 ]]
		then
			echo -n -e "$YELLOW"
			echo -e "\tfulltest failed"
			echo  -e "\toutput file ${file%.cgdl}.out differs from
			reference output file"
			echo -n -e "$RESET"
			#exit $REGRESSION_FAILED
			read -p "press any key to continue" -n 1 -s
		else
			echo -n -e "$GREEN"
			echo -e "\tfulltest passed"
			echo -n -e "$RESET"
		fi
	fi
	echo -n -e "$RESET"
done
