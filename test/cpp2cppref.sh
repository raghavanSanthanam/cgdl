#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Usage: `basename $0` <removecppref|rename|removeboth>"
  exit -1
fi

cppfiles=`find -name *.cpp`
for file in $cppfiles
do
	if [[ $1 == "rename" ]]
	then
	 	mv -v $file ${file%.cpp}.cppref
	elif [[ $1 == "removecppref" ]]
	then
		rm ${file%.cpp}.cppref
	elif [[ $1 == "removeboth" ]]
	then
		rm $file ${file%.cpp}.cppref
	fi
done
