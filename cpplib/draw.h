#ifndef _DRAW_H
#define _DRAW_H

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>

//#include "cgdl.h"

bool gameOver = false;
bool randomBool(){
	if (std::rand()%2 == 1) return true;
	return false;
}

using namespace std;
vector<string> cardAttributeNames;
vector<string> cardAttributeFieldNames;
vector<string> playerNumberFieldNames;
vector<string> playerStringFieldNames;
vector<string> playerBoolFieldNames;

string end = "q";


int cardWidth = 3;
bool selectable = false;
string input;
// Generates the table

bool isVisible(Card* card, Player* thisPlayer) {
	if (card->visible == self && card->player == thisPlayer)
		return true;
	else if (card->visible == all)
		return true;
	else if (card->visible == noOne)
		return false;
	else return false;
}
void drawBoard(Player* thisPlayer){
    int x = 100;
    // int y = 50;
	int ret = system("clear");
	(void)ret;

	// Separator
	if (thisPlayer != NULL){
	  cout << "\033[1;31m" << thisPlayer->name
		<< "'s turn. " << "\033[0m"
		<< "Enter 'q' to quit. Red cards are selectable." <<endl;
	} else {
		cout << "\033[1;31m" << "Table view. " << "\033[0m"
			<< "Enter 'q' to quit. Red cards are selectable."
			<<endl;
	}

	cout.width(x);
	cout.fill('=');
	cout << "=" << endl;

	int numPiles = piles.size();
	cout.fill(' ');
	cout << endl;
	int numVisible = 0;

	for (int i = 0; i < numPiles; ++i ) {
	  Player* player = piles.at(i)->player;
		if (player != NULL) {
			cout << "\033[1;34m" << player->name << "'s hand:"
				<< "\033[0m" << endl;
		} else {
			cout << "\033[1;34m" << "Pile " << i+1
				<< ":" << "\033[0m" << endl;
		}

		if (player != NULL/* && player == thisPlayer*/) {
		  for (int k = 0; k < NUM_PLAYER_NUMBERS; k++) {
		    cout << playerNumberFieldNames[k] << ": "
				<< player->numbers[k] << endl;
		  }

		  for (int k = 0; k < NUM_PLAYER_STRINGS; k++) {
		    cout << playerStringFieldNames[k] << ": "
				<< player->strings[k] << endl;
		  }

		  for (int k = 0; k < NUM_PLAYER_BOOLS; k++) {
		    cout << playerBoolFieldNames[k] << ": "
				<< (player->bools[k] ? "true" : "false")
				<< endl;
		  }
		}

		int numCards = piles.at(i)->cards.size();
		int numInvisible = 0;

		if (numCards == 0) {
			cout << "Empty" << endl << endl;
		} else {
			for (int j = 0; j < numCards; ++j) {
				if (isVisible(
					piles.at(i)->cards.at(j),thisPlayer)){
					numVisible++;
					cout << "| " << "\033[1;31m"
						<< numVisible << "\033[0m";
					cout << " " <<
					piles.at(i)->cards.at(j)->visualize()
							<< " |";
				} else {
					++numInvisible;
				}
			}

			//Invisible cards
			if (numInvisible > 0) {
				cout << "| " << numInvisible
					<< " cards not visible to player";
			}

			cout << endl << endl;
		}
	}

	cout.width(x);
	cout.fill('=');
	cout << "=" << endl;
	cout.fill(' ');
	
}

#endif // _CARD_H

