#ifndef _PLAYER_H
#define _PLAYER_H

#include <cstdlib>
#include <string>
#include <cstring>

#include "pile.h"
#include "dynamic.h"

using namespace std;

extern vector<Player*> players;

struct Player {
	Pile * hand;
	string name;
	long double *numbers;
	string *strings;
	bool *bools;

	Player() {
		name = "";
		hand = new Pile();
		hand->player = this;
		hand->visible = self;
		players.push_back(this);

		numbers = new long double[NUM_PLAYER_NUMBERS];
		for (int i = 0; i < NUM_PLAYER_NUMBERS; i++) {
		  numbers[i] = 0;
		}

		strings = new string[NUM_PLAYER_STRINGS];
		for (int i = 0; i < NUM_PLAYER_STRINGS; i++) {
		  strings[i] = "";
		}

		bools = new bool[NUM_PLAYER_BOOLS];
		for (int i = 0; i < NUM_PLAYER_BOOLS; i++) {
		  bools[i] = false;
		}
	}

	~Player() {
		// hand should be deleted separately
	    delete[] numbers;
	    delete[] strings;
	    delete[] bools;
		players.erase(find(players.begin(), players.end(), this));
	}

};

#endif // _PLAYER_H

