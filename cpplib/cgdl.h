#ifndef _CGDL_H
#define _CGDL_H

enum CgdlConstants { null };
enum VisibilityConstant { all, self, noOne };

#include "attribute.h"
#include "functions.h"
#include "card.h"
#include "player.h"
#include "pile.h"
#include "io.h"
#include "draw.h"
#include "dynamic.h"

#endif // _CGDL_H

