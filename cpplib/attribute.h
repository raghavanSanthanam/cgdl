#ifndef _ATTRIBUTE_H
#define _ATTRIBUTE_H

#include <cassert>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "card.h"
#include "dynamic.h"

using namespace std;

enum {NONE=0};
vector<int> NONE_ATTR;

enum StdRanks { ACE=1, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN,
							JACK, QUEEN, KING };
vector<int> STD_RANK;

enum { DIAMOND = 14, CLUB, HEART, SPADE };
vector<int> STD_SUIT;

vector<string> arrayOfNames;

int currCardCompareAttr;

void initAttributes() {
	// Can run a loop to add integers from 0 to 16 to vectors STD_RANK and
	// STD_SUIT. But, this is more intuitive.
  NONE_ATTR.push_back(NONE);

	STD_RANK.push_back(ACE);
	STD_RANK.push_back(TWO);
	STD_RANK.push_back(THREE);
	STD_RANK.push_back(FOUR);
	STD_RANK.push_back(FIVE);
	STD_RANK.push_back(SIX);
	STD_RANK.push_back(SEVEN);
	STD_RANK.push_back(EIGHT);
	STD_RANK.push_back(NINE);
	STD_RANK.push_back(TEN);
	STD_RANK.push_back(JACK);
	STD_RANK.push_back(QUEEN);
	STD_RANK.push_back(KING);
	STD_SUIT.push_back(DIAMOND);
	STD_SUIT.push_back(CLUB);
	STD_SUIT.push_back(HEART);
	STD_SUIT.push_back(SPADE);

	arrayOfNames.push_back("NONE");

	arrayOfNames.push_back("ACE");
	arrayOfNames.push_back("TWO");
	arrayOfNames.push_back("THREE");
	arrayOfNames.push_back("FOUR");
	arrayOfNames.push_back("FIVE");
	arrayOfNames.push_back("SIX");
	arrayOfNames.push_back("SEVEN");
	arrayOfNames.push_back("EIGHT");
	arrayOfNames.push_back("NINE");
	arrayOfNames.push_back("TEN");
	arrayOfNames.push_back("JACK");
	arrayOfNames.push_back("QUEEN");
	arrayOfNames.push_back("KING");
	arrayOfNames.push_back("DIAMOND");
	arrayOfNames.push_back("CLUB");
	arrayOfNames.push_back("HEART");
	arrayOfNames.push_back("SPADE");

	addNewAttributes();
}

// for card sorting in Pile.h
void setCardCompareAttr(int cardAttr) {
	assert(cardAttr < NUM_CARD_ATTRIBUTES);
	currCardCompareAttr = cardAttr;
}

bool cardCompareFunc(Card* c1, Card* c2) { 
	return (c1->attributes[currCardCompareAttr] <
					c2->attributes[currCardCompareAttr]); 
}

#endif // ATTRIBUTE_H

