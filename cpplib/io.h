#ifndef _IO_H
#define _IO_H

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include "draw.h"

using namespace std;

void inline message(string text) {
	cout << text << endl;
	fflush(NULL);
}

int querySelectionHelper(int number, int numPiles, Player *thisPlayer,
						vector<Card *> &output) {
	int numVisible = 0;

	for (int i = 0; i < numPiles; ++i) {
		int numCards = piles.at(i)->cards.size();

		for (int j = 0; j < numCards; ++j) {
			if (isVisible(piles.at(i)->cards.at(j), thisPlayer)) {
				numVisible++;
				// cout <<"Number: "<< number << "
				// numVisible: " <<numVisible << endl;
				if (number == numVisible &&
					find(output.begin(), output.end(),
						piles.at(i)->cards.at(j))
							== output.end()) {
					output.push_back
						(piles.at(i)->cards.at(j));
				}
			}
		}
	}

	return numVisible;
}

vector<Card *> querySelection(Player* thisPlayer, string s) {
	vector<Card *> output;
	output.clear();
	cout << "Please make a selection by entering the card numbers: ";
	cout << endl;

	string userIn;
	string str;

	int number = 0;
	int numPiles = piles.size();

	getline(cin, userIn);
	if (userIn == "q"){
		exit(1);
	}
	istringstream iss(userIn);

	while (iss) {
		if ((iss >> str)) {
			number = atoi(str.c_str()); 

			bool reloop = false;

			if (number != 0) {
				int numVisible = querySelectionHelper
						(number, numPiles,
							thisPlayer, output);
				if (number > numVisible || output.back()->player != thisPlayer) {
					cout << "Invalid input. Try again."
							<< endl;
					output.clear();

					reloop = true;
				}
			} else {
				cout << "Invalid input. Try again." << endl;
				output.clear();

				reloop = true;				

				// getline(cin, userIn);
				// istringstream iss(userIn);
			}

			if (reloop) {
				getline(cin, userIn);
				if (userIn == "q"){
					exit(1);
				}
				istringstream iss(userIn);
			}
		}
	}

	cout <<"Your selection is: | ";
	for (size_t i = 0; i < output.size(); ++i) {
		cout << output.at(i)->visualize() << "| ";
	}

	cout << endl;
	return output;
}

long double queryNumber(string text) {
	cout << text << endl;

	double retNumber = 0;

	while (true) {
		string input = "";
		getline(cin, input);
		if (input == "q"){
			exit(1);
		}
		stringstream numStream(input);
		if (numStream >> retNumber)
			break;
		cerr << "Not a number." << endl;
	}

	return retNumber;
}

string inline queryString(string text) {
	cout << text << endl;
	string userIn;
	getline(cin, userIn);
	if (userIn == "q"){
		exit(1);
	}
	return userIn;
}

string choose(string msg, string s) {
	string r("");

	cout << msg << endl;
	istringstream iss(s);
	cout << "Valid choices are: ";
	do {
		string word;
		iss >> word;
		cout << word << " "; 
	} while (iss);

	cout << endl;
	string userIn;
	string word = " ";
	getline(cin, userIn);
	if (userIn == "q"){
			exit(1);
		}
	while (word != userIn) {
		istringstream iss(s);

		while (iss) {
			iss >> word;
			if (word == userIn) {
				cout << "You entered " << word
					<< " successfully."  << endl;
				return word;
			}
		}
		cout << "Invalid input. Try again." << endl;
		getline(cin, userIn);
		if (userIn == "q"){
			exit(1);
		}
	}

	return r;
}

string str(long double number) {
	stringstream numStream;
	numStream << number;

	return numStream.str();
}

string str(bool b) {
	return b ? "true" : "false";
}

string str(int i) {
	stringstream intStream;
	intStream << i;

	return intStream.str();
}

#endif // _IO_H

