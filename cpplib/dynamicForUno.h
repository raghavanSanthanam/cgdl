#ifndef _DYNAMIC_H
#define _DYNAMIC_H

using namespace std;

extern vector<string> arrayOfNames;
extern vector<string> cardAttributeNames;
extern vector<string> playerNumberFieldNames;
extern vector<string> playerStringFieldNames;
extern vector<string> playerBoolFieldNames;

vector<int> color;
vector<int> rank;
enum CardAttribute { CARD_RANK, CARD_COLOR, NUM_CARD_ATTRIBUTES, };

enum PlayerNumber { PLAYER_POINTS, NUM_PLAYER_NUMBERS, };

enum PlayerString { NUM_PLAYER_STRINGS, };

enum PlayerBool { NUM_PLAYER_BOOLS, };


void initFields() {
	cardAttributeNames.push_back("rank");
	cardAttributeNames.push_back("color");
	playerNumberFieldNames.push_back("points");
}


void addNewAttributes(void)
{
	rank.push_back(_0);
	rank.push_back(_1);
	rank.push_back(_2);
	rank.push_back(_3);
	rank.push_back(_4);
	rank.push_back(_5);
	rank.push_back(_6);
	rank.push_back(_7);
	rank.push_back(_8);
	rank.push_back(_9);
	rank.push_back(REVERSE);
	rank.push_back(SKIP);
	rank.push_back(DRAWTWO);
	rank.push_back(WILD);
	rank.push_back(WILDDRAWFOUR);
	color.push_back(RED);
	color.push_back(YELLOW);
	color.push_back(GREEN);
	color.push_back(BLUE);

	arrayOfNames.push_back("ZERO");
	arrayOfNames.push_back("ONE");
	arrayOfNames.push_back("TWO");
	arrayOfNames.push_back("THREE");
	arrayOfNames.push_back("FOUR");
	arrayOfNames.push_back("FIVE");
	arrayOfNames.push_back("SIX");
	arrayOfNames.push_back("SEVEN");
	arrayOfNames.push_back("EIGHT");
	arrayOfNames.push_back("NINE");
	arrayOfNames.push_back("REVERSE");
	arrayOfNames.push_back("SKIP");
	arrayOfNames.push_back("DRAWTWO");
	arrayOfNames.push_back("WILD");
	arrayOfNames.push_back("WILDDRAWFOUR");
	arrayOfNames.push_back("RED");
	arrayOfNames.push_back("YELLOW");
	arrayOfNames.push_back("GREEN");
	arrayOfNames.push_back("BLUE");
}


#endif // _DYNAMIC_H
