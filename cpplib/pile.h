#ifndef _PILE_H
#define _PILE_H

#include <cstdlib>
#include <algorithm>
#include <deque>
#include <vector>
#include <cassert>

#include "card.h"
#include "functions.h"

struct Player;

using namespace std;
extern vector<Pile *> piles;
extern Pile* deckPile;

int inline myrandom (int i) {
	return std::rand() % i;
}

struct Pile {
	deque<Card *> cards;
	Player *player;
	int visible;
	
	Pile() : player(NULL), visible(noOne) {
		piles.push_back(this);
	}

	~Pile() {
		if (!empty()) { // prevent this from happening during cleanup
			deckPile->putOnBottom(this);
		}
		piles.erase(find(piles.begin(), piles.end(), this));
	}

	void removeFromPile(Card *c) {
		if (c->pile) {
			for (deque<Card*>::iterator it = c->pile->cards.begin();
				it != c->pile->cards.end(); ++it) {
				if (*it == c) {
					c->pile->cards.erase(it);
					c->pile = this;
					if (player) {
						c->player = player;
					}
					return;
				}
			}
			assert(false);
		}
		c->pile = this;
		if (player) {
			c->player = player;
		}
	}

	void putOnTop(Card* c) {
		c->visible = this->visible;
		cards.push_back(c);
		removeFromPile(c);
	}

	void putOnTop(vector<Card*> cs) {
		for (unsigned int i = 0; i < cs.size(); ++i) {
			putOnTop(cs[i]);
		}
	}

	void putOnTop(Pile* p) {
		while(!p->cards.empty()) {
			putOnTop(p->getFromFront());
		}
	}

	void putAtEnd(Card* c) {
		putOnTop(c);
	}

	void putAtEnd(vector<Card*> cs) {
		putOnTop(cs);
	}

	void putAtEnd(Pile* p) {
		putOnTop(p);
	}

	void putOnBottom(Card* c) {
		c->visible = this->visible;
		cards.push_front(c);
		removeFromPile(c);
	}

	void putOnBottom(vector<Card*> cs) {
		for (unsigned int i = 0; i < cs.size(); ++i) {
			putOnBottom(cs[i]);
		}
	}

	void putOnBottom(Pile* p) {
		while(!p->cards.empty()) {
			putOnBottom(p->getFromEnd());
		}
	}

	void putAtFront(Card* c) {
		putOnBottom(c);
	}

	void putAtFront(vector<Card*> cs) {
		putOnBottom(cs);
	}

	void putAtFront(Pile* p) {
		putOnBottom(p);
	}

	Card* getFromTop() {
		return cards.empty() ? NULL : cards.back();
	}

	vector<Card*> getFromTop(long double N) {
		vector<Card*> selection; // if this doesn't work,
						// check if this needs a new

		deque<Card*>::reverse_iterator it = cards.rbegin();
		for (unsigned long long int i = 0;
				i < (unsigned long long int) N; ++i) {
			selection.push_back(*it);
			if (it == cards.rend()) break;
			++it;
		}

		return selection;
	}

	Card* getFromEnd() {
		return getFromTop();
	}

	vector<Card*> getFromEnd(long double N) {
		return getFromTop((unsigned long long int) N);
	}

	Card* getFromBottom() {
		if (cards.empty()) return NULL;
		return cards.front();
	}

	vector<Card*> getFromBottom(long double N) {
		vector<Card*> selection; // same as above
		deque<Card*>::iterator it = cards.begin();

		for (unsigned long long int i = 0;
				i < (unsigned long long int) N; ++i) {
			selection.push_back(*it);
			if (it == cards.end()) break;
			++it;
		}

		return selection;
	}

	Card* getFromFront() {
		return getFromBottom();
	}

	vector<Card*> getFromFront(long double N) {
		return getFromBottom((unsigned long long int) N);
	}

	Card* at(long double N) {
		return cards.at((unsigned long long int) N);
	}

	bool empty() {
		return cards.empty();
	}

	size_t size() {
		return cards.size();
	}

	void shuffle() {
		random_shuffle(cards.begin(), cards.end(), myrandom);
	}

	void sort(int attr, bool ascending) {
		setCardCompareAttr(attr);
		if (ascending) {
			std::sort(cards.begin(), cards.end(), cardCompareFunc);
		}
		else { // descending uses reverse iterators
			std::sort(cards.rbegin(), cards.rend(),
							cardCompareFunc);
		}
	}
};

#endif // _PILE_H

