#ifndef _CARD_H
#define _CARD_H

#include <cstdlib>
#include <string>
#include <deque>
#include <sstream>

#include "dynamic.h"

using namespace std;
extern vector<string> arrayOfNames;

struct Pile;
struct Player;
struct Card {
	int visible;
	Player* player;
	Pile* pile;
	int *attributes;
	
	Card() : player(NULL), pile(NULL) {
		attributes = new int[NUM_CARD_ATTRIBUTES];
	}

	~Card() {
		delete[] attributes;
	}

	string visualize() {
		stringstream str;
		for (int i = 0; i < NUM_CARD_ATTRIBUTES; i++) {
		  if (attributes[i] == 0) { // should be NONE
		    continue;
		  }
			str << arrayOfNames[attributes[i]] << " ";
		}
		return str.str();
	}

	void show() {
		cout << visualize() << endl;
	}
};

#endif // _CARD_H

