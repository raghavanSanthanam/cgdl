#ifndef _FUNCTION_H
#define _FUNCTION_H

#include <vector>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <ctime>

#include "pile.h"
#include "player.h"
#include "card.h"
#include "attribute.h"
#include "dynamic.h"

using namespace std;

vector<Pile *> piles; // need to initialize
// Upon seeing numPlayers in an assignment
// statement, compiler puts a loop just next to
// it without an intermediate code to check the
// validity of value received in numPlayers. So,
// we will save in a temp and if found correct,
// we use it for the standard numPlayers.
int numPlayersEntered;
int numPlayers;
vector<Player *> players;
Pile* deckPile;


void init() {
	srand(time(NULL));
	deckPile = new Pile();
	deckPile->visible = noOne;

	initFields();
	initAttributes();
}

void cleanup() {
	unsigned int origSize = piles.size();
	for (unsigned int i = 0; i < origSize; i++) {
		// cards
		unsigned int origCardsSize = piles[i]->cards.size();

		Card **tmpCards = new Card*[origCardsSize];
		int k = 0;

		for (deque<Card*>::iterator it = piles[i]->cards.begin();
			it != piles[i]->cards.end(); ++it) {
			tmpCards[k] = *it;
			k++;
		}

		piles[i]->cards.clear();
		for (unsigned int j = 0; j < origCardsSize; j++) {
			delete tmpCards[j];
			tmpCards[j] = NULL;
	  	}

		delete[] tmpCards;
		tmpCards = NULL;
	}

	// piles
	unsigned int origPilesSize = piles.size();
	Pile **tmpPiles = new Pile*[origPilesSize];

	for (unsigned int i = 0; i < origPilesSize; i++) {
		//cout << piles[i] << endl;
		tmpPiles[i] = piles[i];
	}

	for (unsigned int i = 0; i < origPilesSize; i++) {
		//cout << tmpPiles[i] << endl;
		delete tmpPiles[i];
		tmpPiles[i] = NULL;
	}
	delete[] tmpPiles;
	tmpPiles = NULL;

	// players
	unsigned int origPlayersSize = players.size();
	Player **tmpPlayers = new Player*[origPlayersSize];
	for (unsigned int i = 0; i < origPlayersSize; i++) {
		tmpPlayers[i] = players[i];
	}
	for (unsigned int i = 0; i < origPlayersSize; i++) {
		delete tmpPlayers[i];
		tmpPlayers[i] = NULL;
	}
	delete[] tmpPlayers;
	tmpPlayers = NULL;
}

void addToDeck(vector<vector<int> > vec) {
	unsigned int numAttrs = vec.size();
	unsigned int totalNum = 1;
	for (unsigned int i = 0; i < numAttrs; i++) {
		totalNum *= vec[i].size();
	}

	vector<Card*> cards;
	for (unsigned int i = 0; i < totalNum; i++) {
		cards.push_back(new Card());
	}

	for (unsigned int i = 0; i < numAttrs; i++) {
		unsigned int numCycles = 1;
		for (unsigned int j = 0; j < i; j++) {
			numCycles *= vec[j].size();
		}
		unsigned int numReps = 1;
		for (unsigned int j = i+1; j < numAttrs; j++) {
			numReps *= vec[j].size();
		}
		
		unsigned int k =  0;
		for (unsigned int m = 0; m < numCycles; m++) {
			for (unsigned int p = 0; p < vec[i].size(); p++) {
				for (unsigned int n = 0; n < numReps; n++) {
					cards[k]->attributes[i] = vec[i][p];
					cards[k]->visible = noOne;
					k++;
				}
			}
		}
	}
	
	deckPile->putOnTop(cards);
}


template <class T>
vector<T> subset (vector<T> set, int a, int b) {
  typename vector<T>::iterator it;
  vector<T> output;
  for (int i = a; i < b; ++i){
    output.push_back(set.at(i));
  }
  return output;
}

void swapPlayers(Player *p1, Player *p2) {
	iter_swap(find(players.begin(), players.end(), p1),
			find(players.begin(), players.end(), p2));
}

void reversePlayers() {
	reverse(players.begin(), players.end());
}

Player* nextPlayer(Player *p, int num) {
  vector<Player *>::iterator pIter = find(players.begin(), players.end(), p);

  if (num < 0) {
    for (int i = 0; i > num; i--) {
      if (pIter == players.begin()) {
	pIter = players.end();
      }
      --pIter;
    }
  } else {
    for (int i = 0; i < num; i++) {
      ++pIter;
      if (pIter == players.end()) {
	pIter = players.begin();
      }
    }
  }

  return *pIter;
}

#endif // _FUNCTION_H

