#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <sstream>
#include "cgdl.h"

using namespace std;


// Uno Card Game in CGDL 
// global 
// choose by displayed index 
// draw card 
// got number card 
// draw cards 
// for hand secrecy 
// game is over 
// only one 0 per color 
// two of everything else (except WILD) per color 
Player* currPlayer;

double  WIN_AMOUNT = 500;
int  attrColorChoice;

void playCard(Card* card, Pile* playPile) {
  if (!playPile->empty()) {
    deckPile->putOnBottom(playPile->getFromEnd());
  }
  playPile-> putAtFront(card);
}

bool  tryPlay(Card*  card, Pile* playPile){
    Card*  lastCard =  playPile-> getFromFront();

    if (card->attributes[CARD_RANK] ==  WILD ||  card->attributes[CARD_RANK] ==  WILDDRAWFOUR) {
      playCard(card, playPile);
      return true;
    }

    if (lastCard->attributes[CARD_RANK] ==  WILD ||  lastCard->attributes[CARD_RANK] ==  WILDDRAWFOUR) {
        if (card-> attributes[CARD_COLOR] ==  attrColorChoice) {
	  playCard(card, playPile);
            return true;
        }
    }
    else if (card-> attributes[CARD_RANK] ==  lastCard->attributes[CARD_RANK] ||  card->attributes[CARD_COLOR] ==  lastCard->attributes[CARD_COLOR]) {
      playCard(card, playPile);
        return true;
    }

    message("Couldn't play that card.");
    return false;
}
double  calcPoints(Pile*  pile){
    double  points = 0;
    for (deque<Card*>::iterator pileIt = pile->cards.begin(); pileIt != pile->cards.end(); ++pileIt){
        Card* card = *pileIt;
        if (card-> attributes[CARD_RANK] ==  WILD ||  card-> attributes[CARD_RANK] ==  WILDDRAWFOUR) {
            points =  points + 50;
        }
        else if (card-> attributes[CARD_RANK] > 9) {
            points =  points + 20;
        }
        else{
            points =  points + ( card-> attributes[CARD_RANK] -  _0);
        }
    }
    return points;
}
void playOrDraw (Pile* playPile, Player* player) {
    bool  played = false;
    while (!(played)) {
      drawBoard(currPlayer);
      string  choice =  choose("Your turn.", "play " "draw ");
        if (choice == "play") {
	  drawBoard(currPlayer);
            double  cardChoice =  queryNumber("Which Card?");
            if (tryPlay( player-> hand->at( cardChoice), playPile)) {
                played = true;
            }
        }
        else{
            player-> hand-> putAtEnd( deckPile-> getFromTop());
	    drawBoard(currPlayer);
            string  playChoice =  choose("Play it?", "Yes " "No ");
            if (playChoice == "Yes") {
                tryPlay( player-> hand-> getFromEnd(), playPile);
            }
         played = true;
        }
    }
}
void game(){
    currPlayer = NULL;
    Player*  winner = NULL;
        
    bool  reversed = false;
    for (vector<Player*>::iterator playerIt = players.begin(); playerIt != players.end(); ++playerIt){
        Player* player = *playerIt;
        deckPile->putOnBottom(player-> hand);
    }

    for (int i=0; i<7;i++){
        for (vector<Player*>::iterator playerIt = players.begin(); playerIt != players.end(); ++playerIt){
            Player* player = *playerIt;
            player-> hand-> putAtEnd( deckPile-> getFromTop());
        }
    }
    currPlayer = players[0];
    Pile playPile_;
    Pile * playPile = &playPile_;
    playPile->visible = all;

    bool ok = false;
    while (!(ok)) {
        Card*  card =  deckPile-> getFromTop();
	playCard(card, playPile);
	
        if (!(card-> attributes[CARD_RANK] ==  WILD) &&  
	    !(card-> attributes[CARD_RANK] ==  WILDDRAWFOUR) &&  
	    card-> attributes[CARD_RANK] <=  _9) {
            ok = true;
            attrColorChoice =  card-> attributes[CARD_COLOR];
	}
    }

    bool drawn = true;

    while (winner == NULL) {
        drawBoard(currPlayer); 
        Card*  lastCard =  playPile-> getFromFront();

            if (lastCard-> attributes[CARD_RANK] ==  DRAWTWO && !drawn) {
                for (int i=0; i<2;i++){
                    currPlayer-> hand-> putAtEnd( deckPile-> getFromTop());
                }
		drawn = true;
            }
            else if (lastCard-> attributes[CARD_RANK] ==  WILDDRAWFOUR && !drawn) {
                for (int i=0; i<4;i++){
                    currPlayer-> hand-> putAtEnd( deckPile-> getFromTop());
                }
		drawn = true;
            }
            else{
                playOrDraw(playPile, currPlayer);
		Card*  cardPlayed =  playPile-> getFromFront();
		if (cardPlayed-> attributes[CARD_RANK] ==  DRAWTWO || cardPlayed-> attributes[CARD_RANK] ==  WILDDRAWFOUR) {
		  drawn = false;
		}
            }
            
            Card*  cardPlayed =  playPile-> getFromFront();
            if (cardPlayed-> attributes[CARD_RANK] ==  REVERSE) {
	      reversePlayers();
            }
            if (cardPlayed-> attributes[CARD_RANK] ==  SKIP) {
	        currPlayer = nextPlayer(currPlayer, 1);
            }
            if (cardPlayed-> attributes[CARD_RANK] ==  WILD ||  cardPlayed-> attributes[CARD_RANK] ==  WILDDRAWFOUR) {
	      drawBoard(currPlayer);
	      string  colorChoice =  choose("Which color?", "Red " "Yellow " "Green " "Blue ");
                if (colorChoice == "Red"){
                    attrColorChoice = RED;
                }  
                else if(colorChoice == "Yellow") {
                    attrColorChoice = YELLOW;
                }
                else if(colorChoice == "Green"){
                    attrColorChoice = GREEN;
                }
                else if(colorChoice == "Blue"){
                    attrColorChoice = BLUE;
                }
            }
            if (currPlayer-> hand-> empty()) {
	      winner =  currPlayer;
	      drawBoard(winner); 
            }

	    currPlayer = nextPlayer(currPlayer, 1);

    }

    cleanup(); 
}

int main(void) {
    init();
    currPlayer = NULL;
    Player*  winner = NULL;

    deckPile->visible = all;

    vector<vector<int> > vec;
    vec.push_back(subset(rank, 0, 1));
    vec.push_back(color);
    addToDeck(vec);

    for (int i=0; i<2;i++){
        vector<vector<int> > vec;
        vec.push_back(subset(rank,1,13));
        vec.push_back(color);
        addToDeck(vec);
    }
    for (int i=0; i<4;i++){
        vector<vector<int> > vec;
        vec.push_back(subset(rank,13,15));
        vec.push_back(NONE_ATTR);
        addToDeck(vec);
    }
    //deckPile-> shuffle();

    drawBoard(currPlayer);
    numPlayers =  queryNumber("How many players?");
    players.clear();
    for (int i = 0; i < numPlayers; i++) { 
      Player *p = new Player();
	p->name = "Player " + itoa(i);
    }

    while (winner == NULL) {
        drawBoard(currPlayer); 
        game();
        for (vector<Player*>::iterator playerIt = players.begin(); playerIt != players.end(); ++playerIt){
            Player* player = *playerIt;
            player->numbers[PLAYER_POINTS] =  player->numbers[PLAYER_POINTS] +  calcPoints( player-> hand);
        }
        bool end = false;
        for (vector<Player*>::iterator playerIt = players.begin(); playerIt != players.end(); ++playerIt){
            Player* player = *playerIt;
            if (player->numbers[PLAYER_POINTS] >=  WIN_AMOUNT) {
                end = true;
            }
        }
        if (end) {
            for (vector<Player*>::iterator playerIt = players.begin(); playerIt != players.end(); ++playerIt){
                    Player* player = *playerIt;
                    if (!winner ||  player->numbers[PLAYER_POINTS] <  winner->numbers[PLAYER_POINTS]) {
                    winner =  player;
		    drawBoard(winner);
                    message("Game Over!!");
                }
            }
        }
    }

    cleanup(); 
}
