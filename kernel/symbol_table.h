#ifndef _SYMBOL_TABLE_H
#define _SYMBOL_TABLE_H

#include "ast.h"

extern void combine_intermediate_files_to_form_src_and_exit(int exit_code);

#define COMPILATION_ERROR(...) \
		do {\
			fprintf(stderr, "Error %s %llu: ", src_file,\
				((struct term_or_nonterm *)\
				trans_node)->src_line);\
			fprintf(stderr, __VA_ARGS__);\
			combine_intermediate_files_to_form_src_and_exit(-1);\
	} while ( 0 )

extern void add_comp_type(char *t, unsigned long long int src_line);
extern bool is_var_defined(char *v);
extern void add_symbol(struct unified_type *t,
					unsigned long long int src_line);
extern bool is_type_defined(char *n);
extern bool is_comp_type_member_defined(char *m_name,
						struct comp_type *c);
extern struct unified_type *type_of(char *v, struct unified_type *parent);
extern struct unified_type *unification(char *t, char *v, int type);
extern void free_symbol_lists(void);
extern void display_current_block_symbol_table(void);
extern void display_current_and_enclosing_block_symbol_table(void);
extern void display_all_blocks_symbol_table(void);
extern void free_symbol_list(int b);
extern void display_func_types(void);

#endif // _SYMBOL_TABLE_H
