#!/bin/bash

EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` {file to compile}"
  exit $E_BADARGS
fi

file=$1

./cgdlc $file &>/dev/null;

exit_code=$?
zero=0

if [ $exit_code -eq $zero ]
then
	filename=${file%.*}
	g++ -Wall -g -I ../cpplib $filename.cpp -o $filename 2>/dev/null
	if [[ $? -eq 0 ]]
	then
		echo "Game executable: $filename"
	else
		echo "Building game failed"
	fi
fi

