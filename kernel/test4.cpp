#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <csignal>

#include "cgdl.h"

using namespace std;

unsigned long long int SrcLine;


// Game specific
void *initializerFromCtor;
Player *player;
Card *card;


void CrashHandler(int sig)
{
	cout << "Crash when executing "
	<< SrcLine << " line in test4.cgdl" << endl;
	exit(-1);
}

