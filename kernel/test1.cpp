#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <csignal>

#include "cgdl.h"

using namespace std;

unsigned long long int SrcLine;


// Game specific
void *initializerFromCtor;
Player *player;
Card *card;


void CrashHandler(int sig)
{
	cout << "Crash when executing "
	<< SrcLine << " line in test1.cgdl" << endl;
	exit(-1);
}

// number f(none); 
struct 
x {
	long double y;
	string z;
	bool a;
	Player *p;
	Pile *q;
	Card *card;
};
enum c { c1 } ;
// x.f(); 


int main(void) {

	signal(SIGSEGV, CrashHandler);


	init();
{
	
	Player *currPlayer = NULL;
	Player *winner = NULL;

SrcLine = 17;
	struct 	x x = { (long double)0, "Hello", false, NULL, NULL, NULL} ;

SrcLine = 18;
	x.y;

SrcLine = 19;
	x.z;

SrcLine = 20;
	x.a;

SrcLine = 21;
	x.p;

SrcLine = 22;
	x.q;

SrcLine = 23;
	x.card;

SrcLine = 26;
	long double y[100] = { (long double)1} ;

	while (!winner) 	{

SrcLine = 30;
	enum 		c catalogConst = c1;

SrcLine = 31;
static 	string s = "";

SrcLine = 32;
	long double b = (long double)0;

SrcLine = 33;
		enum VisibilityConstant v = all;

SrcLine = 34;
	bool bl = false;

SrcLine = 36;

		message("catalogConst: ");
		
SrcLine = 37;
		message(str(catalogConst));

SrcLine = 38;
		message("const number y: ");
		
SrcLine = 39;
		message(str(y[0]));

SrcLine = 40;
		message("string s: ");
		
SrcLine = 41;
		message(s);

SrcLine = 42;
		message("number b: ");
		
SrcLine = 43;
		message(str(b));

SrcLine = 44;
		message("visibility v: ");
		
SrcLine = 45;
		message(str(v));

SrcLine = 46;
		message("bool bl: ");
		
SrcLine = 47;
		message(str(bl));

SrcLine = 48;
		getchar();
			}

}

	cleanup();

	return 0;
	}
