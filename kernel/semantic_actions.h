#ifndef _SEMANTIC_ACTIONS_H
#define _SEMANTIC_ACTIONS_H

extern char *strdup(const char *);

extern char *string_literal; // Constructed in cgdl.l
extern void *(*semantic_actions[])();

extern void *lines_semantic_actions(int prod, void *a1, void *a2);
extern void *identifier_semantic_actions(int prod, void *a1);
extern void *constant_semantic_actions(int prod, void *a1);
extern void *visibility_const_semantic_actions(int prod);
extern void *primary_semantic_actions(int prod, void *a1);
extern void *postfix_semantic_actions(int prod, void *a1, void *a2, void *a3);
extern void *arg_exp_list_semantic_actions(int prod, void *a1, void *a2);
extern void *unary_expression_semantic_actions(int prod, void *a1, void *a2);
extern void *unary_operator_semantic_actions(int prod);
extern void *multiplicative_expression_semantic_actions(int prod, void *a1, void *a2);
extern void *additive_expression_semantic_actions(int prod, void *a1, void *a2);
extern void *relational_expression_semantic_actions(int prod, void *a1, void *a2);
extern void *equality_expression_semantic_actions(int prod, void *a1, void *a2);
extern void *logical_and_exp_semantic_actions(int prod, void *a1, void *a2);
extern void *logical_or_exp_semantic_actions(int prod, void *a1, void *a2);
extern void *logical_xor_exp_semantic_actions(int prod, void *a1, void *a2);
extern void *assignment_expression_semantic_actions(int prod, void *a1, void *a2);
extern void *expression_semantic_actions(int prod, void *a1, void *a2);
extern void *constant_expression_semantic_actions(int prod, void *a1);
extern void *declaration_semantic_actions(int prod, void *a1, void *a2);
extern void *decln_spec_semantic_actions(int prod, void *a1, void *a2);
extern void *init_declr_list_semantic_actions(int prod, void *a1, void *a2);
extern void *init_declr_semantic_actions(int prod, void *a1, void *a2);
extern void *storage_spec_semantic_actions(int prod);
extern void *type_spec_semantic_actions(int prod, void *a1);
extern void *game_elem_semantic_actions(int prod);
extern void *attr_spec_semantic_actions(int prod, void *a1, void *a2);
extern void *attr_val_list_semantic_actions(int prod, void *a1, void *a2);
extern void *attr_const_semantic_actions(int prod, void *a1);
extern void *record_spec_semantic_actions(int prod, void *a1, void *a2);
extern void *record_decln_list_semantic_actions(int prod, void *a1, void *a2);
extern void *record_decln_semantic_actions(int prod, void *a1, void *a2);
extern void *spec_qual_list_semantic_actions(int prod, void *a1, void *a2);
extern void *record_declr_list_semantic_actions(int prod, void *a1, void *a2);
extern void *record_declr_semantic_actions(int prod, void *a1);
extern void *catalog_spec_semantic_actions(int prod, void *a1, void *a2);
extern void *catalog_list_semantic_actions(int prod, void *a1, void *a2);
extern void *catalog_const_semantic_actions(int prod, void *a1, void *a2);
extern void *type_qual_semantic_actions(int prod);
extern void *declarator_semantic_actions(int prod, void *a1, void *a2);
extern void *parameter_type_list_semantic_actions(int prod, void *a1);
extern void *parameter_list_semantic_actions(int prod, void *a1, void *a2);
extern void *parameter_decln_semantic_actions(int prod, void *a1, void *a2);
extern void *identifier_list_semantic_actions(int prod, void *a1, void *a2);
extern void *initializer_semantic_actions(int prod, void *a1);
extern void *initializer_list_semantic_actions(int prod, void *a1, void *a2);
extern void *statement_semantic_actions(int prod, void *a1);
extern void *label_stmt_semantic_actions(int prod, void *a1, void *a2);
extern void *expression_stmt_semantic_actions(int prod, void *a1);
extern void *block_stmt_semantic_actions(int prod, void *a1, void *a2);
extern void *decl_list_or_stmt_list_semantic_actions(int prod, void *a1,
								void *a2);
extern void *selection_stmt_semantic_actions(int prod, void *a1, void *a2,
								void *a3);
extern void *iteration_stmt_semantic_actions(int prod, void *a1, void *a2,
								void *a3);
extern void *jump_stmt_semantic_actions(int prod, void *a1);
extern void *attribute_addition_stmt_semantic_actions(int prod, void *a1,
								void *a2,
								void *a3);
extern void *translation_unit_semantic_actions(int prod, void *a1, void *a2);
extern void *elements_semantic_actions(int prod, void *a1);
extern void *function_definition_semantic_actions(int prod, void *a1, void *a2,
								void *a3);
extern void *procedure_definition_semantic_actions(int prod, void *a1,
								void *a2);


#endif // _SEMANTIC_ACTIONS_H

