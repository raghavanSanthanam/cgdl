#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

#include "ast.h"
#include "symbol_table.h"

#ifdef __linux__
#include <execinfo.h>
#endif

extern const char *src_file;
extern FILE *op;
extern FILE *intrmd;
extern FILE *dynamic;
extern char *indentation;
char *previous_indentation;
extern long long int num_of_empty_lines;
extern char *enum_card_attr;
extern char *enum_card_field;
extern char *enum_player_attr;
extern char *enum_player_field;
extern char *new_attr_vec_push;
extern char *dynamic_definitions;
extern char *attr_enums;
extern char *last_member;

extern char *enum_card_number;
extern char *enum_card_string;
extern char *enum_card_bool;

extern char *enum_player_number;
extern char *enum_player_string;
extern char *enum_player_bool;

extern void combine_intermediate_files_to_form_src(void);
extern char *strdup(const char *s);
extern void add_func_type(struct func_type *f, unsigned long long int src_line);

unsigned long long int cur_src_line; // Used to display what line the compiler
					// was reading when it crashed!

void *cur_trans_node;

#define __DEBUG__

#ifdef __DEBUG__
#define add_symbol(...)	do {\
				display_current_block_symbol_table();\
				add_symbol(__VA_ARGS__);\
				display_current_block_symbol_table();\
		} while ( 0 )
#define add_comp_type(...) do {\
				display_current_block_symbol_table();\
				add_comp_type(__VA_ARGS__);\
				display_current_block_symbol_table();\
		} while ( 0 )	
#define add_func_type(...) do {\
				display_func_types();\
				add_func_type(__VA_ARGS__);\
				display_func_types();\
		} while ( 0 )
#endif

#define _EMBED_CGDL_SRC_LINE_

#ifdef _EMBED_CGDL_SRC_LINE_ // Helper macro to debug generated C++ file.
#define EMBED_SRC_LINE() do {\
				fprintf(op, "\nSrcLine = %llu;\n",\
				((struct term_or_nonterm *)cur_trans_node)\
								->src_line);\
		} while ( 0 )
#else
#define EMBED_SRC_LINE()
#endif // EMBED_SRC_LINE

// #define __CODE_GENERATOR_LINE_INFO__

#ifdef __CODE_GENERATOR_LINE_INFO__
#define fprintf(...) do {\
				fprintf(op, "%d: ", __LINE__);\
				fprintf(__VA_ARGS__);\
		} while ( 0 )
#define fputc(...) do {\
				fprintf(op, "%d: ", __LINE__);\
				fputc(__VA_ARGS__);\
		} while ( 0 )
#define fputs(...) do {\
				fprintf(op, "%d: ", __LINE__);\
				fputs(__VA_ARGS__);\
		} while ( 0 )
#endif // __CODE_GENERATOR_LINE_INFO__

#ifdef __DEBUG__
#define UNKNOWN_TYPE(type) do {\
                                fprintf(stderr,\
                                "%s: %s:%d - BUG!!!! - Unknown type: %d\n",\
                                __FILE__, __func__, __LINE__, type); \
				exit(0);\
			} while( 0 )
#define HIT()	do {\
			cur_trans_node = trans_node;\
			if ( !trans_node ) {\
				return;\
			}\
			cur_src_line = ((struct term_or_nonterm *)trans_node)\
								->src_line;\
			printf("%s: %s - %d - addr:%p type: %d\n",\
				__FILE__, __func__, __LINE__,\
				trans_node, *(int *)trans_node); \
			fflush(NULL);\
		} while ( 0 )
#define TOUCH()	do {\
			printf("%s()\n", __func__);\
			fflush(NULL);\
		} while ( 0 )
#else // !__DEBUG__
#define UNKNOWN_TYPE(x)
#define HIT()
#define TOUCH()
#endif // __DEBUG__

#define NO_EXIT_ON_COMPILE_TIME_ERRORS

#ifdef NO_EXIT_ON_COMPILE_TIME_ERRORS
#define exit(exit_code)
#endif // NO_EXIT_ON_COMPILE_TIME_ERRORS

#define INDENTATION_NEEDED

#ifdef INDENTATION_NEEDED
#define INDENT_ONCE()	do {\
				char **indent = &((struct term_or_nonterm *)\
						trans_node)->indentation;\
				if ( *indent ) {\
					previous_indentation = *indent;\
					if ( setup_seen ) {\
						(*indent)\
						[ strlen(*indent) - 1 ] = '\0';\
					}\
					fprintf(op, "%s", *indent);\
					/*free(*indent);\
					*indent = NULL;\*/\
				}\
			} while( 0 )
#define INDENT()	do {\
				char **indent = &((struct term_or_nonterm *)\
						trans_node)->indentation;\
				if ( *indent ) {\
					previous_indentation = *indent;\
					if ( setup_seen ) {\
						(*indent)\
						[ strlen(*indent) - 1 ] = '\0';\
					}\
					fprintf(op, "%s", *indent);\
				}\
			} while ( 0 )
#else // !INDENTATION_NEEDED
#define INDENT()
#endif // INDENTATION_NEEDED

#ifndef STRIP_OF_NEWLINES
#define INSERT_EMPTY_LINES()	do {\
					long long int *em =\
						&((struct term_or_nonterm *)\
							trans_node)\
							->num_of_empty_lines;\
					while ( em[ 0 ]-- > 0 ) {\
						fputc('\n', op);\
					}\
				} while ( 0 )
#else // STRIP_OF_EMPTY_LINES
#define INSERT_EMPTY_LINES()
#endif // !STRIP_OF_EMPTY_LINES

static void trans_lines(void *trans_node);
static void trans_identifier(void *trans_node);
static void trans_constant(void *trans_node);
static void trans_visibility_const(void *trans_node);
static void trans_primary(void *trans_node);
static void trans_postfix(void *trans_node);
static void trans_arg_exp_list(void *trans_node);
static void trans_unary_expression(void *trans_node);
static void trans_unary_operator(void *trans_node);
static void trans_multiplicative_expression(void *trans_node);
static void trans_additive_expression(void *trans_node);
static void trans_relational_expression(void *trans_node);
static void trans_equality_expression(void *trans_node);
static void trans_logical_and_exp(void *trans_node);
static void trans_logical_or_exp(void *trans_node);
static void trans_logical_xor_exp(void *trans_node);
static void trans_assignment_expression(void *trans_node);
static void trans_expression(void *trans_node);
static void trans_constant_expression(void *trans_node);
static void trans_declaration(void *trans_node);
static void trans_decln_spec(void *trans_node);
static void trans_init_declr_list(void *trans_node);
static void trans_init_declr(void *trans_node);
static void trans_storage_spec(void *trans_node);
static void trans_type_spec(void *trans_node);
static void trans_game_elem(void *trans_node);
static void trans_attr_spec(void *trans_node);
static void trans_attr_val_list(void *trans_node);
static void trans_attr_const(void *trans_node);
static void trans_record_spec(void *trans_node);
static void trans_record_decln_list(void *trans_node);
static void trans_record_decln(void *trans_node);
static void trans_spec_qual_list(void *trans_node);
static void trans_record_declr_list(void *trans_node);
static void trans_record_declr(void *trans_node);
static void trans_catalog_spec(void *trans_node);
static void trans_catalog_list(void *trans_node);
static void trans_catalog_const(void *trans_node);
static void trans_type_qual(void *trans_node);
static void trans_declarator(void *trans_node);
static void trans_parameter_type_list(void *trans_node);
static void trans_parameter_list(void *trans_node);
static void trans_parameter_decln(void *trans_node);
static void trans_identifier_list(void *trans_node);
static void trans_initializer(void *trans_node);
static void trans_initializer_list(void *trans_node);
static void trans_statement(void *trans_node);
static void trans_label_stmt(void *trans_node);
static void trans_expression_stmt(void *trans_node);
static void trans_block_stmt(void *trans_node);
static void trans_decl_list_or_stmt_list(void *trans_node);
static void trans_selection_stmt(void *trans_node);
static void trans_iteration_stmt(void *trans_node);
static void trans_jump_stmt(void *trans_node);
static void trans_attribute_addition_spec(void *trans_node);
static void trans_translation_unit(void *trans_node);
static void trans_elements(void *trans_node);
static void trans_function_definition(void *trans_node);
static void trans_procedure_definition(void *trans_node);

const char *type_specifiers[] = { "none", "string", "bool", "number",
					"Card *", "Player *", "Pile *", };

#ifdef __DEBUG__
const char *translator_func[] = {
	"trans_lines", "trans_identifier", "trans_constant",
	"trans_visibility_const", "trans_primary", "trans_postfix",
	"trans_arg_exp_list", "trans_unary_expression",
	"trans_unary_operator", "trans_multiplicative_expression",
	"trans_additive_expression", "trans_relational_expression",
	"trans_equality_expression", "trans_logical_and_exp",
	"trans_logical_or_exp", "trans_logical_xor_exp",
	"trans_constant_expression", "trans_assignment_expression",
	"trans_expression", "trans_declaration", "trans_decln_spec",
	"trans_init_declr_list", "trans_declarator",
	"trans_storage_spec", "trans_type_spec", "trans_game_elem",
	"trans_attr_spec", "trans_attr_val_list", "trans_attr_const",
	"trans_record_spec", "trans_record_decln_list",
	"trans_record_decln", "trans_spec_qual_list",
	"trans_record_declr_list", "trans_record_declr",
	"trans_catalog_spec", "trans_catalog_list",
	"trans_catalog_const", "trans_type_qual", 
	"trans_declarator", "trans_parameter_type_list",
	"trans_parameter_list", "trans_parameter_decln",
	"trans_identifier_list", "trans_initializer",
	"trans_initializer_list", "trans_statement", "trans_label_stmt",
	"trans_expression_stmt", "trans_block_stmt",
	"trans_decl_list_or_stmt_list", "trans_selection_stmt",
	"trans_iteration_stmt", "trans_jump_stmt",
	"trans_attribute_addition_stmt", "trans_translation_unit",
	"trans_elements", "trans_function_definition",
	"trans_procedure_definition",
};
#endif // __DEBUG__

extern void (*semantic_analysis[]) ();
extern void (*code_generation[]) ();

int active_block;

enum number_types { EInt, EReal };
static bool fall_through_seen;
static bool comp_type_var_seen;
static bool decln_seen;
static bool game_seen;
static bool func_arg_seen;
static bool setup_seen;
static bool xor_seen;
static bool xor_first_op_seen;
static bool xor_sec_op_seen;
static bool if_seen;
static bool switch_seen;
static bool case_seen;
static bool while_seen;
// static bool do_while_seen;
static bool if_printed;
static bool main_not_seen = true;
bool last_member_is_comp_type;
bool comp_type_decln_seen;
bool comp_type_member_seen;
static bool attr_seen;
static bool func_ret_type_seen;
static bool func_name_seen;
static bool func_parameter_list_seen;
static bool func_call_name_seen;
static bool func_args_seen;
static bool dot_parent_seen;
static bool assign_seen;
static bool num_players_var_seen;
static bool subscript_seen;
static bool else_seen;
static bool for_loop_seen;
static bool block_stmt_seen;
static bool return_exp_seen;
static bool winner_was_chosen;
static bool first_operand_seen;
static bool second_operand_seen;
static bool modulo_op_numerator_seen;
static bool modulo_op_denominator_seen;
static bool unary_operand_seen;
static bool attr_was_seen;
static bool attr_const_seen;
static bool array_defn_seen;
static bool array_size_expression_seen;
static bool nested_comp_type_decln_seen;

static bool define_int_for_attr;

// static bool first_deck_enum;
static bool comp_type_decln_was_seen;
static bool first_attr_spec;
static bool dont_push_to_deck_now;
static bool dont_cast;
static bool create_temp;
static bool push_to_deck;
static bool vec_already_created;
static bool dont_put_paren;
static bool dont_put_comma;
static bool dont_put_semicolon;
static bool main_func_begun;
static bool first_func_or_proc = true;
static bool dont_cast;
static bool convert_func_arg_into_func_call;
static bool put_flower_bracket;
static bool put_open_paren;
static bool put_close_paren;
static bool dont_insert_empty_lines;
// static bool last_parent_postfix = false;
static bool dont_put_src_line_num;
static bool define_vector;

static bool register_crash_handler;

// static unsigned long long int last_attr_index;
static int num_card_attributes;
static unsigned long long int total_num_attr = 18; // Default number
							// of attributes
// static int num_player_attributes;
// static int num_card_number;
// static int num_card_string;
// static int num_card_bool;
static int num_player_number;
static int num_player_string;
static int num_player_bool;

static unsigned long long int total_parameter_count;
static unsigned long long int parameter_count;

static char *array_size_expression;
static char *array_defn;
static char *to_be_pushed_object;
static char *array_size_expression;
static char *func_arg_type;
static char *first_operand_type;
static char *second_operand_type;
static char *recent_identifier;
static char *opening_brace_indentation;
static const char *unary_operator;
static char *dot_parent_type;
// static char *attr_enums;
static char *func_name;
static char *func_parameter_list;
static char *func_ret_type;
static char *recent_type;
static char *xor_first;
static char *xor_sec;
static char *round_game_elem_identation;
static char *modulo_op_numerator;
static char *modulo_op_denominator;
static char *unary_operand_type;
static char *parameter_type_list;
static char *int_attr_var;
static char *vector_name;


bool main_present;

struct comp_type *last_seen_comp_type;
unsigned long long int num_catalogs = 1;
int cur_comp_type_nesting_level = -1;
static struct comp_type *nested_comp_types[ EMax_comp_type_nesting_limit ];

static void correct_indentation_for_setup(void)
{
	if ( previous_indentation ) {
		if ( strstr(previous_indentation, "\t\t") ) {
			return;
		}

		char s[
		strlen(previous_indentation) +
		strlen("\t") +
		1 ];
		strcpy(s, previous_indentation);
		strcat(s, "\t");

		free(previous_indentation);
		previous_indentation =
				strdup(s);
	} else {
		if ( setup_seen ) {
			previous_indentation = strdup("\t\t");
		} else {
			previous_indentation = strdup("\t");
		}
	}
}

static inline char *str_upper(char *s)
{
	TOUCH();

	char *t = strdup(s);
	char *p = t;
	while ( *t ) {
		if ( *t >= 'a' && *t <= 'z' ) {
			*t -= 32;
		}
		t++;
	}

	return p;
}

static inline char *str_lower(char *s)
{
	TOUCH();

	char *t = strdup(s);
	char *p = t;
	while ( *t ) {
		if ( *t >= 'A' && *t <= 'Z' ) {
			*t += 32;
		}
		t++;
	}

	return p;
}

static void lines_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ELine1:	{
				break;
			}
	case ELine2:	{
				break;
			}
	case ELine3:	{
				break;
			}
	}
}

static void lines_code_generation(void *trans_node)
{
	TOUCH();

	struct lines *l = trans_node;
	int action = l->type;
	switch ( action ) {
	case ELine1:	{
				trans_lines(l->u.l1.l);
				trans_translation_unit(l->u.l1.t);
				INDENT_ONCE();
				fputc('\n', op);

				l->d_type = NULL;
				semantic_analysis[ ELines ](action,
							l->u.l1.l->d_type, 
							l->u.l1.t->d_type);
				break;
			}
	case ELine2:	{
				trans_lines(l->u.l2.l);
				INDENT_ONCE();
				fputc('\n', op);

				l->d_type = NULL;
				semantic_analysis[ ELines ](action,
							l->u.l2.l->d_type);
				break;
			}
	case ELine3:	{	
				break;
			}
	}	
}

static void trans_lines(void *trans_node)
{
	HIT();

	struct lines *l = trans_node;
	switch ( l->type ) {
	case ELine1:
	case ELine2: 	
	case ELine3: 	{
				code_generation[ ELines ](l);
				break;
			}				
	default:	{
				UNKNOWN_TYPE(l->type);
				break;
			}
	}
}

static void define_main_if_not_defined(struct identifier *i)
{
	TOUCH();

	if ( game_seen && main_not_seen ) {
		fprintf(op, "\n\n\nint main(void) ");
		game_seen = false;
		main_not_seen = false;
	} else {
		void *trans_node = i; // For the below macro.
		COMPILATION_ERROR("Redefinition of main\n");
	}
}

static void add_declaration(struct identifier *i)
{
	TOUCH();

	bool symbol_addition_needed = false;

	if ( comp_type_decln_seen && comp_type_member_seen ) {
		if ( nested_comp_type_decln_seen ) {
			add_comp_type(i->u.i1.i, i->src_line);
			last_member_is_comp_type = true;
		} else {
			symbol_addition_needed = true;
		}
	} else if ( comp_type_decln_seen && comp_type_var_seen ) {

		struct unified_type *u = malloc(sizeof(*u));
		if ( u ) {
			u->type = EComp;
			u->u.c = last_seen_comp_type;
			u->u.c->inst = i->u.i1.i;						
			i->d_type = u;
			add_symbol(u, i->src_line);
		}
	} else if ( comp_type_decln_seen ) {
		add_comp_type(i->u.i1.i, i->src_line);
	} else {
		symbol_addition_needed = true;
	}

	if ( symbol_addition_needed ) {
		struct unified_type *u = malloc(sizeof(*u));

		if ( u ) {
			u->type = EBasic;
								
			u->u.s = malloc(sizeof(*u->u.s));

			if ( u->u.s ) {
				strcpy(u->u.s->n, i->u.i1.i);
				u->u.s->t = recent_type;
			}				
			add_symbol(u, i->src_line);
			i->d_type = u;
		}
	}
}

static void push_object(char *obj)
{
	TOUCH();

	if ( func_args_seen && push_to_deck ) {
		if ( !vec_already_created ) {
			fputs("vector<vector<int> > vec;\n", op);
			vec_already_created = true;
		}

		if ( !dont_push_to_deck_now  ) {
			fprintf(op, "\tvec.push_back(%s);\n", obj);
			free(obj);
			obj = NULL;
		} else {
			to_be_pushed_object = obj;
		}
	}
} 

static char *strip_off_trailing_underscores(const char *s)
{
	char *t = strdup(s);
	char *ret = t;

	t += strlen(t) - 1;

	while ( t >= ret && *t == '_' ) {
		t--;
	}

	if ( *(t + 1) == '_' ) {
		*(t + 1) = '\0';
	}

	return ret;
}

static void identifier_code_generator1(void *trans_node)
{
	TOUCH();

	struct identifier *i = trans_node;

	if ( array_defn_seen ) {
		if ( array_size_expression_seen ) {
			size_t l = array_size_expression ?
					strlen(array_size_expression) : 0;
			char *t = realloc(array_size_expression,
						l +
				strlen(i->u.i1.i) +
						1);
			if ( t ) {
				strcpy(t, i->u.i1.i);
				array_size_expression = t;
			}
		} else {

			if ( array_defn ) {
				fputs(array_defn, op);
				free(array_defn);
				array_defn = NULL;
			}

			fputs(i->u.i1.i, op);

			if ( array_size_expression ) {
				fputc('(', op);
				fputs(array_size_expression, op);
				fputc(')', op);
				array_size_expression = NULL;
			}
		}
		return;
	}

	if ( attr_const_seen ) {
		if ( first_attr_spec ) {
			char s[ 100 ] = "";
			sprintf(s, "%llu", total_num_attr);

			char *t = realloc(attr_enums,
					strlen(attr_enums) +
					strlen(i->u.i1.i) +
					strlen(" = ") +
					strlen(s) +
					1);
			if ( t ) {
				sprintf(t + strlen(t),
					"%s = %s",
					i->u.i1.i, s);
				attr_enums = t;
				t = NULL;
			}
			first_attr_spec = false;
		} else {
			const char *s = "";
			if ( !(total_num_attr % 4) ) {
				s = "\n\t";
			}

			char *t = realloc(attr_enums,
					strlen(attr_enums) +
					strlen(", ") +
					strlen(s) +
					strlen(i->u.i1.i) +
					1);
			if ( t ) {
				sprintf(t + strlen(t),
					", %s%s", s, i->u.i1.i);
				attr_enums = t;
				t = NULL;
			}
		}

		total_num_attr++;

		char *t = realloc(new_attr_vec_push,
					strlen(new_attr_vec_push) +
					strlen("\t\t") +
					strlen(vector_name) +
					strlen(".push_back(") +
					strlen(i->u.i1.i) +
					strlen(");\n") +
					1);
		if ( t ) {
			sprintf(t + strlen(t),
				"\t\t%s.push_back(%s);\n", vector_name, i->u.i1.i);
			new_attr_vec_push = t;
			t = NULL;
		}

		char *w_u = strip_off_trailing_underscores(i->u.i1.i);
		if ( w_u ) {
			char *t1 = realloc(new_attr_vec_push,
						strlen(new_attr_vec_push) +
						strlen("\t\t") +
						strlen("arrayOfNames") +
						strlen(".push_back(") +
						strlen("\"") +
						strlen(w_u) +
						strlen("\"") +
						strlen(");\n") +
						1);
			if ( t1 ) {
				sprintf(t1 + strlen(t1),
					"\t\tarrayOfNames.push_back(\"%s\");\n",
									w_u);
				new_attr_vec_push = t1;
				t1 = NULL;
			}

			free(w_u);
			w_u = NULL;
		}

		free(i->u.i1.i);
		i->u.i1.i = NULL;
		return;
	}				
			
	if ( attr_seen ) {
		char *t = realloc(attr_enums, strlen(attr_enums) +
						strlen(i->u.i1.i) +
						1);
		if ( t ) {
			strcat(t, i->u.i1.i);
			free(i->u.i1.i);
			i->u.i1.i = NULL;
			attr_enums = t;
		}
		return;
	}

	if ( !attr_seen && attr_was_seen ) {
		int_attr_var = i->u.i1.i;
		define_int_for_attr = true;
		attr_was_seen = false;
		return;
	}	

	if ( subscript_seen ) {
		if ( array_size_expression ) {
			char *t = realloc(array_size_expression,
					strlen(array_size_expression) +
					strlen(i->u.i1.i) +
					1);
			if ( t ) {
				strcat(t, i->u.i1.i);
				array_size_expression = t;

				free(i->u.i1.i);
				i->u.i1.i = NULL;
			}
			return;
		}
	}

	if ( func_arg_seen ) {
		struct unified_type *ut = type_of(func_name, NULL);
		if ( ut ) {
			func_arg_type = ut->u.s->t;
			return;
		} else {
			func_arg_type = NULL;
		}
	}

	if ( first_operand_seen || second_operand_seen ) {
		if ( !strcmp(i->u.i1.i, "visible") ) {
			if ( first_operand_seen ) {
				first_operand_type = strdup("VISIBILITY");
			} else {
				second_operand_type = strdup("VISIBILITY");
			}	
			fputs("visible", op);
			return;
		}

		struct unified_type *ut = type_of(i->u.i1.i, NULL);
		char *t = NULL;
		if ( ut ) {
			t = ut->type == EBasic ? ut->u.s->t :
				ut->type == EComp ? ut->u.c->tag :
				ut->type == EFunc ? ut->u.f->name :
					strdup("UNKNOWN TYPE!!!!!!");
			free(ut);
			ut = NULL;

			if ( first_operand_seen ) {
				first_operand_type = strdup(t);
			} else {
				second_operand_type = strdup(t);
			}
		}

		bool let_mul_func_dump = false;
		if ( modulo_op_numerator_seen ) {
			modulo_op_numerator = i->u.i1.i;
			let_mul_func_dump = true;
		} else if ( modulo_op_denominator_seen ) {
			modulo_op_denominator = i->u.i1.i;
			let_mul_func_dump = true;
		}
		if ( let_mul_func_dump ) {
			return;
		}
	}

	if ( create_temp ) {
		fseek(op, -1L, SEEK_CUR); // To erase * got from type_spec
		fputs(i->u.i1.i, op);
		fputc('_', op);
		fputs(";\n", op);
		if ( !(i->indentation && *i->indentation) ) {
			i->indentation = previous_indentation;
		} else {
			previous_indentation = i->indentation;
		}
		INDENT();
		fputs(recent_type, op);
		fputs(i->u.i1.i, op);
		recent_identifier = i->u.i1.i;
		return;
	}

	if ( return_exp_seen ) {
		fputs(i->u.i1.i, op);
		return;
	}

	if ( !dont_insert_empty_lines ) {
		INSERT_EMPTY_LINES();
	}

	if ( convert_func_arg_into_func_call ) {
		INDENT_ONCE();
		fputs(i->u.i1.i, op);
		free(i->u.i1.i);
		i->u.i1.i = NULL;
		fputs("()", op);

		convert_func_arg_into_func_call = false;
		return;
	}	

	if ( while_seen ) {
		INDENT_ONCE();
		fputs("for (unsigned long long int __i = ", op);
		dont_cast = true;
	}

	if ( for_loop_seen ) {
		INDENT_ONCE();
		fputs("for (long long int ", op);
		dont_cast = true;
	}
	
	if ( if_seen && !if_printed ) {

		if ( setup_seen ) {
			correct_indentation_for_setup();
		}

		i->indentation = previous_indentation;

		if ( !else_seen && block_stmt_seen ) {
			EMBED_SRC_LINE();
			INDENT_ONCE();
		}

		if ( unary_operator ) {
			i->indentation = previous_indentation;
			INDENT_ONCE();
		}

		fputs("if (", op);
		if_printed = true;


	} else {
		previous_indentation = i->indentation;
		INDENT_ONCE();
	}

	if ( unary_operator ) {
		fputs(unary_operator, op);
		unary_operator = NULL;
	}
	if ( put_open_paren ) {
		fputc('(', op);
		put_open_paren = false;
	}

	if ( assign_seen ) {
		if ( !strcmp(i->u.i1.i, "numPlayers") ) {
			num_players_var_seen = true;
		} else if ( !strcmp(i->u.i1.i, "winner") ) {
			winner_was_chosen = true;
		}
	}

	if ( dot_parent_seen ) {
		if ( !is_var_defined(i->u.i1.i) ) {
			COMPILATION_ERROR("%s not defined\n", i->u.i1.i);
		}
	
		fputs(i->u.i1.i, op);
		
		struct unified_type *ut = type_of(i->u.i1.i, NULL);
		if ( ut ) {
			dot_parent_type = ut->u.s->t;
			printf("dot_parent_type : %s\n", dot_parent_type);
			free(ut);
			ut = NULL;
		} else if ( !strcmp(i->u.i1.i, "player") ||
				!strcmp(i->u.i1.i, "winner") ) {
			dot_parent_type = (char *)"Player *";
		} else if ( !strcmp(i->u.i1.i, "card") ) {
			dot_parent_type = (char *)"Card *";
		}
			
		return;
	}

	if ( func_args_seen && push_to_deck ) {
		push_object(i->u.i1.i);
		return;
	}

	if ( /*!last_parent_postfix && */ func_call_name_seen ) {
		char *f_n = i->u.i1.i;

		func_name = i->u.i1.i;

		if ( !strcmp(f_n, "addToDeck") ) {
			push_to_deck = true;
			dont_put_paren = true;
			dont_put_semicolon = true;
			return;
		} else if ( !strcmp(f_n, "play") ) {
			convert_func_arg_into_func_call = true;
			dont_put_paren = true;
			return;
		} else if ( !strcmp(f_n, "choose") ) {
			fputs("(drawBoard(currPlayer), ", op);
			put_close_paren = true;
		} else if ( !strcmp(f_n, "queryBool") ) {
			fputs("(drawBoard(currPlayer), ", op);
			put_close_paren = true;
		} else if ( !strcmp(f_n, "queryString") ) {
			fputs("(drawBoard(currPlayer), ", op);
			put_close_paren = true;
		} else if ( !strcmp(f_n, "queryNumber") ) {
			fputs("(drawBoard(currPlayer), ", op);
			put_close_paren = true;
		} else if ( !strcmp(f_n, "querySelection") ) {
			fputs("(drawBoard(currPlayer), ", op);
			put_close_paren = true;
		}
		if ( dont_put_paren ) {
			return;
		}
	}

	if ( func_name_seen ) {
		func_name = i->u.i1.i;
		return;
	}

	if ( !strcmp(i->u.i1.i, "main") ) {
		define_main_if_not_defined(i);
	} else {
		if ( decln_seen || comp_type_decln_seen ) {
			add_declaration(i);
		} else { // else-if can be used here.
				// But, this is more intuitive.
			if ( !is_var_defined(i->u.i1.i) ) {
				COMPILATION_ERROR("%s not defined\n",
								i->u.i1.i);
			}
		}

		if ( xor_seen ) {
			if ( xor_first_op_seen ) {
				xor_first = i->u.i1.i;
				xor_first_op_seen = false;
			} else if ( xor_sec_op_seen ) {
				xor_sec = i->u.i1.i;
				xor_sec_op_seen = false;
			}
			xor_seen = false;
		} else {	
			if ( subscript_seen ) {
				fputs("(unsigned long long int)", op);
			}
			printf("off : %zd\n", ftell(op));
			if (switch_seen) {
				fputs("(long long int) ", op);
			}
			fprintf(op, "%s", i->u.i1.i);
			free(i->u.i1.i); // strdup()'d in cgdl.y
			i->u.i1.i = NULL;
		}
	}
}

static void identifier_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EIdentifier1:	{
					break;
				}
	}
}

static void identifier_code_generation(void *trans_node)
{
	TOUCH();

	struct identifier *i = trans_node;
	int action = i->type;
	switch ( action ) {
	case EIdentifier1:	{
					identifier_code_generator1(i);

					semantic_analysis[ EIdentifier ]
							(action, i->d_type);
					break;
				}
	}
}

static void trans_identifier(void *trans_node)
{
	HIT();

	struct identifier *i = trans_node;
	switch ( i->type ) {
	case EIdentifier1:	{
					code_generation[ EIdentifier ](i);
					break;
				}
	default:		{
					UNKNOWN_TYPE(i->type);
					break;
				}
	}
}

static void constant_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EConst1:	{
				break;
			}
	case EConst2:	{
				break;
			}
	case EConst3:	{
				break;
			}
	case EConst4:	{
				break;
			}
	case EConst5:	{
				break;
			}
	}
}

static void populate_array_expression(void *trans_node, char *s)
{
	if ( array_defn_seen ) {
		if ( array_size_expression_seen ) {
			size_t l = array_size_expression ?
					strlen(array_size_expression) : 0;
			char *t = realloc(array_size_expression,
						l +
					strlen(s) +
						1);
			if ( t ) {
				strcpy(t, s);
				array_size_expression = t;
			}
		} else {
			if ( array_defn ) {				
				INDENT_ONCE();
				fputs(array_defn, op);
				free(array_defn);
				array_defn = NULL;
			}

			if ( array_size_expression ) {
				fputc('(', op);
				fputs(array_size_expression, op);
				fputc(')', op);
				array_size_expression = NULL;
			}
		}
		return;
	}
}

static void constant_code_generation(void *trans_node)
{
	TOUCH();

	struct constant *cn = trans_node;
	int action = cn->type;
	switch ( action ) {
	case EConst1:	{
				cn->d_type = unification(strdup("long double"),
								cn->u.c1.r,
								EBasic);

				if ( subscript_seen ) {
					if ( strchr(cn->u.c1.r, '.') ) {
						COMPILATION_ERROR
						("Array size specifier"
						" must be a non-negative"
						" integer\n");
					}
				}

				if ( case_seen ) {

					if ( strchr(cn->u.c1.r, '.') ) {
						COMPILATION_ERROR
						("Case label"
						" must be an integer\n");
					}
				}

				if ( switch_seen ) {

					if ( strchr(cn->u.c1.r, '.') ) {
						COMPILATION_ERROR
						("Switch lookup expression"
						" must be an integer\n");
					}
				}

/*				if ( array_size_expression ) {
					char *t =
						realloc(array_size_expression,
						strlen(array_size_expression) +
						strlen(cn->u.c1.r) +
						1);
					if ( t ) {
						strcat(t, cn->u.c1.r);
						free(cn->u.c1.r);
						cn->u.c1.r = NULL;
					}
					return;
				}
*/

				if ( array_defn_seen ) {
					populate_array_expression(trans_node,
								cn->u.c1.r);
					free(cn->u.c1.r);
					cn->u.c1.r = NULL;
					return;
				}

				if ( func_arg_seen ) {
					func_arg_type = (char *)"long double";
				}

				semantic_analysis[ EConstant ](action,
								cn->d_type);


				if ( while_seen ) {
					INDENT_ONCE();
					fputs(
					"for (unsigned long long int __i = ",
									op);
					
					dont_cast = true;
				}


				if ( first_operand_seen ) {
					first_operand_type =
							strdup("long double");
				} else if ( second_operand_seen ) {
					second_operand_type =
							strdup("long double");
				}


				if ( !subscript_seen && !dont_cast ) {
					INDENT();
					if (!case_seen) {
						fputs("(long double)", op);
					}
				}

				if ( dont_cast ) {
					dont_cast = false;
				}

				if ( func_args_seen && push_to_deck ) {
					push_object(cn->u.c1.r);
					cn->u.c1.r = NULL;
				} else {
					INDENT_ONCE();
					fputs(cn->u.c1.r, op);
					free(cn->u.c1.r);
					cn->u.c1.r = NULL;
				}


				break;				
			}
	case EConst2:	{
				cn->d_type = unification(
						strdup("string-literal"),
								cn->u.c2.s,
								EBasic);

				if ( func_arg_seen ) {
					func_arg_type = (char *)"string";
				}

				semantic_analysis[ EConstant ]
						(action, cn->d_type);


				if ( first_operand_seen ) {
					first_operand_type = strdup("string");
				} else if ( second_operand_seen ) {
					second_operand_type = strdup("string");
				}


				if ( func_args_seen && push_to_deck ) {
					push_object(cn->u.c2.s);
					cn->u.c2.s = NULL;
				} else {
					INDENT_ONCE();
					fprintf(op, "%s", cn->u.c2.s);
					free(cn->u.c2.s); // strdup()'d in cgdl.l
					cn->u.c2.s = NULL;
				}

				break;
			}
	case EConst3:	{
				trans_visibility_const(cn->u.c3.v);
	
				if ( func_arg_seen ) {
					func_arg_type = (char *)"VISIBILITY";
				}

				semantic_analysis[ EConstant ](action,
							cn->u.c3.v->d_type);
				break;
			}
	case EConst4:	{
				char str_bool[ 100 ] = "";
				sprintf(str_bool, "%s",
						cn->u.c4.b ? "true" : "false");
				cn->d_type = unification(strdup("bool"),
							strdup(str_bool),
								EBasic);

				if ( func_arg_seen ) {
					func_arg_type = (char *)"bool";
				}

				if ( first_operand_seen ) {
					first_operand_type = strdup("bool");
				} else if ( second_operand_seen ) {
					second_operand_type = strdup("bool");
				}

				semantic_analysis[ EConstant ]
						(action, cn->d_type);

				if ( func_args_seen && push_to_deck ) {
					push_object(strdup(str_bool));
				} else {
					INDENT_ONCE();
					fprintf(op, "%s", str_bool);
				}
				break;
			}
	case EConst5:	{
				if ( func_arg_seen ) {
					func_arg_type = (char *)"NULL CONSTANT";
				}

				if ( first_operand_seen ) {
					first_operand_type =
						strdup("NULL CONSTANT");
				} else if ( second_operand_seen ) {
					second_operand_type =
						strdup("NULL CONSTANT");
				}

				if ( func_args_seen && push_to_deck ) {
					push_object(strdup("NONE_ATTR"));
				} else {
					fputs("NULL", op);
				}
				break; // Dummy
			}
	}
}

static void trans_constant(void *trans_node)
{
	HIT();

	struct constant *cn = trans_node;
	switch ( cn->type ) {
	case EConst1:	
	case EConst2:	
	case EConst3:	
	case EConst4:	
	case EConst5:	{
				code_generation[ EConstant ](cn);
				break;
			}
	default:	{
				UNKNOWN_TYPE(cn->type);
				break;
			}
	}
}

static void visibility_const_semantic_analysis(int action,
						struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EVisibility1:	
	case EVisibility2:	
	case EVisibility3:	{
					if ( first_operand_seen ) {
						first_operand_type =
							strdup("VISIBILITY");
					} else if ( second_operand_seen ) {
						second_operand_type =
							strdup("VISIBILITY");
					}
					break;
				}
	}
}

static void visibility_const_code_generation(void *trans_node)
{
	TOUCH();

	struct visibility_const *v = trans_node;
	int action = v->type;
	switch ( action ) {
	case EVisibility1:	{
					v->d_type = unification(strdup("All"),
							strdup("Visibility"),
							EBasic);
					semantic_analysis[ EVisibility ]
								(action,
								v->d_type);

					if ( func_args_seen && push_to_deck ) {
						push_object(strdup("all"));
					} else {
						fprintf(op, "all");
					}
					break;
				}
	case EVisibility2:	{
					v->d_type = unification(strdup("NoOne"),
							strdup("Visibility"),
							EBasic);
					semantic_analysis[ EVisibility ]
								(action,
								v->d_type);

					if ( func_args_seen && push_to_deck ) {
						push_object(strdup("noOne"));
					} else {
						fprintf(op, "noOne");
					}
					break;
				}
	case EVisibility3:	{
					v->d_type = unification(strdup("Self"),
							strdup("Visibility"),
							EBasic);
					semantic_analysis[ EVisibility ]
								(action,
								v->d_type);


					if ( func_args_seen && push_to_deck ) {
						push_object(strdup("self"));
					} else {
						fprintf(op, "self");
					}
					break;
				}
	}
}

static void trans_visibility_const(void *trans_node)
{
	HIT();

	struct visibility_const *v = trans_node;
	switch ( v->type ) {
	case EVisibility1:
	case EVisibility2:
	case EVisibility3:	{
					code_generation[ EVisibility ](v);
					break;
				}
	default:		{
					UNKNOWN_TYPE(v->type);
					break;
				}
	}
}

static void primary_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EPrimary1:	{
				break;
			}
	case EPrimary2:	{
				break;
			}
	case EPrimary3:	{
				break;
			}
	}
}

static void primary_code_generation(void *trans_node)
{
	TOUCH();

	struct primary_expression *p = trans_node;
	int action = p->type;
	switch ( action ) {
	case EPrimary1:	{
				trans_identifier(p->u.p1.i);
				p->d_type = p->u.p1.i->d_type;

				semantic_analysis[ EPrimary ](action,
							p->u.p1.i->d_type);
				break;
			}
	case EPrimary2: {
				trans_constant(p->u.p2.c);
				p->d_type = p->u.p2.c->d_type;

				semantic_analysis[ EPrimary ](action,
							p->u.p2.c->d_type);
				break;
			}
	case EPrimary3: {
				if ( (if_seen && if_printed) || !if_seen ) {
					if ( unary_operator ) {
						fputs(unary_operator, op);
						unary_operator = NULL;
					}
					fputc('(', op);
				} else {
					put_open_paren = true;
				}
				trans_expression(p->u.p3.e);
				p->d_type = p->u.p3.e->d_type;
				fputc(')', op);

				semantic_analysis[ EPrimary ](action,
							p->u.p3.e->d_type);
				break;
			}
	}
}

static void trans_primary(void *trans_node)
{
	HIT();

	struct primary_expression *p = trans_node;
	switch ( p->type ) {
	case EPrimary1:
	case EPrimary2:
	case EPrimary3:	{
				code_generation[ EPrimary ](p);
				break;
			}
	default:	{
				UNKNOWN_TYPE(p->type);
				break;
			}
	}
}

static void postfix_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2,
						struct unified_type *t3)
{
	TOUCH();

	switch ( action ) {
	case EPostfix1:	{
				break;
			}
	case EPostfix2:	{
				break;
			}
	case EPostfix3:	{
				break;
			}
	case EPostfix4:	{
				break;
			}
	case EPostfix5:	{
				break;
			}
	case EPostfix6:	{
				break;
			}
	}
}

static bool card_attribute_mapping(struct postfix_expression *p)
{
	TOUCH();

	bool added = false;

	char s[ 100 ] = "CARD_";
	strcat(s, p->u.p6.i->u.i1.i);
	char *t = str_upper(s);

	char sub[ strlen(t) + 1/* Comma */ + 1/* NUL */ ];
	strcpy(sub, t);
	strcat(sub, ",");

	if ( strstr(enum_card_attr, sub) ) {
		fputs("attributes[", op);
		fputs(t, op);
		free(t);
		t = NULL;
		fputc(']', op);
		free(p->u.p6.i->u.i1.i);
		p->u.p6.i->u.i1.i = NULL;

		added = true;
	}

	return added;
}
static bool player_field_mapping(struct postfix_expression *p)
{
	TOUCH();

	bool added = false;

	char s[ 100 ] = "PLAYER_";
	strcat(s, p->u.p6.i->u.i1.i);
	char *t = str_upper(s);

	char sub[ strlen(t) + 1/* Comma */ + 1/* NUL */ ];
	strcpy(sub, t);
	strcat(sub, ",");

	if ( strstr(enum_player_number, sub) ) {
		fputs("numbers[", op);
		fputs(t, op);
		free(t);
		t = NULL;
		fputc(']', op);
		free(p->u.p6.i->u.i1.i);
		p->u.p6.i->u.i1.i = NULL;

		added = true;
	} else if ( strstr(enum_player_bool, sub) ) {
		fputs("bools[", op);
		fputs(t, op);
		free(t);
		t = NULL;
		fputc(']', op);
		free(p->u.p6.i->u.i1.i);
		p->u.p6.i->u.i1.i = NULL;

		added = true;
	}

	return added;
}

static void postfix_code_generation(void *trans_node)
{
	TOUCH();

	struct postfix_expression *p = trans_node;
	int action = p->type;
	switch ( action ) {
	case EPostfix1:	{
				trans_primary(p->u.p1.p);
				p->d_type = p->u.p1.p->d_type;

				semantic_analysis[ EPostfix ]
							(action,
							p->u.p1.p->d_type);							
				break;
			}
	case EPostfix2: {
				trans_postfix(p->u.p2.p);
				p->d_type = p->u.p2.p->d_type;
	
				fputc('[', op);
				subscript_seen = true;
				trans_expression(p->u.p2.e);
				subscript_seen = false;
				fputc(']', op);

				semantic_analysis[ EPostfix ]
							(action,
							p->u.p2.p->d_type,
							p->u.p2.e->d_type);
				break;
			}
	case EPostfix3: {
				INDENT_ONCE();
				
				dont_push_to_deck_now = true;
				trans_postfix(p->u.p3.p);
				dont_push_to_deck_now = false;
				p->d_type = p->u.p3.p->d_type;

				fputs("vec.push_back(subset(", op);
				fputs(to_be_pushed_object, op);
				fputs(", ", op);

				bool save = push_to_deck;
				push_to_deck = false;

				dont_cast = true;
				trans_expression(p->u.p3.e1);
				dont_cast = false;

				fputs(", ", op);

				dont_cast = true;
				trans_expression(p->u.p3.e2);
				dont_cast = false;
				
				push_to_deck = save;

				fputs("));\n", op);

				semantic_analysis[ EPostfix ]
							(action,
							p->u.p3.p->d_type,
							p->u.p3.e1->d_type,
							p->u.p3.e2->d_type);
				break;
			}
	case EPostfix4: {
				trans_postfix(p->u.p4.p);
				p->d_type = p->u.p4.p->d_type; // Func return
								// type.
				fputc('(', op);
				fputc(')', op);
				// newlines_needed = p->num_of_empty_lines;
				// INSERT_EMPTY_LINES();

				semantic_analysis[ EPostfix ]
							(action,
							p->u.p4.p->d_type);
				break;
			}
	case EPostfix5: {
				func_call_name_seen = true;
				trans_postfix(p->u.p5.p);
				func_call_name_seen = false;
				p->d_type = p->u.p5.p->d_type; // Func return
								// type.

				if ( !dont_put_paren ) {
					fputc('(', op);
				}

				func_args_seen = true;
				if ( dont_put_paren ) {
					dont_put_comma = true;
				}

				struct unified_type *ut =
					type_of(func_name, NULL);
				if ( ut ) {
					if ( ut->type != EFunc ) {
						COMPILATION_ERROR
						("%s not a function\n",
								func_name);
					}

					parameter_type_list =
						ut->u.f->parameter_list;
					total_parameter_count = 1;
	
					if ( strcmp(parameter_type_list,
								"void") ) {
						char *t = parameter_type_list;
						while ( *t &&
							(t = strchr(t, '|')) ) {
							total_parameter_count++;
							t++;
						}
					} else {
						total_parameter_count = 0;
					}	
				}

				trans_arg_exp_list(p->u.p5.a);

				if ( parameter_count < total_parameter_count ) {
					COMPILATION_ERROR
					("Too few arguments to %s\n",
								func_name);
				}

				// Reset
				total_parameter_count = 0;
				parameter_count = 0;

				if ( dont_put_comma ) {
					dont_put_comma = false;
				}
				func_args_seen = false;

				if ( !dont_put_paren ) {
					fputc(')', op);
				} else {
					dont_put_paren = false;
				}


				if ( push_to_deck ) {
					fputs("\taddToDeck(vec);\n", op);
					fputs("\tvec.clear();\n", op);
					push_to_deck = false;
/*
					deck_enums = realloc(deck_enums,
							strlen(deck_enums) +
							strlen(" };\n") +
							1);
					if ( deck_enums ) {
						strcat(deck_enums, " };\n");
						fputs(deck_enums, op);
						free(deck_enums);
						deck_enums = NULL;

					}
*/
				}
				// newlines_needed = p->num_of_empty_lines;
				// INSERT_EMPTY_LINES();

				semantic_analysis[ EPostfix ](action,
							p->u.p5.p->d_type,
							p->u.p5.a->d_type);	
				break;
			}
	case EPostfix6: {

//				last_parent_postfix =
//						p->u.p6.p->type == EPostfix6;

				dot_parent_seen = true;
				trans_postfix(p->u.p6.p);
				dot_parent_seen = false;

				// parent_type = p->d_type;
				// fputc('.', op);
				if ( dot_parent_type &&
					!strcmp(dot_parent_type, "RECORD") ) {
					fputc('.', op);
				} else {
					fputs("->", op);
				}

				if ( /*!last_parent_postfix
							&& */ dot_parent_type ) {
					bool attr_added = false;
					bool field_added = false;

					if ( !strcmp(dot_parent_type,
							"Card *") ) {
						attr_added =
						card_attribute_mapping(p);

						/*if ( !attr_added ) {
							field_added =
							card_field_mapping(p);
							}*/
					} else if ( !strcmp(dot_parent_type,
							"Player *") ) {
					  /*attr_added =
						player_attribute_mapping(p);

						if ( !attr_added ) {*/
							field_added =
							player_field_mapping(p);
							/*}*/
					}

					if ( !attr_added && !field_added ) {
						trans_identifier(p->u.p6.i);
					}

					dot_parent_type = NULL;
				} else {
					trans_identifier(p->u.p6.i);
				}
					
				p->d_type = p->u.p6.i->d_type;
				// parent_type = NULL;

				semantic_analysis[ EPostfix ](action,
							p->u.p6.p->d_type,
							p->u.p6.i->d_type);	
				break;
			}
	}
}

static void trans_postfix(void *trans_node)
{
	HIT();

	struct postfix_expression *p = trans_node;
	switch ( p->type ) {
	case EPostfix1:
	case EPostfix2:
	case EPostfix3:
	case EPostfix4:
	case EPostfix5:
	case EPostfix6:	{
				code_generation[ EPostfix ](p);
				break;
			}
	default:	{
				UNKNOWN_TYPE(p->type);
				break;
			}
	}
}

static void arg_exp_list_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EArg1:	{
				break;
			}
	case EArg2:	{
				break;
			}
	}
}

static void check_func_arg_parameter_types(void *trans_node)
{
	if ( !total_parameter_count ||
		(parameter_count == total_parameter_count) ) {
		COMPILATION_ERROR("Too many arguments for %s\n", func_name);
	} else if ( parameter_type_list ) {
		parameter_count++;

		char *l_t = parameter_type_list;

		if ( strcmp(func_arg_type, l_t) ) {
			COMPILATION_ERROR
			("Argument %llu of type %s"
			" being passed to %s doesn't"
			" match the expected parameter"
			" type %s\n", parameter_count,
			func_arg_type, func_name, l_t);
		}

		char *n_t = strchr(parameter_type_list, '|');
		if ( n_t ) {
			parameter_type_list = n_t + 1;
		}
	}
}

static void arg_exp_list_code_generation(void *trans_node)
{
	TOUCH();

	struct arg_exp_list *a = trans_node;
	int action = a->type;
	switch ( action ) {
	case EArg1:	{
				func_arg_seen = true;
				trans_assignment_expression(a->u.a1.as);
				func_arg_seen = false;
				a->d_type = a->u.a1.as->d_type;

				check_func_arg_parameter_types(trans_node);

				semantic_analysis[ EArg_exp_list ](action,
							a->u.a1.as->d_type);
				break;
			}
	case EArg2:	{
				func_arg_seen = true;
				trans_arg_exp_list(a->u.a2.al);
				if ( !dont_put_comma ) {
					fputc(',', op);
					fputc(' ', op);
				}

				check_func_arg_parameter_types(trans_node);

				trans_assignment_expression(a->u.a2.as);

				check_func_arg_parameter_types(trans_node);

				func_arg_seen = false;

				a->d_type = a->u.a2.as->d_type;

				semantic_analysis[ EArg_exp_list ](action,
							a->u.a2.al->d_type,
							a->u.a2.as->d_type);
				break;
			}
	}
}

static void trans_arg_exp_list(void *trans_node)
{
	HIT();

	struct arg_exp_list *a = trans_node;
	switch ( a->type ) {
	case EArg1:
	case EArg2:	{
				code_generation[ EArg_exp_list ](a);
				break;
			}
	default:	{
				UNKNOWN_TYPE(a->type);
				break;
			}
	}
}

static void unary_expression_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EUnary1:	{
				break;
			}
	case EUnary2:	{
				break;
			}
	}
}

static void unary_expression_code_generation(void *trans_node)
{
	TOUCH();

	struct unary_expression *u = trans_node;
	int action = u->type;
	switch ( action ) {
	case EUnary1:	{
				trans_postfix(u->u.u1.p);
				u->d_type = u->u.u1.p->d_type;

				semantic_analysis[ EUnary_exp ](action,
							u->u.u1.p->d_type);
				break;
			}
	case EUnary2:	{
				trans_unary_operator(u->u.u2.uo);

				unary_operand_seen = true;
				trans_unary_expression(u->u.u2.ue);
				unary_operand_seen = false;

				if ( unary_operand_type ) {
					bool n_t = !strcmp(unary_operand_type,
							"long double");
					bool b_t = !strcmp(unary_operand_type,
							"bool");	

					if ( !n_t && !b_t ) {
						COMPILATION_ERROR
						("Invalid operand of type %s "
						"to unary operator %s\n",
						unary_operand_type,
						unary_operator);
					}

					unary_operand_type = NULL;
				}

				u->d_type = u->u.u2.ue->d_type;

				semantic_analysis[ EUnary_exp ](action,
							u->u.u2.uo->d_type,
							u->u.u2.ue->d_type);
				break;
			}
	}
}

static void trans_unary_expression(void *trans_node)
{
	HIT();

	struct unary_expression *u = trans_node;
	switch ( u->type ) {
	case EUnary1:
	case EUnary2:	{
				code_generation[ EUnary_exp ](u);
				break;
			}
	default:	{
				UNKNOWN_TYPE(u->type);
				break;
			}
	}
}


static void unary_operator_semantic_analysis(int action,
						struct unified_type *t1)
{
	TOUCH();
	
	switch ( action ) {
	case EUnary_op1:	{
					break;
				}
	case EUnary_op2:	{
					break;
				}
	case EUnary_op3:	{
					break;
				}
	}
}

static void unary_operator_code_generation(void *trans_node)
{
	TOUCH();

	struct unary_operator *u = trans_node;
	int action = u->type;
	switch ( action ) {
	case EUnary_op1:	{
					u->d_type =
					unification(strdup("Unary Operator"),
							strdup("&"), EBasic);

					semantic_analysis[ EUnary_op ](action,
								u->d_type);

					unary_operator = "&";
					break;
				}
	case EUnary_op2:	{
					u->d_type =
					unification(strdup("Unary Operator"),
							strdup("-"), EBasic);

					semantic_analysis[ EUnary_op ](action,
								u->d_type);

					unary_operator = "-";
					break;
				}
	case EUnary_op3:	{
					u->d_type =
					unification(strdup("Unary Operator"),
							strdup("!"), EBasic);

					semantic_analysis[ EUnary_op ](action,
								u->d_type);

					unary_operator = "!";
					u->d_type = NULL;
					break;
				}
	}
}

static void trans_unary_operator(void *trans_node)
{
	HIT();

	struct unary_operator *u = trans_node;
	switch ( u->type ) {
	case EUnary_op1:
	case EUnary_op2:
	case EUnary_op3:	{
					code_generation[ EUnary_op ](u);
					break;
				}
	default:		{
					UNKNOWN_TYPE(u->type);
					break;
				}
	}
}

static void multiplicative_expression_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2,
						unsigned long long int src_line)
{
	TOUCH();

	switch ( action ) {
	case EMul1:	{
				break;
			}
	case EMul2:	{
				break;
			}
	case EMul3:	{
				break;
			}
	case EMul4:	{
				break;
			}
	}
}

static void check_mul_operand_types(void *trans_node, char opr)
{
	TOUCH();

	if ( first_operand_type && second_operand_type ) {
		bool f_n = !strcmp(first_operand_type, "long double");
		bool s_n = !strcmp(second_operand_type, "long double");

		if ( !f_n && !s_n ) {
			COMPILATION_ERROR
			("Invalid combination of operands of types : "
			"%s and %s\n to binary operator %c\n",
			first_operand_type, second_operand_type, opr);
		} else if ( !f_n ) {
			COMPILATION_ERROR
			("Invalid first operand of type %s to binary"
			" operator %c\n", first_operand_type, opr);
		} else if ( !s_n ) {
			COMPILATION_ERROR
			("Invalid second operand of type %s to binary"
			" operator %c\n", second_operand_type, opr);
		}
	}

	first_operand_type = NULL;
	second_operand_type = NULL;
}

static void multiplicative_expression_code_generation(void *trans_node)
{
	TOUCH();

	struct multiplicative_expression *m = trans_node;
	int action = m->type;
	switch ( action ) {
	case EMul1:	{
				trans_unary_expression(m->u.m1.u);
				m->d_type = m->u.m1.u->d_type;

				semantic_analysis[ EMultiplicative_exp ](action,
							m->u.m1.u->d_type,
								m->src_line);
				break;
			}
	case EMul2:	{
				first_operand_seen = true;
				trans_multiplicative_expression(m->u.m2.m);
				first_operand_seen = false;

				// No type promotions and the like. So,
				// pick any one operand's type here.
				m->d_type = m->u.m2.m->d_type;

				fputs(" * ", op);

				second_operand_seen = true;
				trans_unary_expression(m->u.m2.u);
				second_operand_seen = false;

				check_mul_operand_types(m, '*');
	
				semantic_analysis[ EMultiplicative_exp ](action,
							m->u.m2.m->d_type,
							m->u.m2.u->d_type);
				break;
			}
	case EMul3:	{
				first_operand_seen = true;
				trans_multiplicative_expression(m->u.m3.m);
				first_operand_seen = false;

				m->d_type = m->u.m3.m->d_type;

				fputs(" / ", op);

				second_operand_seen = true;
				trans_unary_expression(m->u.m3.u);
				second_operand_seen = false;

				check_mul_operand_types(m, '/');

				semantic_analysis[ EMultiplicative_exp ](
							action,
							m->u.m3.m->d_type,
							m->u.m3.u->d_type);
				break;
			}
	case EMul4:	{
				first_operand_seen = true;
				modulo_op_numerator_seen = true;
				trans_multiplicative_expression(m->u.m4.m);
				modulo_op_numerator_seen = false;
				first_operand_seen = false;

				m->d_type = m->u.m4.m->d_type;

				fputs(" % ", op);

				second_operand_seen = true;
				modulo_op_denominator_seen = true;
				trans_unary_expression(m->u.m4.u);
				modulo_op_denominator_seen = false;
				second_operand_seen = false;

				check_mul_operand_types(m, '%');

				fprintf(op, "fmodl(%s, %s)",
						modulo_op_numerator,
						modulo_op_denominator);
				free(modulo_op_numerator);
				free(modulo_op_denominator);
				modulo_op_numerator = NULL;
				modulo_op_denominator = NULL;

				semantic_analysis[ EMultiplicative_exp ](
							action,
							m->u.m4.m->d_type,
							m->u.m4.u->d_type);
				break;
			}
	}
}

static void trans_multiplicative_expression(void *trans_node)
{
	HIT();

	struct multiplicative_expression *m = trans_node;
	switch ( m->type ) {
	case EMul1:
	case EMul2:
	case EMul3:
	case EMul4:	{
				code_generation[ EMultiplicative_exp ](m);
				break;
			}
	default:	{
				UNKNOWN_TYPE(m->type);
				break;
			}
	}
}

static void additive_expression_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EAdd1:	{
				break;
			}
	case EAdd2:	{
				break;
			}
	case EAdd3:	{
				break;
			}
	} 
}

static void additive_expression_code_generation(void *trans_node)
{
	TOUCH();

	struct additive_expression *a = trans_node;
	int action = a->type;
	switch ( action ) {
	case EAdd1:	{
				trans_multiplicative_expression(a->u.a1.m);
				a->d_type = a->u.a1.m->d_type;

				semantic_analysis[ EAdditive_exp ](action,
							a->u.a1.m->d_type);
				break;
			}
	case EAdd2:	{
				first_operand_seen = true;
				trans_additive_expression(a->u.a2.a);
				first_operand_seen = false;
				a->d_type = a->u.a2.a->d_type;
				fputs(" + ", op);
				second_operand_seen = true;
				trans_multiplicative_expression(a->u.a2.m);
				second_operand_seen = false;

				if ( first_operand_type
					&& second_operand_type ) {
					bool f_number =
						!strcmp(first_operand_type,
							"long double");
					bool f_string =
						!strcmp(first_operand_type,
							"string");
					bool s_number =
						!strcmp(second_operand_type,
							"long double");
					bool s_string =
						!strcmp(second_operand_type,
							"string");

					if (!f_number && !f_string &&
						!s_number && !s_string) {
						COMPILATION_ERROR(
						"Invalid combination of "
						"operands of types : %s and %s"
						" to binary operator +\n",
							first_operand_type,
							second_operand_type);
					} else if (!f_number && !f_string) {
						COMPILATION_ERROR
						("Invalid first operand"
						" of type %s to binary "
						"operator +\n",
							first_operand_type);
					} else if (!s_number && !s_string) {
						COMPILATION_ERROR
						("Invalid second operand"
						" of type %s to binary "
						"operator +\n",
							second_operand_type);
					}	
				}

				first_operand_type = NULL;
				second_operand_type = NULL;

				semantic_analysis[ EAdditive_exp ](action,
							a->u.a2.a->d_type,
							a->u.a2.m->d_type);
				break;
			}
	case EAdd3:	{
				first_operand_seen = true;
				trans_additive_expression(a->u.a3.a);
				first_operand_seen = false;
				a->d_type = a->u.a3.a->d_type;
				fputs(" - ", op);
				second_operand_seen = true;
				trans_multiplicative_expression(a->u.a3.m);
				second_operand_seen = false;

				if ( first_operand_type
						&& second_operand_type ) {
					bool f_number =
						!strcmp(first_operand_type,
							"long double");
					bool s_number =
						!strcmp(second_operand_type,
							"long double");
				
					if ( !f_number && !s_number ) {	
						COMPILATION_ERROR
					("Invalid combination of operands of"
					" types %s and %s to binary operator"
					" -\n", first_operand_type,
						second_operand_type);
					} else if ( !f_number ) {
						COMPILATION_ERROR
					("Invalid first operand of"
					" type %s to binary operator"
					" -\n", first_operand_type);
					} else if ( !s_number ) {
						COMPILATION_ERROR
					("Invalid second operand of"
					" type %s to binary operator"
					" -\n", second_operand_type);
					}
				}

				first_operand_type = NULL;
				second_operand_type = NULL;
					
				semantic_analysis[ EAdditive_exp ](action,
							a->u.a3.a->d_type,
							a->u.a3.m->d_type);
				break;
			}
	}
}

static void trans_additive_expression(void *trans_node)
{
	HIT();

	struct additive_expression *a = trans_node;
	switch ( a->type ) {
	case EAdd1:
	case EAdd2:
	case EAdd3:	{
				code_generation[ EAdditive_exp ](a);
				break;
			}
	default:	{
				UNKNOWN_TYPE(a->type);
				break;
			}
	}
}

static void relational_expression_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ERel1:	{
				break;
			}
	case ERel2:	{
				break;
			}
	case ERel3:	{
				break;
			}
	case ERel4:	{
				break;
			}
	case ERel5:	{
				break;
			}
	}
}

static void check_rel_op_types(void *trans_node, const char *opr)
{
	TOUCH();

	if ( first_operand_type && second_operand_type ) {
		bool f_n = !strcmp(first_operand_type, "long double");
		bool s_n = !strcmp(second_operand_type, "long double");

		if ( !f_n && !s_n ) {
			COMPILATION_ERROR
			("Invalid operands of types :"
			" %s and %s to binary operator %s\n",
					first_operand_type,
					second_operand_type, opr);
		} else if ( !f_n ) {
			COMPILATION_ERROR
			("Invalid first operand of type"
			" %s to binary operator %s\n",
					first_operand_type, opr);
		} else if ( !s_n ) {
			COMPILATION_ERROR
			("Invalid second operand of type"
			" %s to binary operator %s\n",
					second_operand_type, opr);
		}
	}

	first_operand_type = NULL;
	second_operand_type = NULL;
}

static void relational_expression_code_generation(void *trans_node)
{
	TOUCH();

	struct relational_expression *r = trans_node;
	int action = r->type;
	switch ( action ) {
	case ERel1:	{
				trans_additive_expression(r->u.r1.a);
				r->d_type = r->u.r1.a->d_type;

				semantic_analysis[ ERelational_exp ](action,
							r->u.r1.a->d_type);
				break;
			}
	case ERel2:	{
				first_operand_seen = true;
				trans_relational_expression(r->u.r2.r);
				first_operand_seen = false;
				r->d_type = r->u.r2.r->d_type;
				fputs(" < ", op);
				second_operand_seen = true;
				trans_additive_expression(r->u.r2.a);
				second_operand_seen = false;

				check_rel_op_types(trans_node, "<");

				semantic_analysis[ ERelational_exp ](action,
							r->u.r2.r->d_type,
							r->u.r2.a->d_type);
				break;
			}
	case ERel3:	{
				first_operand_seen = true;
				trans_relational_expression(r->u.r3.r);
				first_operand_seen = false;
				r->d_type = r->u.r3.r->d_type;
				fputs(" > ", op);
				second_operand_seen = true;
				trans_additive_expression(r->u.r3.a);
				second_operand_seen = false;

				check_rel_op_types(trans_node, ">");

				semantic_analysis[ ERelational_exp ](action,
							r->u.r3.r->d_type,
							r->u.r3.a->d_type);
				break;
			}
	case ERel4:	{
				first_operand_seen = true;	
				trans_relational_expression(r->u.r4.r);
				first_operand_seen = false;
				r->d_type = r->u.r4.r->d_type;
				fputs(" <= ", op);
				second_operand_seen = true;
				trans_additive_expression(r->u.r4.a);
				second_operand_seen = false;

				check_rel_op_types(trans_node, "<=");

				semantic_analysis[ ERelational_exp ](action,
							r->u.r4.r->d_type,
							r->u.r4.a->d_type);
				break;
			}
	case ERel5:	{
				first_operand_seen = true;
				trans_relational_expression(r->u.r5.r);
				first_operand_seen = false;
				r->d_type = r->u.r5.r->d_type;
				fputs(" >= ", op);
				second_operand_seen = true;
				trans_additive_expression(r->u.r5.a);
				second_operand_seen = false;

				check_rel_op_types(trans_node, ">=");

				semantic_analysis[ ERelational_exp ](action,
							r->u.r5.r->d_type,
							r->u.r5.a->d_type);
				break;
			}
	}	
}

static void trans_relational_expression(void *trans_node)
{
	HIT();

	struct relational_expression *r = trans_node;
	switch ( r->type ) {
	case ERel1:
	case ERel2:
	case ERel3:
	case ERel4:
	case ERel5:	{
				code_generation[ ERelational_exp ](r);
				break;
			}
	default:	{
				UNKNOWN_TYPE(r->type);
				break;
			}
	}
}

static void equality_expression_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EEqual1:	{
				break;
			}
	case EEqual2:	{
				break;
			}
	case EEqual3:	{
				break;
			}
	}
}

static void check_equality_op_types(void *trans_node, const char *opr)
{
	TOUCH();

	if ( first_operand_type && second_operand_type ) {
		bool f_n = !strcmp(first_operand_type, "long double");
		bool f_s = !strcmp(first_operand_type, "string");
		bool f_b = !strcmp(first_operand_type, "bool");
		bool f_v = !strcmp(first_operand_type, "VISIBILITY");

		bool s_n = !strcmp(first_operand_type, "long double");
		bool s_s = !strcmp(first_operand_type, "string");
		bool s_b = !strcmp(first_operand_type, "bool");
		bool s_v = !strcmp(first_operand_type, "VISIBILITY");

		bool not_same_valid_type = strcmp(first_operand_type,
							second_operand_type);

		if ( !f_n && !f_s && !f_b && !f_v &&
			!s_n && !s_s && !s_b && !s_v ) {
			COMPILATION_ERROR
			("Invalid operands of types : %s and %s to binary"
			" operator %s\n", first_operand_type,
						second_operand_type, opr);
		} else if ( (f_n || f_s || f_b || f_v) &&
				(!s_n && !s_s && !s_b && !s_v) ) {
			COMPILATION_ERROR
			("Invalid second operand of type %s to binary"
			" operator %s\n", second_operand_type, opr);
		} else if ( (!f_n && !f_s && !f_b && !f_v) &&
				(s_n || s_s || s_b || s_v) ) {
			COMPILATION_ERROR
			("Invalid first operand of type %s to binary"
			" operator %s\n", first_operand_type, opr);
		} else if ( not_same_valid_type ) {
			COMPILATION_ERROR
			("Can't compare %s with %s using operator %s\n",
			first_operand_type, second_operand_type, opr);
		}
	}

	first_operand_type = NULL;
	second_operand_type = NULL;
}

static void equality_expression_code_generation(void *trans_node)
{
	TOUCH();

	struct equality_expression *e = trans_node;
	int action = e->type;
	switch ( action ) {
	case EEqual1:	{
				trans_relational_expression(e->u.e1.r);
				e->d_type = e->u.e1.r->d_type;

				semantic_analysis[ EEquality_exp ](action,
							e->u.e1.r->d_type);
				break;
			}
	case EEqual2:	{
				first_operand_seen = true;
				trans_equality_expression(e->u.e2.e);
				first_operand_seen = false;

				e->d_type = e->u.e2.e->d_type;
				fputs(" == ", op);

				second_operand_seen = true;
				trans_relational_expression(e->u.e2.r);
				second_operand_seen = false;

				check_equality_op_types(trans_node, "==");

				semantic_analysis[ EEquality_exp ](action,
							e->u.e2.e->d_type,
							e->u.e2.r->d_type);
				break;
			}
	case EEqual3:	{
				first_operand_seen = true;
				trans_equality_expression(e->u.e3.e);
				first_operand_seen = false;

				e->d_type = e->u.e3.e->d_type;
				fputs(" != ", op);	

				second_operand_seen = true;
				trans_relational_expression(e->u.e3.r);
				second_operand_seen = false;

				check_equality_op_types(trans_node, "!=");

				semantic_analysis[ EEquality_exp ](action,
							e->u.e3.e->d_type,
							e->u.e3.r->d_type);
				break;
			}
	}	
}

static void trans_equality_expression(void *trans_node)
{
	HIT();

	struct equality_expression *e = trans_node;
	switch ( e->type ) {
	case EEqual1:
	case EEqual2:
	case EEqual3:	{
				code_generation[ EEquality_exp ](e);
				break;
			}
	default:	{
				UNKNOWN_TYPE(e->type);
				break;
			}
	}	
}

static void logical_and_exp_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ELand1:	{
				break;
			}
	case ELand2:	{
				break;

			}
	}
}

static void check_logical_binary_op_types(void *trans_node, const char *opr)
{
	TOUCH();

	if ( first_operand_type && second_operand_type ) {
		bool f_b = !strcmp(first_operand_type, "bool");
		bool s_b = !strcmp(second_operand_type, "bool");

		if ( !f_b && !s_b ) {
			COMPILATION_ERROR
			("Invalid operands of types : %s and %s"
			" to binary operator %s\n", first_operand_type,
						second_operand_type, opr);
		} else if ( !f_b ) {
			COMPILATION_ERROR
			("Invalid first operand of type %s to"
			" binary operator %s\n", first_operand_type, opr);
		} else if ( !s_b ) {
			COMPILATION_ERROR
			("Invalid second operand of type %s to"
			" binary operator %s\n", second_operand_type, opr);
		}
	}

	first_operand_type = NULL;
	second_operand_type = NULL;
}

static void logical_and_exp_code_generation(void *trans_node)
{
	TOUCH();

	struct logical_and_exp *l = trans_node;
	int action = l->type;
	switch ( action ) {
	case ELand1:	{
				trans_equality_expression(l->u.l1.e);
				l->d_type = l->u.l1.e->d_type;

				semantic_analysis[ ELogical_and_exp ](action,
							l->u.l1.e->d_type);
				break;
			}
	case ELand2:	{
				first_operand_seen = true;
				trans_logical_and_exp(l->u.l2.l);
				first_operand_seen = false;

				l->d_type = l->u.l2.l->d_type;
				fputs(" && ", op);

				second_operand_seen = true;
				trans_equality_expression(l->u.l2.e);
				second_operand_seen = false;

				check_logical_binary_op_types(trans_node,
								"'and'");

				semantic_analysis[ ELogical_and_exp ](action,
							l->u.l2.l->d_type,
							l->u.l2.e->d_type);
				break;
			}
	}
}

static void trans_logical_and_exp(void *trans_node)
{
	HIT();

	struct logical_and_exp *l = trans_node;
	switch ( l->type ) {
	case ELand1:
	case ELand2:	{
				code_generation[ ELogical_and_exp ](l);
				break;
			}
	default:	{
				UNKNOWN_TYPE(l->type);
				break;
			}
	}
}

static void logical_or_exp_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ELor1:	{
				break;
			}
	case ELor2:	{
				break;
			}
	}
}

static void logical_or_exp_code_generation(void *trans_node)
{
	TOUCH();

	struct logical_or_exp *l = trans_node;
	int action = l->type;
	switch ( action ) {
	case ELor1:	{
				trans_logical_and_exp(l->u.l1.a);
				l->d_type = l->u.l1.a->d_type;

				semantic_analysis[ ELogical_or_exp ](action,
							l->u.l1.a->d_type);
				break;
			}
	case ELor2:	{
				first_operand_seen = true;
				trans_logical_or_exp(l->u.l2.o);
				first_operand_seen = false;

				l->d_type = l->u.l2.o->d_type;
				fputs(" || ", op);

				second_operand_seen = true;
				trans_logical_and_exp(l->u.l2.a);
				second_operand_seen = false;

				check_logical_binary_op_types(trans_node,
								"'or'");

				semantic_analysis[ ELogical_or_exp ](action,
							l->u.l2.o->d_type,
							l->u.l2.a->d_type);
				break;
			}
	}
}

static void trans_logical_or_exp(void *trans_node)
{
	HIT();

	struct logical_or_exp *l = trans_node;
	switch ( l->type ) {
	case ELor1:
	case ELor2:	{
				code_generation[ ELogical_or_exp ](l);
				break;
			}
	default:	{
				UNKNOWN_TYPE(l->type);
				break;
			}
	}
}

static void logical_xor_exp_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ELxor1:	{
				break;
			}
	case ELxor2:	{
				break;
			}
	}
}

static void logical_xor_exp_code_generation(void *trans_node)
{
	TOUCH();

	struct logical_xor_exp *l = trans_node;
	int action = l->type;
	switch ( action ) {
	case ELxor1:	{
				trans_logical_or_exp(l->u.l1.o);
				l->d_type = l->u.l1.o->d_type;

				semantic_analysis[ ELogical_xor_exp ](action,
							l->u.l1.o->d_type);
				break;
			}
	case ELxor2:	{
				xor_seen = true;

				xor_first_op_seen = true;
				first_operand_seen = true;
				trans_logical_xor_exp(l->u.l2.x);
				first_operand_seen = false;
				l->d_type = l->u.l2.x->d_type;
				xor_first_op_seen = false;

				xor_sec_op_seen = true;
				second_operand_seen = true;
				trans_logical_or_exp(l->u.l2.o);
				second_operand_seen = false;
				xor_sec_op_seen = false;

				check_logical_binary_op_types(trans_node,
								"'xor'");

				fprintf(op, "((%s) && !(%s)) "
						"|| (!(%s) && (%s))",
							xor_first, xor_sec,	
							xor_first, xor_sec);
				free(xor_first); // strdup()'d in cgdl.l and
						// got through
						// trans_identifier().
				xor_first = NULL;
				free(xor_sec); // strdup()'d in cgdl.l and
						// got through
						// trans_identifier().
				xor_sec = NULL;

				semantic_analysis[ ELogical_xor_exp ](action,
							l->u.l2.x->d_type,
							l->u.l2.o->d_type);
				break;
			}
	}	
}

static void trans_logical_xor_exp(void *trans_node)
{
	HIT();

	struct logical_xor_exp *l = trans_node;
	switch ( l->type ) {
	case ELxor1:
	case ELxor2:	{
				code_generation[ ELogical_xor_exp ](l);
				break;
			}
	default:	{
				UNKNOWN_TYPE(l->type);
				break;
			}
	}
}

static void constant_expression_semantic_analysis(int action,
						struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EConst_exp1:	{
					break;
				}
	}
}

static void constant_expression_code_generation(void *trans_node)
{
	TOUCH();

	struct constant_expression *ce = trans_node;
	int action = ce->type;
	switch ( action ) {
	case EConst_exp1:	{
					trans_logical_xor_exp(ce->u.c1.x);
					ce->d_type = ce->u.c1.x->d_type;

					semantic_analysis[ EConst_exp ](action,
							ce->u.c1.x->d_type);
					break;
				}
	}	
}

static void trans_constant_expression(void *trans_node)
{
	HIT();

	struct constant_expression *ce = trans_node;
	switch ( ce->type ) {
	case EConst_exp1:	{
					code_generation[ EConst_exp ](ce);
					break;
				}
	default:		{
					UNKNOWN_TYPE(ce->type);
					break;
				}
	}
}

static void assignment_expression_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EAssign1:	{
				break;
			}
	case EAssign2:	{
				break;
			}
	}
}

static void assignment_expression_code_generation(void *trans_node)
{
	TOUCH();

	struct assignment_expression *a = trans_node;
	int action = a->type;
	switch ( a->type ) {
	case EAssign1:	{
				trans_logical_xor_exp(a->u.a1.x);
				a->d_type = a->u.a1.x->d_type;

				semantic_analysis[ EAssign_exp ](action,
							a->u.a1.x->d_type);
				break;
			}
	case EAssign2:	{
				INDENT_ONCE();
				assign_seen = true;
				first_operand_seen = true;
				trans_unary_expression(a->u.a2.u);
				first_operand_seen = false;
				assign_seen = false;

				a->d_type = a->u.a2.u->d_type;
				fputs(" = ", op);

				second_operand_seen = true;
				trans_assignment_expression(a->u.a2.a);
				second_operand_seen = false;
	
				if ( first_operand_type &&
						second_operand_type ) {
					if ( strcmp(first_operand_type,
							second_operand_type) ) {
						COMPILATION_ERROR
						("Cannot convert %s to %s\n",
						second_operand_type,
						first_operand_type);
					}
				}
			
				semantic_analysis[ EAssign_exp ](action,
							a->u.a2.u->d_type,
							a->u.a2.a->d_type);
				break;
			}
	}	
}

static void trans_assignment_expression(void *trans_node)
{
	HIT();

	struct assignment_expression *a = trans_node;
	switch ( a->type ) {
	case EAssign1:
	case EAssign2:	{
				code_generation[ EAssign_exp ](a);
				break;
			}
	default:	{
				UNKNOWN_TYPE(a->type);
				break;
			}
	}	
}

static void expression_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EExp1:	{
				break;
			}
	case EExp2:	{
				break;
			}
	}
}

static void expression_code_generation(void *trans_node)
{
	TOUCH();

	struct expression *e = trans_node;
	int action = e->type;
	switch ( e->type ) {
	case EExp1:	{
				trans_assignment_expression(e->u.e1.a);
				e->d_type = e->u.e1.a->d_type;

				semantic_analysis[ EExp ](action,
						e->u.e1.a->d_type);
				break;
			}
	case EExp2:	{
				trans_expression(e->u.e2.e);
				e->d_type = e->u.e2.e->d_type;
				fputs(", ", op);
				trans_assignment_expression(e->u.e2.a);

				semantic_analysis[ EExp ](action,
						e->u.e2.e->d_type,
						e->u.e2.a->d_type);
				break;
			}
	}	
}

static void trans_expression(void *trans_node)
{
	HIT();

	struct expression *e = trans_node;
	switch ( e->type ) {
	case EExp1:
	case EExp2:	{
				code_generation[ EExp ](e);
				break;
			}
	default:	{
				UNKNOWN_TYPE(e->type);
				break;
			}
	}
}

static void declaration_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EDeclaration1:	{
					break;
				}
	case EDeclaration2:	{
					break;
				}
	}
}

static void declaration_code_generation(void *trans_node)
{
	TOUCH();

	struct declaration *d = trans_node;
	int action = d->type;
	switch ( action ) {
	case EDeclaration1:	{
					INDENT_ONCE();
					decln_seen = true;
					trans_decln_spec(d->u.d1.d);

					if ( array_size_expression_seen ) {
						array_size_expression_seen =
									false;
					}

					decln_seen = false;
					d->d_type = d->u.d1.d->d_type;

					bool semicolon_not_needed =
						dont_put_semicolon;
					
					if ( dont_put_semicolon ) {
						dont_put_semicolon = false;
					}

					if ( dont_put_src_line_num ) {
						dont_put_src_line_num =
							false;
					}
					if ( define_vector && vector_name ) {
						char *t =
						realloc(dynamic_definitions,
							strlen(
							dynamic_definitions) +
							strlen("\n\t") +
							strlen
							("vector<int> ") +
							strlen(vector_name) +
							strlen(";\n") +
							1);

						if ( t ) {
						sprintf(t + strlen(t),
							"\n\tvector<int> %s"
							";\n", vector_name);
						dynamic_definitions = t;
						}
	
						define_vector = false;
						free(vector_name);
						vector_name = NULL;

						semicolon_not_needed = true;
					}

					if ( !semicolon_not_needed ) {
						fputs(";\n", op);
					}	

					if ( array_defn_seen ) {
						array_defn_seen = false;
					}

				semantic_analysis[ EDeclaration ](
							action,
							d->u.d1.d->d_type);
					break;
				}
	case EDeclaration2:	{
					INDENT();
					decln_seen = true;
					trans_decln_spec(d->u.d2.d);

					if ( array_size_expression_seen ) {
						array_size_expression_seen =
									false;
					}

					decln_seen = false;
					d->d_type = d->u.d2.d->d_type;
					trans_init_declr_list(d->u.d2.i);

					if ( put_close_paren ) {
						fputc(')', op);
						put_close_paren = false;
					}	

					if ( !dont_put_semicolon ) {
						fputs(";\n", op);
					} else {
						dont_put_semicolon = false;
					}

					if ( dont_put_src_line_num ) {
						dont_put_src_line_num =
							false;
					}

					INDENT_ONCE();
					if ( define_int_for_attr ) {
						fputs("int ", op);
						fputs(int_attr_var, op);
						free(int_attr_var);
						int_attr_var = NULL;
						define_int_for_attr = false;
						fputs(";\n", op);
					}
					if ( define_vector && vector_name ) {
						char *t =
						realloc(dynamic_definitions,
							strlen(
							dynamic_definitions) +
							strlen("\n\t") +
							strlen
							("vector<int> ") +
							strlen(vector_name) +
							strlen(";\n") +
							1);

						if ( t ) {
						sprintf(t + strlen(t),
							"\n\tvector<int> %s"
							";\n", vector_name);
						dynamic_definitions = t;
						}
	
						define_vector = false;
						free(vector_name);
						vector_name = NULL;
					}

					semantic_analysis[ EDeclaration ](
							action,
							d->u.d2.d->d_type,
							d->u.d2.i->d_type);
					break;
				}
	}	
}

static void trans_declaration(void *trans_node)
{
	HIT();

	struct declaration *d = trans_node;
	switch ( d->type ) {
	case EDeclaration1:
	case EDeclaration2:	{
					code_generation[ EDeclaration ](d);
					break;
				}
	default:		{
					UNKNOWN_TYPE(d->type);
					break;
				}
	}
}

static void decln_spec_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EDecln_spec1:	{
					break;
				}
	case EDecln_spec2:	{
					break;
				}
	case EDecln_spec3:	{
					break;
				}
	case EDecln_spec4:	{
					break;
				}
	case EDecln_spec5:	{
					break;
				}
	case EDecln_spec6:	{
					break;
				}
	case EDecln_spec7:	{
					break;
				}
	case EDecln_spec8:	{
					break;
				}
	}
}

static void decln_spec_code_generation(void *trans_node)
{
	TOUCH();

	struct decln_spec *d = trans_node;
	int action = d->type;
	switch ( action ) {
	case EDecln_spec1:	{
					trans_storage_spec(d->u.d1.s);
					d->d_type = NULL;

					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d1.s->d_type);
					break;
				}
	case EDecln_spec2:	{
					trans_storage_spec(d->u.d2.s); 
					trans_decln_spec(d->u.d2.d);
					d->d_type = d->u.d2.d->d_type;

					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d2.s->d_type,
							d->u.d2.d->d_type);
					break;
				}
	case EDecln_spec3:	{
					trans_type_spec(d->u.d3.t);
					d->d_type = d->u.d3.t->d_type;

					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d3.t->d_type);
					break;
				}
	case EDecln_spec4:	{
					array_defn = malloc(
							strlen("vector<") +
						strlen(type_specifiers
							[ d->u.d4.t->type ]) +
							strlen("> ") +
							1);
					if ( array_defn ) {
						sprintf(array_defn,
							"vector<%s> ",
							type_specifiers
							[ d->u.d4.t->type ]);
					}	

					recent_type = strdup(type_specifiers[
							d->u.d4.t->type ]);
					// trans_type_spec(d->u.d4.t);
					// d->d_type = d->u.d4.t->d_type;

					array_defn_seen = true;
					array_size_expression_seen = true;
					array_size_expression = NULL;
					// fputs("[]", op);
					
					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d4.t->d_type);
					break;
				}
	case EDecln_spec5:	{
					array_defn = malloc(
							strlen("vector<") +
						strlen(type_specifiers
							[ d->u.d4.t->type ]) +
							strlen("> ") +
							1);
					if ( array_defn ) {
						sprintf(array_defn,
							"vector<%s> ",
							type_specifiers
							[ d->u.d4.t->type ]);
					}
	
					//fprintf(op, "vector<%s> ",
					//	type_specifiers
					//		[ d->u.d5.t->type ]);

					recent_type = strdup(type_specifiers[
							d->u.d4.t->type ]);
					// trans_type_spec(d->u.d5.t);
					// d->d_type = d->u.d5.t->d_type;

					array_defn_seen = true;

					// array_size_expression = strdup("[");
					subscript_seen = true;
					array_size_expression_seen = true;
					trans_expression(d->u.d5.e);
					array_size_expression_seen = false;
					subscript_seen = false;

					//char *t = realloc(array_size_expression,
					//	strlen(array_size_expression) +
					//	strlen("]") + 1);
					//if ( t ) {
					//	strcat(t, "]");
					//	array_size_expression = t;
					//}

					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d5.t->d_type,
							d->u.d5.e->d_type);
					break;
				}
	case EDecln_spec6: 	{
					trans_type_spec(d->u.d6.t);
					d->d_type = d->u.d6.t->d_type;
					trans_decln_spec(d->u.d6.d);

					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d6.t->d_type,
							d->u.d6.d->d_type);
					break;
				}
	case EDecln_spec7: 	{
					trans_type_qual(d->u.d7.t);
					d->d_type = NULL;

					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d7.t->d_type);
					break;
				}
	case EDecln_spec8: 	{
					trans_type_qual(d->u.d8.t);
					trans_decln_spec(d->u.d8.d);
					d->d_type = d->u.d8.d->d_type;

					semantic_analysis[ EDeclaration_spec ](
							action,
							d->u.d8.t->d_type,
							d->u.d8.d->d_type);
					break;
				}
	}	
}

static void trans_decln_spec(void *trans_node)
{
	HIT();

	struct decln_spec *d = trans_node;
	switch ( d->type ) {
	case EDecln_spec1 :
	case EDecln_spec2 :
	case EDecln_spec3 :
	case EDecln_spec4 :
	case EDecln_spec5 :
	case EDecln_spec6 :
	case EDecln_spec7 :
	case EDecln_spec8 :	{
					code_generation[ EDeclaration_spec ](d);
					break;
				}
	default :		{
					UNKNOWN_TYPE(d->type);
					break;
				}
	}
}

static void init_declr_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EInit_declr_list1:	{
					break;
				}
	case EInit_declr_list2:	{
					break;
				}
	}
}						

static void init_declr_list_code_generation(void *trans_node)
{
	TOUCH();

	struct init_declr_list *i = trans_node;
	int action = i->type;
	switch ( action ) {
	case EInit_declr_list1:	{
					trans_init_declr(i->u.i1.i);
					i->d_type = i->u.i1.i->d_type;

					semantic_analysis[ EInit_declr_list ](
							action,
							i->u.i1.i->d_type);
					break;
				}
	case EInit_declr_list2: {
					trans_init_declr_list(i->u.i2.d);
					i->d_type = i->u.i2.d->d_type;
					fputs(", ", op);
					trans_init_declr(i->u.i2.i);

					semantic_analysis[ EInit_declr_list ](
							action,
							i->u.i2.d->d_type,
							i->u.i2.i->d_type);
					break;
				}
	}	
}

static void trans_init_declr_list(void *trans_node)
{
	HIT();

	struct init_declr_list *i = trans_node;
	switch ( i->type ) {
	case EInit_declr_list1:
	case EInit_declr_list2:	{
					code_generation[ EInit_declr_list ](i);
					break;
				}
	default:		{
					UNKNOWN_TYPE(i->type);
					break;
				}
	}
}

static void init_declr_code_generator1(void *trans_node)
{
	TOUCH();	

	struct init_declr *i = trans_node;

	create_temp = !strcmp(recent_type, "Player *") ||
				!strcmp(recent_type, "Card *") ||
				!strcmp(recent_type, "Pile *");


	if ( comp_type_decln_was_seen ) {
		comp_type_var_seen = true;
		comp_type_decln_was_seen = false;
	}

	decln_seen = true;
	trans_declarator(i->u.i1.d);
	decln_seen = false;


	if ( array_size_expression ) {
		fputs(array_size_expression, op);
		free(array_size_expression);
		array_size_expression = NULL;
	}

	if ( create_temp ) {
		if ( recent_identifier ) {
			fputs(" = ", op);
			fputc('&', op);
			fputs(recent_identifier, op);
			free(recent_identifier);
			recent_identifier = NULL;
			fputc('_', op);
		}
		create_temp = false;
	}

	i->d_type = i->u.i1.d->d_type;
}

static void init_declr_code_generator2(void *trans_node)
{
	TOUCH();

	struct init_declr *i = trans_node;

	if ( comp_type_decln_was_seen ) {
		comp_type_var_seen = true;
		comp_type_decln_was_seen = false;
	}

	decln_seen = true;
	trans_declarator(i->u.i2.d);
	decln_seen = false;

//	if ( array_size_expression ) {
//		fputs(array_size_expression, op);
//		free(array_size_expression);
//		array_size_expression = NULL;
//	}

	
	if ( array_defn_seen ) {
		array_defn_seen = false;
	}

	fputs(" = ", op);
	trans_initializer(i->u.i2.i);
}

static void init_declr_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EInit_declr1:	{
					break;
				}
	case EInit_declr2:	{
					break;
				}
	}
}

static void init_declr_code_generation(void *trans_node)
{
	TOUCH();

	struct init_declr *i = trans_node;
	int action = i->type;
	switch ( action ) {
	case EInit_declr1:	{
					init_declr_code_generator1(i);

					semantic_analysis[ EInit_declr ](action,
							i->u.i1.d->d_type);
					break;
				}
	case EInit_declr2:	{
					init_declr_code_generator2(i);

					semantic_analysis[ EInit_declr ](action,
							i->u.i2.d->d_type,
							i->u.i2.i->d_type);
					break;
				}
	}
}

static void trans_init_declr(void *trans_node)
{
	HIT();

	struct init_declr *i = trans_node;
	switch ( i->type ) {
	case EInit_declr1:
	case EInit_declr2:	{
					code_generation[ EInit_declr ](i);
					break;
				}
	default:		{
					UNKNOWN_TYPE(i->type);
					break;
				}
	}
}

static void storage_spec_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EStorage1:	{
				break;
			}
	}
}

static void storage_spec_code_generation(void *trans_node)
{
	TOUCH();

	struct storage_spec *s = trans_node;
	int action = s->type;
	switch ( action ) {
	case EStorage1:	{
				if ( !dont_put_src_line_num ) {
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				fputs("static ", op);
				s->d_type = unification(
						strdup("static"), NULL, EBasic);

				semantic_analysis[ EStorage_spec ](action,
								s->d_type);
				break;
			}
	}	
}

static void trans_storage_spec(void *trans_node)
{
	HIT();

	struct storage_spec *s = trans_node;
	switch ( s->type ) {
	case EStorage1:	{
				code_generation[ EStorage_spec ](s);
				break;
			}
	default:	{
				UNKNOWN_TYPE(s->type);
				break;
			}
	}
}

static void update_func_ret_type(const char *t)
{
	if ( t ) {
		if ( func_ret_type ) {
			size_t l = strlen(func_ret_type);

			func_ret_type = realloc(func_ret_type,
						l +
						strlen(" ") +
						strlen(t) + 1);

			strcat(func_ret_type, " ");
			strcat(func_ret_type, t);
		} else {
			func_ret_type = strdup(t);
		}
	}
}

static void update_func_parameter_list(const char *t)
{
	if ( func_parameter_list_seen && t ) {
		if ( func_parameter_list ) {
			size_t l = func_parameter_list ?
					strlen(func_parameter_list)
					: 0;

			func_parameter_list = realloc(func_parameter_list,
						l + strlen(t)
							+ 1);

			strcat(func_parameter_list, "|");
			strcat(func_parameter_list, strdup(t));
		} else {
			func_parameter_list = strdup(t);
		}
	}
}

static void type_spec_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EType1:	{
				update_func_ret_type("void");
				update_func_parameter_list("void");
				break;
			}
	case EType2:	{
				update_func_ret_type("string");
				update_func_parameter_list("string");
				break;
			}
	case EType3:	{
				update_func_ret_type("bool");
				update_func_parameter_list("bool");				
				break;
			}
	case EType4:	{
				update_func_ret_type("long double");
				update_func_parameter_list("long double");
				break;
			}
	case EType5:	{
				update_func_ret_type("Card *");
				update_func_parameter_list("Card *");	
				break;
			}
	case EType6:	{
				update_func_ret_type("Player *");
				update_func_parameter_list("Player *");
				break;
			}
	case EType7:	{
				update_func_ret_type("Pile *");
				update_func_parameter_list("Pile *");
				break;
			}
	case EType8:	{
				update_func_ret_type
						("VISIBILITY");
				update_func_parameter_list
						("VISIBILITY");
				break;
			}
	case EType9:	{
				break;
			}
	case EType10:	{
				break;
			}
	case EType11:	{
				break;
			}
	}
}

static void type_spec_code_generation(void *trans_node)
{
	TOUCH();

	struct type_spec *t = trans_node;
	int action = t->type;
	switch ( action ) {
	case EType1: 	{
				t->d_type = unification(strdup("void"),
								NULL, EBasic);
				semantic_analysis[ EType_spec ](action,
								t->d_type);

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( !func_ret_type_seen ) {
					if ( previous_indentation
						&& *previous_indentation ) {
						t->indentation =
							previous_indentation;
						INDENT_ONCE();
					} else {
						previous_indentation =
							strdup("\t");
						fputc('\t', op);
					}
				}
					
				fputs("void ", op);
				recent_type = strdup("void");
				break;
			}
	case EType2: 	{
				t->d_type = unification(strdup("string"),
								NULL, EBasic);
				semantic_analysis[ EType_spec ](action,
								t->d_type);

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}

				if ( !func_ret_type_seen ) {
					if ( previous_indentation
						&& *previous_indentation ) {
						t->indentation =
							previous_indentation;
						INDENT_ONCE();
					} else {
						previous_indentation =
							strdup("\t");
						fputc('\t', op);
					}
				}
				
				fputs("string ", op);
				recent_type = strdup("string");
				break;
			}
	case EType3: 	{
				t->d_type = unification(strdup("bool"), NULL,
									EBasic);
				semantic_analysis[ EType_spec ](action,
								t->d_type);

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}

				if ( !func_ret_type_seen ) {
					if ( previous_indentation
						&& *previous_indentation ) {
						t->indentation =
							previous_indentation;
						INDENT_ONCE();
					} else {
						previous_indentation =
							strdup("\t");
						fputc('\t', op);
					}
				}

				fputs("bool ", op);
				recent_type = strdup("bool");
				break;
			}
	case EType4: 	{
				t->d_type = unification(strdup("number"),
								NULL, EBasic);
				semantic_analysis[ EType_spec ](action,
								t->d_type);	

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}

				if ( !func_ret_type_seen ) {
					if ( previous_indentation
						&& *previous_indentation ) {
						t->indentation =
							previous_indentation;
						INDENT_ONCE();
					} else {
						previous_indentation =
							strdup("\t");
						fputc('\t', op);
					}
				}

				fputs("long double ", op);
				recent_type = strdup("long double");
				break;
			}
	case EType5: 	{
				t->d_type = unification(strdup("Card"),
								NULL, EComp);
				semantic_analysis[ EType_spec ](action,
								t->d_type);	

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}

				if ( previous_indentation &&
					*previous_indentation ) {
					t->indentation = previous_indentation;
				} else {
					previous_indentation =
						t->indentation;
				}

				INDENT_ONCE();
				fputs("Card *", op);
//				if ( decln_seen ) {
//					fputc('*', op);
					recent_type = strdup("Card *");
//				} else {
//					recent_type = strdup("Card");
//				}
				break;
			}
	case EType6: 	{
				t->d_type = unification(strdup("Player"),
								NULL, EComp);
				semantic_analysis[ EType_spec ](action,
								t->d_type);

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}
			
				if ( previous_indentation &&
					*previous_indentation ) {
					t->indentation = previous_indentation;
				} else {
					previous_indentation =
						t->indentation;
				}
				INDENT_ONCE();
				fputs("Player *", op);
//				if ( decln_seen ) {
//					fputc('*', op);
					recent_type = strdup("Player *");
//				} else {
//					recent_type = strdup("Player");
//				}
				break;
			}
	case EType7: 	{
				t->d_type = unification(strdup("Pile"),
								NULL, EComp);
				semantic_analysis[ EType_spec ](action,
								t->d_type);	

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}

				if ( previous_indentation &&
					*previous_indentation ) {
					t->indentation = previous_indentation;
				} else {
					previous_indentation =
						t->indentation;
				}
				INDENT_ONCE();

				fputs("Pile *", op);
//				if ( decln_seen ) {
//					fputc('*', op);
					recent_type = strdup("Pile *");
//				} else {
//					recent_type = strdup("Pile");
//				}
				break;
			}
	case EType8:	{
				t->d_type = unification(
					strdup("VISIBILITY"),
						NULL, EBasic);
				semantic_analysis[ EType_spec ](action,
								t->d_type);

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}


				if ( previous_indentation &&
					*previous_indentation ) {
					t->indentation = previous_indentation;
				} else {
					previous_indentation =
						t->indentation;
				}
				INDENT_ONCE();

				fputs("int ", op);
				recent_type = strdup("int");
				break;
			}
	case EType9:	{
				t->d_type = unification(strdup("ATTRIBUTE"),
								NULL, EComp);
				semantic_analysis[ EType_spec ](action,
								t->d_type);				

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}

				if ( previous_indentation &&
					*previous_indentation ) {
					t->indentation = previous_indentation;
				} else {
					previous_indentation =
						t->indentation;
				}
				INDENT_ONCE();
		
				recent_type = strdup("ATTRIBUTE");
				trans_attr_spec(t->u.t9.a);
				break;
			}
	case EType10:	{
				t->d_type = unification(strdup("RECORD"),
								NULL, EComp);
				semantic_analysis[ EType_spec ](action,
								t->d_type);

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}

				if ( previous_indentation &&
					*previous_indentation ) {
					t->indentation = previous_indentation;
				} else {
					previous_indentation =
						t->indentation;
				}
				INDENT_ONCE();

				recent_type = strdup("RECORD");
				trans_record_spec(t->u.t10.r);
				break;
			}
	case EType11:	{
				t->d_type = unification(strdup("CATALOG"),
								NULL, EComp);
				semantic_analysis[ EType_spec ](action,
								t->d_type);

				if ( !dont_put_src_line_num &&
					active_block != 0 ) { // Not Global
					EMBED_SRC_LINE();
					dont_put_src_line_num = true;
				}

				if ( setup_seen ) {
					correct_indentation_for_setup();
				}


				if ( previous_indentation &&
					*previous_indentation ) {
					t->indentation = previous_indentation;
				} else {
					previous_indentation =
						t->indentation;
				}
				INDENT_ONCE();

				recent_type = strdup("CATALOG");
				trans_catalog_spec(t->u.t11.c);
				break;
			}
	case EType12:	{
				recent_type = strdup("SET");
				INDENT_ONCE();
				fputs(recent_type, op);
				fputc(' ', op);
				break;
			}
	}
}

static void trans_type_spec(void *trans_node)
{
	HIT();

	struct type_spec *t = trans_node;
	switch ( t->type ) {
	case EType1:
	case EType2:
	case EType3:
	case EType4:
	case EType5:
	case EType6:
	case EType7:
	case EType8:
	case EType9:
	case EType10:
	case EType11:
	case EType12:	{
				code_generation[ EType_spec ](t);
				break;
			}
	default:	{
				UNKNOWN_TYPE(t->type);
				break;
			}
	}
}

static void game_elem_semantic_analysis(int action,
					struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EGame1:	{
				break;
			}
	case EGame2:	{
				break;
			}
	}
}

static void game_elem_code_generation(void *trans_node)
{
	TOUCH();

	struct game_elem *g = trans_node;
	int action = g->type;
	switch ( action ) {
	case EGame1:	{
				g->d_type = unification(
						strdup("Setup"), NULL, EComp);
				semantic_analysis[ EGame_elem ](
						action, g->d_type);	

				printf("Remove-me-later:Setup");
				setup_seen = true;
				break;
			}
	case EGame2:	{
				g->d_type =
				unification(strdup("Round"), NULL, EComp);
				semantic_analysis[ EGame_elem ](
						action, g->d_type);

				printf("Remove-me-later:Round");
				fputc('\n', op);
				INDENT();
				fprintf(op, "while (!winner) ");
				round_game_elem_identation = g->indentation;
				break;
			}
	}	
}

static void trans_game_elem(void *trans_node)
{
	HIT();

	struct game_elem *g = trans_node;
	switch ( g->type ) {
	case EGame1:
	case EGame2:	{
				code_generation[ EGame_elem ](g);
				break;
			}
	default:	{
				UNKNOWN_TYPE(g->type);
				break;
			}
	}
}	

static void attr_spec_semantic_analysis(int action,
					struct unified_type *t1,
					struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EAttr1:	{
				break;
			}
	}
}

static void attr_spec_code_generation(void *trans_node)
{
	TOUCH();

	struct attr_spec *a = trans_node;
	int action = a->type;
	switch ( action ) {
	case EAttr1:	{
				attr_seen = true;

				char *attr_spec_n = a->u.a1.i->u.i1.i;

				// trans_identifier(a->u.a1.i);

				define_vector = true;
				vector_name = a->u.a1.i->u.i1.i;

				char *t = realloc(attr_enums,
						strlen(attr_enums) +
						strlen("enum ") +
						strlen(attr_spec_n) +
						strlen(" { ") +
						1);
				if ( t ) {
					sprintf(t + strlen(t),
						"enum %s { ", attr_spec_n);
					attr_enums = t;
					t = NULL;
				}

				first_attr_spec = true;
				trans_attr_val_list(a->u.a1.a);

				t = realloc(attr_enums,
						strlen(attr_enums) +
						strlen(" };\n") +
						1);
				if ( t ) {
					strcat(t, " };\n");
					attr_enums = t;
					t = NULL;
				}

				a->d_type = a->u.a1.i->d_type;
				attr_seen = false;

				attr_was_seen = true;

				dont_put_semicolon = true;

				if ( attr_const_seen ) {
					attr_const_seen = false;
				}

				semantic_analysis[ EAttr_spec ](action,
						a->u.a1.i->d_type,
						a->u.a1.a->d_type);	
				break;
			}
	}	
}

static void trans_attr_spec(void *trans_node)
{
	HIT();

	struct attr_spec *a = trans_node;
	switch ( a->type ) {
	case EAttr1:	{
				code_generation[ EAttr_spec ](a);
				break;
			}
	default:	{
				UNKNOWN_TYPE(a->type);
				break;
			}
	}
}

static void attr_val_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EAttr_val_list1:	{
					break;
				}
	case EAttr_val_list2:	{
					break;
				}
	}
}

static void attr_val_list_code_generation(void *trans_node)
{
	TOUCH();

	struct attr_val_list *a = trans_node;
	int action = a->type;
	switch ( action ) {
	case EAttr_val_list1:	{
					attr_const_seen = true;
					trans_attr_const(a->u.a1.a);
					a->d_type = a->u.a1.a->d_type;

					semantic_analysis[ EAttr_val_list ](
							action,
							a->u.a1.a->d_type);
					break;
				}
	case EAttr_val_list2: 	{
					attr_const_seen = true;
					trans_attr_val_list(a->u.a2.l);
					// fputs(" , ", op);
					trans_attr_const(a->u.a2.a);
					a->d_type = a->u.a2.a->d_type;

					semantic_analysis[ EAttr_val_list ](
							action,
							a->u.a2.l->d_type,
							a->u.a2.a->d_type);
					break;
				}
	}
}

static void trans_attr_val_list(void *trans_node)
{
	HIT();

	struct attr_val_list *a = trans_node;
	switch ( a->type ) {
	case EAttr_val_list1:
	case EAttr_val_list2:	{
					code_generation[ EAttr_val_list ](a);
					break;
				}			
	default:		{
					UNKNOWN_TYPE(a->type);
					break;
				}
	}
}

static void attr_const_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EAttr_const1:	{
					break;
				}
	}
}

static void attr_const_code_generation(void *trans_node)
{
	TOUCH();

	struct attr_const *a = trans_node;
	int action = a->type;
	switch ( action ) {
	case EAttr_const1: 	{
					trans_constant_expression(a->u.a1.c);
					a->d_type = a->u.a1.c->d_type;

					semantic_analysis[ EAttr_const ](
							action,
							a->d_type);
					break;
				}
	}	
}

static void trans_attr_const(void *trans_node)
{
	HIT();

	struct attr_const *a = trans_node;
	switch ( a->type ) {
	case EAttr_const1: 	{
					code_generation[ EAttr_const ](a);
					break;
				}
	default:		{
					UNKNOWN_TYPE(a->type);
					break;
				}
	}
}

static void record_spec_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ERecord_spec1:	{
					break;
				}
	case ERecord_spec2:	{
					break;
				}
	}
}

static void record_spec_code_generation(void *trans_node)
{
	TOUCH();

	struct record_spec *r = trans_node;
	int action = r->type;
	switch ( action ) {
	case ERecord_spec1:	{
					if ( comp_type_decln_seen ) {
						nested_comp_type_decln_seen =
								true;
					}

					comp_type_decln_seen = true;

					fputs("struct ", op);
					trans_identifier(r->u.r1.i);
					fputs(" {\n", op);
					r->d_type = r->u.r1.i->d_type;

					trans_record_decln_list(r->u.r1.r);

					fputc('}', op);
					nested_comp_types
					[ cur_comp_type_nesting_level-- ]
						= NULL;

					last_member = NULL;

					if ( cur_comp_type_nesting_level
									== 1 ) {
						nested_comp_type_decln_seen =
								false;
					}

					if ( cur_comp_type_nesting_level < 0 ) {
						comp_type_decln_seen = false;
						last_seen_comp_type = NULL;
					}

					comp_type_decln_was_seen = true;					

					semantic_analysis[ ERecord_spec ](
							action,
							r->u.r1.i->d_type,
							r->u.r1.r->d_type);
					break;
				}
	case ERecord_spec2:	{
					fputs("struct ", op);
					trans_identifier(r->u.r2.i);
					fputc(' ', op);
					r->d_type = r->u.r2.i->d_type;

					semantic_analysis[ ERecord_spec ](
							action,
							r->u.r2.i->d_type);
					break;
				}
	}
}

static void trans_record_spec(void *trans_node)
{
	HIT();

	struct record_spec *r = trans_node;
	switch ( r->type ) {
	case ERecord_spec1:
	case ERecord_spec2:	{
					code_generation[ ERecord_spec ](r);
					break;
				}
	default:		{
					UNKNOWN_TYPE(r->type);
					break;
				}
	}
}

static void record_decln_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ERecord_decln_list1:	{
						break;
					}
	case ERecord_decln_list2:	{
						break;
					}
	}
}

static void record_decln_list_code_generation(void *trans_node)
{
	TOUCH();

	struct record_decln_list *r = trans_node;
	int action = r->type;
	switch ( action ) {
	case ERecord_decln_list1:	{
						trans_record_decln(r->u.r1.r);
						r->d_type = r->u.r1.r->d_type;

						semantic_analysis
						[ ERecord_decln_list ](action,
							r->u.r1.r->d_type);
						break;
					}
	case ERecord_decln_list2:	{
						trans_record_decln_list(
								r->u.r2.r1);
						trans_record_decln(r->u.r2.r2);
						r->d_type = r->u.r2.r2->d_type;

						semantic_analysis
						[ ERecord_decln_list ](action,
							r->u.r2.r1->d_type,
							r->u.r2.r2->d_type);
						break;
					}
	}
}

static void trans_record_decln_list(void *trans_node)
{
	HIT();

	struct record_decln_list *r = trans_node;
	switch ( r->type ) {
	case ERecord_decln_list1:
	case ERecord_decln_list2:	{
						code_generation
						[ ERecord_decln_list ](r);
						break;
					}
	default:			{
						UNKNOWN_TYPE(r->type);
						break;
					}
	}
}

static void record_decln_semantic_analysis(int action, struct unified_type *t1,
							struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ERecord1:	{
				break;
			}
	}
}

static void record_decln_code_generation(void *trans_node)
{
	TOUCH();

	struct record_decln *r = trans_node;
	int action = r->type;
	switch ( action ) {
	case ERecord1:	{
				INDENT_ONCE();
				trans_spec_qual_list(r->u.r1.s);
				comp_type_member_seen = true;
				trans_record_declr_list(r->u.r1.r);
				comp_type_member_seen = false;
				fputs(";\n", op);

				r->d_type = r->u.r1.r->d_type;

				semantic_analysis[ ERecord_decln ](action,
							r->u.r1.s->d_type,
							r->u.r1.r->d_type);
				break;
			}
	}	
}

static void trans_record_decln(void *trans_node)
{
	HIT();

	struct record_decln *r = trans_node;
	switch ( r->type ) {
	case ERecord1:	{
				code_generation[ ERecord_decln ](r);
				break;
			}
	default:	{
				UNKNOWN_TYPE(r->type);
				break;
			}
	}
}

static void spec_qual_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ESpec_qual_list1:	{
					break;
				}
	case ESpec_qual_list2:	{
					break;
				}
	case ESpec_qual_list3:	{
					break;
				}
	case ESpec_qual_list4:	{
					break;
				}
	}
}

static void spec_qual_list_code_generation(void *trans_node)
{
	TOUCH();

	struct spec_qual_list *s = trans_node;
	int action = s->type;
	switch ( action ) {
	case ESpec_qual_list1:	{
					trans_type_spec(s->u.s1.t);
					trans_spec_qual_list(s->u.s1.s);
					s->d_type = s->u.s1.t->d_type;

					semantic_analysis[ ESpec_qual_list ](
							action,
							s->u.s1.t->d_type,
							s->u.s1.s->d_type);	
					break;
				}
	case ESpec_qual_list2:	{
					trans_type_spec(s->u.s2.t);
					s->d_type = s->u.s2.t->d_type;

					semantic_analysis[ ESpec_qual_list ](
							action,
							s->u.s2.t->d_type);
					break;
				}
	case ESpec_qual_list3:	{
					trans_type_qual(s->u.s3.t);
					trans_spec_qual_list(s->u.s3.s);
					s->d_type = s->u.s3.s->d_type;

					semantic_analysis[ ESpec_qual_list ](
							action,
							s->u.s3.t->d_type,
							s->u.s3.s->d_type);
					break;
				}
	case ESpec_qual_list4:	{
					trans_type_qual(s->u.s4.t);
					s->d_type = s->u.s4.t->d_type;

					semantic_analysis[ ESpec_qual_list ](
							action,
							s->u.s4.t->d_type);
					break;
				}
	}	
}

static void trans_spec_qual_list(void *trans_node)
{
	HIT();

	struct spec_qual_list *s = trans_node;
	switch ( s->type ) {
	case ESpec_qual_list1:
	case ESpec_qual_list2:
	case ESpec_qual_list3:
	case ESpec_qual_list4:	{
					code_generation[ ESpec_qual_list ](s);
					break;
				}
	default:		{
					UNKNOWN_TYPE(s->type);
					break;
				}
	}	
}

static void record_declr_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ERecord_declr_list1:	{
						break;
					}
	case ERecord_declr_list2:	{
						break;
					}
	}
}

static void record_declr_list_code_generation(void *trans_node)
{
	TOUCH();

	struct record_declr_list *r = trans_node;
	int action = r->type;
	switch ( action ) {
	case ERecord_declr_list1:	{
						trans_record_declr(
								r->u.r1.r);
						r->d_type = r->u.r1.r->d_type;

						semantic_analysis
						[ ERecord_declr_list ](action,
							r->u.r1.r->d_type);
						break;
					}
	case ERecord_declr_list2:	{
						trans_record_declr_list(
								r->u.r2.r1);
						fputs(", ", op);
						trans_record_declr(
								r->u.r2.r2);
						r->d_type = r->u.r2.r2->d_type;

						semantic_analysis
						[ ERecord_declr_list ](action,
							r->u.r2.r1->d_type,
							r->u.r2.r2->d_type);
						break;
					}
	}
}

static void trans_record_declr_list(void *trans_node)
{
	HIT();

	struct record_declr_list *r = trans_node;
	switch ( r->type ) {
	case ERecord_declr_list1:	
	case ERecord_declr_list2:	{
						code_generation
						[ ERecord_declr_list ](r);
						break;
					}
	default:			
					{
						UNKNOWN_TYPE(r->type);
						break;
					}
	}
}

static void record_declr_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ERecord_declr1:	{
					break;
				}
	}
}

static void record_declr_code_generation(void *trans_node)
{
	TOUCH();

	struct record_declr *r = trans_node;
	int action = r->type;
	switch ( action ) {
	case ERecord_declr1:	{
					trans_declarator(r->u.r1.d);
					r->d_type = r->u.r1.d->d_type;

					semantic_analysis[ ERecord_declr ](
							action,
							r->u.r1.d->d_type);
					break;
				}
	}	
}

static void trans_record_declr(void *trans_node)
{
	HIT();

	struct record_declr *r = trans_node;
	switch ( r->type ) {
	case ERecord_declr1:	{
					code_generation[ ERecord_declr ](r);
					break;
				}
	default:		{
					UNKNOWN_TYPE(r->type);
					break;
				}
	}
}

static void catalog_spec_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ECatalog_spec1:	{
					break;
				}
	case ECatalog_spec2:	{
					break;
				}
	case ECatalog_spec3:	{
					break;
				}
	}
}

static void catalog_spec_code_generation(void *trans_node)
{
	TOUCH();

	struct catalog_spec *cs = trans_node;
	int action = cs->type;
	switch ( action ) {
	case ECatalog_spec1:	{
					fputs("enum { ", op);
					dont_insert_empty_lines = true;
					dont_cast = true;
					trans_catalog_list(cs->u.c1.c);
					dont_cast = false;
					dont_insert_empty_lines = false;
					fputs(" }", op);
					cs->d_type = cs->u.c1.c->d_type;

					num_catalogs = 1;

					semantic_analysis[ ECatalog_spec ](
							action,
							cs->u.c1.c->d_type);
					break;
				}
	case ECatalog_spec2:	{
					fputs("enum ", op);
					dont_insert_empty_lines = true;
					trans_identifier(cs->u.c2.i);
					fputs(" { ", op);
					cs->d_type = cs->u.c2.i->d_type;
					dont_cast = true;
					trans_catalog_list(cs->u.c2.c);
					dont_cast = false;
					fputs(" } ", op);
					dont_insert_empty_lines = false;

					num_catalogs = 1;

					semantic_analysis[ ECatalog_spec ](
							action,
							cs->u.c2.i->d_type,
							cs->u.c2.c->d_type);
					break;
				}
	case ECatalog_spec3:	{
					fputs("enum ", op);
					dont_insert_empty_lines = true;
					trans_identifier(cs->u.c3.i);
					fputc(' ', op);
					dont_insert_empty_lines = false;
					cs->d_type = cs->u.c3.i->d_type;

					semantic_analysis[ ECatalog_spec ](
							action,
							cs->u.c3.i->d_type);
					break;
				}
	}	
}

static void trans_catalog_spec(void *trans_node)
{
	HIT();

	struct catalog_spec *cs = trans_node;
	switch ( cs->type ) {
	case ECatalog_spec1:
	case ECatalog_spec2:
	case ECatalog_spec3:	{
					code_generation[ ECatalog_spec ](cs);
					break;
				}
	default:		{
					UNKNOWN_TYPE(cs->type);
					break;
				}
	}
}

static void catalog_list_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ECatalog_list1:	{
					break;
				}
	case ECatalog_list2:	{
					break;
				}
	}
}

static void catalog_list_code_generation(void *trans_node)
{
	TOUCH();

	struct catalog_list *cl = trans_node;
	int action = cl->type;
	switch ( action ) {
	case ECatalog_list1:	{
					trans_catalog_const(cl->u.c1.c);
					cl->d_type = cl->u.c1.c->d_type;

					semantic_analysis[ ECatalog_list ](
							action,
							cl->u.c1.c->d_type);
					break;
				}
	case ECatalog_list2:	{
					trans_catalog_list(cl->u.c2.c1);
					fputs(", ", op);
					trans_catalog_const(cl->u.c2.c2);
					cl->d_type = cl->u.c2.c2->d_type;

					semantic_analysis[ ECatalog_list ](
							action,
							cl->u.c2.c1->d_type,
							cl->u.c2.c2->d_type);
					break;
				}
	}
}

static void trans_catalog_list(void *trans_node)
{
	HIT();

	struct catalog_list *cl = trans_node;
	switch ( cl->type ) {
	case ECatalog_list1:
	case ECatalog_list2:	{
					code_generation[ ECatalog_list ](cl);
					break;
				}
	default:		{
					UNKNOWN_TYPE(cl->type);
					break;
				}
	}
}

static void catalog_const_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ECatalog_const1:	{
					break;
				}
	case ECatalog_const2:	{
					break;
				}
	}
}

static void catalog_const_code_generation(void *trans_node)
{
	TOUCH();

	struct catalog_const *cc = trans_node;
	int action = cc->type;
	switch ( action ) {
	case ECatalog_const1:	{
					if ( !(num_catalogs % 4) ) {
						fputs("\n\t\t", op);
					}

					trans_identifier(cc->u.c1.i);
					cc->d_type = cc->u.c1.i->d_type;

					num_catalogs++;

					semantic_analysis[ ECatalog_const ](
							action,
							cc->u.c1.i->d_type);
					break;
				}
	case ECatalog_const2:	{
					if ( !(num_catalogs % 4) ) {
						fputs("\n\t\t", op);
					}

					trans_identifier(cc->u.c2.i);
					fputs(" = ", op);
					trans_constant_expression(cc->u.c2.c);
					cc->d_type = cc->u.c2.c->d_type;

					num_catalogs++;

					semantic_analysis[ ECatalog_const ](
							action,
							cc->u.c2.i->d_type,
							cc->u.c2.c->d_type);
					break;
				}
	}
}

static void trans_catalog_const(void *trans_node)
{
	HIT();

	struct catalog_const *cc = trans_node;
	switch ( cc->type ) {
	case ECatalog_const1:
	case ECatalog_const2:	{
					code_generation[ ECatalog_const ](cc);
					break;
				}
	default:		{
					UNKNOWN_TYPE(cc->type);
					break;
				}
	}
}

static void type_qual_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EType_qual1:	{
					break;
				}
	}
}

static void type_qual_code_generation(void *trans_node)
{
	TOUCH();

	struct type_qual *t = trans_node;
	int action = t->type;
	switch ( action ) {
	case EType_qual1:	{
					t->d_type = unification(strdup("const"),
								NULL, EBasic);
					semantic_analysis[ EType_qual ](action,
								t->d_type);

					if ( !dont_put_src_line_num ) {
						EMBED_SRC_LINE();
						dont_put_src_line_num = true;
					}

					fputs("const ", op);
					break;
				}
	}	
}

static void trans_type_qual(void *trans_node)
{
	HIT();

	struct type_qual *t = trans_node;
	switch ( t->type ) {
	case EType_qual1:	{
					code_generation[ EType_qual ](t);
					break;
				}
	default:		{
					UNKNOWN_TYPE(t->type);
					break;
				}
	}
}

static void declarator_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EDecl1:	{
				break;
			}
	case EDecl2:	{
				break;
			}
	case EDecl3:	{
				break;
			}
	case EDecl4:	{
				break;
			}
	case EDecl5:	{
				break;
			}
	}
}

static void declarator_code_generation(void *trans_node)
{
	TOUCH();

	struct declarator *d = trans_node;
	int action = d->type;
	switch ( action ) {
	case EDecl1:	{
				trans_identifier(d->u.d1.i);
				d->d_type = d->u.d1.i->d_type;

				semantic_analysis[ EDeclarator ](action,
							d->u.d1.i->d_type);
				break;
			}
	case EDecl2:	{
				fputc('(', op);
				trans_declarator(d->u.d2.d);
				fputc(')', op);
				d->d_type = d->u.d2.d->d_type;

				semantic_analysis[ EDeclarator ](action,
							d->u.d2.d->d_type);
				break;
			}
	case EDecl3:	{
				func_name_seen = true;
				trans_declarator(d->u.d3.d);
				func_name_seen = false;

				fputs(func_name, op);

				d->d_type = d->u.d3.d->d_type;
				fputc('(', op);
				func_parameter_list_seen = true;
				trans_parameter_type_list(d->u.d3.p);
				func_parameter_list_seen = false;
				fputs(") ", op);

				semantic_analysis[ EDeclarator ](action,
							d->u.d3.d->d_type,
							d->u.d3.p->d_type);
				break;
			}
	case EDecl4:	{
				trans_declarator(d->u.d4.d);
				d->d_type = d->u.d4.d->d_type;

				fputs(func_name, op);

				fputc('(', op);
				trans_identifier_list(d->u.d4.i);
				fputc(')', op);

				semantic_analysis[ EDeclarator ](action,
							d->u.d4.d->d_type,
							d->u.d4.i->d_type);
				break;
			}
	case EDecl5:	{
				func_name_seen = true;
				trans_declarator(d->u.d5.d);
				func_name_seen = false;

				fputs(func_name, op);

				fputs("()", op);
				d->d_type = d->u.d5.d->d_type;

				semantic_analysis[ EDeclarator ](action,
							d->u.d5.d->d_type);
				break;
			}
	}	
}

static void trans_declarator(void *trans_node)
{
	HIT();

	struct declarator *d = trans_node;
	switch ( d->type ) {
	case EDecl1:
	case EDecl2:
	case EDecl3:
	case EDecl4:
	case EDecl5:	{
				code_generation[ EDeclarator ](d);
				break;
			}
	default:	{
				UNKNOWN_TYPE(d->type);
				break;
			}
	}
}

static void parameter_type_list_semantic_analysis(int action,
							struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EParameter_type_list:	{
						break;
					}
	}
}

static void parameter_type_list_code_generation(void *trans_node)
{
	TOUCH();

	struct parameter_type_list *p = trans_node;
	int action = p->type;
	switch ( action ) {
	case EParameter_type_list1:	{
						trans_parameter_list(p->u.p1.p);
						p->d_type = p->u.p1.p->d_type;

						semantic_analysis
						[ EParameter_type_list ](action,
							p->u.p1.p->d_type);
						break;
					}
	}	
}

static void trans_parameter_type_list(void *trans_node)
{
	HIT();

	struct parameter_type_list *p = trans_node;
	switch ( p->type ) {
	case EParameter_type_list1:	{
						code_generation
						[ EParameter_type_list ](p);
						break;
					}
	default:			{
						UNKNOWN_TYPE(p->type);
						break;
					}
	}
}

static void parameter_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EParameter_list1:	{
					break;
				}
	case EParameter_list2:	{
					break;
				}
	}
}

static void parameter_list_code_generation(void *trans_node)
{
	TOUCH();

	struct parameter_list *p = trans_node;
	int action = p->type;
	switch ( action ) {
	case EParameter_list1:	{
					trans_parameter_decln(p->u.p1.p);
					p->d_type = p->u.p1.p->d_type;

					semantic_analysis[ EParameter_list ](
							action,
							p->u.p1.p->d_type);
					break;
				}
	case EParameter_list2:	{
					trans_parameter_list(p->u.p2.p1);
					fputs(", ", op);
					trans_parameter_decln(p->u.p2.p2);
					p->d_type = p->u.p2.p2->d_type;

					semantic_analysis[ EParameter_list ](
							action,
							p->u.p2.p1->d_type,
							p->u.p2.p2->d_type);
					break;
				}
	}	
}

static void trans_parameter_list(void *trans_node)
{
	HIT();

	struct parameter_list *p = trans_node;
	switch ( p->type ) {
	case EParameter_list1:
	case EParameter_list2:	{
					code_generation[ EParameter_list ](p);
					break;
				}
	default:		{
					UNKNOWN_TYPE(p->type);
					break;
				}
	}	
}

static void parameter_decln_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EParameter_decln1:	{
					break;
				}
	case EParameter_decln2:	{
					break;
				}
	}
}

static void parameter_decln_code_generation(void *trans_node)
{
	TOUCH();

	struct parameter_decln *p = trans_node;
	int action = p->type;
	switch ( action ) {
	case EParameter_decln1:	{
					trans_decln_spec(p->u.p1.d1);
					trans_declarator(p->u.p1.d2);
					p->d_type = p->u.p1.d2->d_type;

					semantic_analysis[ EParameter_decln ](
							action,
							p->u.p1.d1->d_type,
							p->u.p1.d2->d_type);
					break;
				}
	case EParameter_decln2:	{
					trans_decln_spec(p->u.p2.d);
					p->d_type = p->u.p2.d->d_type;

					semantic_analysis[ EParameter_decln ](
							action,
							p->u.p2.d->d_type);
					break;
				}
	}
}

static void trans_parameter_decln(void *trans_node)
{
	HIT();

	struct parameter_decln *p = trans_node;
	switch ( p->type ) {
	case EParameter_decln1:
	case EParameter_decln2:	{
					code_generation[ EParameter_decln ](p);
					break;
				}
	default:		{
					UNKNOWN_TYPE(p->type);
					break;
				}
	}
}

static void identifier_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EIdentifier_list1:	{
					break;
				}
	case EIdentifier_list2:	{
					break;
				}
	}
}

static void identifier_list_code_generation(void *trans_node)
{
	TOUCH();

	struct identifier_list *i = trans_node;
	int action = i->type;
	switch ( action ) {
	case EIdentifier_list1: {
					trans_identifier(i->u.i1.i);
					i->d_type = i->u.i1.i->d_type;

					semantic_analysis[ EIdentifier_list ](
							action,
							i->u.i1.i->d_type);
					break;
				}
	case EIdentifier_list2: {
					trans_identifier_list(i->u.i2.i1);
					fputs(", ", op);
					trans_identifier(i->u.i2.i2);
					i->d_type = i->u.i2.i2->d_type;

					semantic_analysis[ EIdentifier_list ](
							action,
							i->u.i2.i1->d_type,
							i->u.i2.i2->d_type);
					break;
				}
	}
}

static void trans_identifier_list(void *trans_node)
{
	HIT();

	struct identifier_list *i = trans_node;
	switch ( i->type ) {
	case EIdentifier_list1:
	case EIdentifier_list2: {
					code_generation[ EIdentifier_list ](i);
					break;
				}
	default:		{
					UNKNOWN_TYPE(i->type);
					break;
				}
	}
}

static void initializer_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EInitializer1:	{
					break;
				}
	case EInitializer2:	{
					break;
				}
	}	
}

static void initializer_code_generation(void *trans_node)
{
	TOUCH();

	struct initializer *i = trans_node;
	int action = i->type;
	switch ( action ) {
	case EInitializer1:	{
					trans_assignment_expression(i->u.i1.e);
					i->d_type = i->u.i1.e->d_type;

					semantic_analysis[ EInitializer ](
							action,
							i->u.i1.e->d_type);
					break;
				}
	case EInitializer2:	{
					fputs("{ ", op);
					trans_initializer_list(i->u.i2.i);
					fputs("} ", op);
					i->d_type = i->u.i2.i->d_type;

					semantic_analysis[ EInitializer ](
							action,
							i->u.i2.i->d_type);
					break;
				}
	}
}

static void trans_initializer(void *trans_node)
{
	HIT();

	struct initializer *i = trans_node;
	switch ( i->type ) {
	case EInitializer1:
	case EInitializer2:	{
					code_generation[ EInitializer ](i);
					break;
				}
	default:		{
					UNKNOWN_TYPE(i->type);
					break;
				}
	}
}

static void initializer_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EInitializer_list1:	{
						break;
					}
	case EInitializer_list2:	{
						break;
					}
	}
}					

static void initializer_list_code_generation(void *trans_node)
{
	TOUCH();

	struct initializer_list *i = trans_node;
	int action = i->type;
	switch ( action ) {
	case EInitializer_list1:	{
						trans_initializer(i->u.i1.i);
						i->d_type = i->u.i1.i->d_type;

						semantic_analysis
						[ EInitializer_list ](
							action,
							i->u.i1.i->d_type);
						break;
					}
	case EInitializer_list2:	 	
					{
						trans_initializer_list(
								i->u.i2.i1);
						fputs(", ", op);
						trans_initializer(i->u.i2.i2);
						i->d_type = i->u.i2.i2->d_type;

						semantic_analysis
						[ EInitializer_list ](
							action,
							i->u.i2.i1->d_type,
							i->u.i2.i2->d_type);
						break;
					}
	}	
}

static void trans_initializer_list(void *trans_node)
{
	HIT();

	struct initializer_list *i = trans_node;
	switch ( i->type ) {
	case EInitializer_list1:
	case EInitializer_list2:	{
						code_generation
						[ EInitializer_list ](i);
						break;
					}
	default:			{
						UNKNOWN_TYPE(i->type);
						break;
					}
	}
}

static void statement_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EStatement1:	{
					break;
				}
	case EStatement2:	{
					break;
				}
	case EStatement3:	{
					break;
				}
	case EStatement4:	{
					break;
				}
	case EStatement5:	{
					break;
				}
	case EStatement6:	{
					break;
				}
	}		
}

static void statement_code_generation(void *trans_node)
{
	TOUCH();

	struct statement *s = trans_node;
	int action = s->type;
	switch ( action ) {
	case EStatement1:	{
					trans_expression_stmt(s->u.s1.e);
					s->d_type = s->u.s1.e->d_type;

					semantic_analysis[ EStatement ](action,
							s->u.s1.e->d_type);
					break;
				}
	case EStatement2:	{
					trans_block_stmt(s->u.s2.b);
					s->d_type = s->u.s2.b->d_type;

					semantic_analysis[ EStatement ](action,
							s->u.s2.b->d_type);
					break;
				}
	case EStatement3:	{
					trans_selection_stmt(s->u.s3.s);
					s->d_type = s->u.s3.s->d_type;

					semantic_analysis[ EStatement ](action,
							s->u.s3.s->d_type);
					break;
				}
	case EStatement4:	{
					trans_label_stmt(s->u.s4.l);
					s->d_type = s->u.s4.l->d_type;

					semantic_analysis[ EStatement ](action,
							s->u.s4.l->d_type);
					break;
				}
	case EStatement5:	{
					trans_iteration_stmt(s->u.s5.i);
					s->d_type = s->u.s5.i->d_type;

					semantic_analysis[ EStatement ](action,
							s->u.s5.i->d_type);
					break;
				}
	case EStatement6:	{
					trans_jump_stmt(s->u.s6.j);
					s->d_type = s->u.s6.j->d_type;

					semantic_analysis[ EStatement ](action,
							s->u.s6.j->d_type);
					break;
				}
	}
}

static void trans_statement(void *trans_node)
{

	HIT();

	struct statement *s = trans_node;
	switch ( s->type ) {
	case EStatement1:
	case EStatement2:
	case EStatement3:
	case EStatement4:
	case EStatement5:
	case EStatement6:	{
					code_generation[ EStatement ](s);
					break;
				}
	default:		{
					UNKNOWN_TYPE(s->type);
					break;
				}
	}
}

static void label_stmt_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ELabel1:	{
				break;
			}
	case ELabel2:	{
				break;
			}
	}
}

static void label_stmt_code_generation(void *trans_node)
{
	TOUCH();

	struct label_stmt *l = trans_node;
	int action = l->type;
	switch ( action ) {
	case ELabel1: 	{
				case_seen = true;
				fputs("case ", op);
				trans_constant_expression(l->u.l1.c);
				case_seen = false;
				fputs(": ", op);
				trans_statement(l->u.l1.s);
				l->d_type = l->u.l1.s->d_type;
				if ( fall_through_seen ) {
					// Yeah, do nothing! :P
					fall_through_seen = false;
				} else {
					fputs("break;", op);
				}

				semantic_analysis[ ELabel_stmt ](action,
						l->u.l1.c->d_type,
						l->u.l1.s->d_type);
				break;
			}
	case ELabel2: 	
			{
				fputs("default: ", op);
				trans_statement(l->u.l2.s);
				l->d_type = l->u.l2.s->d_type;
				if ( fall_through_seen ) {
					fall_through_seen = false;
				} else {
					fputs("break;", op);
				}

				semantic_analysis[ ELabel_stmt ](action,
						l->u.l2.s->d_type);
				break;
			}
	}	
}

static void trans_label_stmt(void *trans_node)
{
	HIT();

	struct label_stmt *l = trans_node;
	switch ( l->type ) {
	case ELabel1:
	case ELabel2:	{
				code_generation[ ELabel_stmt ](l);
				break;
			}
	default:	{
				UNKNOWN_TYPE(l->type);
				break;
			}
	}
}

static void expression_stmt_semantic_analysis(int action,
						struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EExp_stmt1:	{
					break;
				}
	case EExp_stmt2:	{
					break;
				}
	}
}

static void dump_players_init_code(void)
{

	fputs("\tplayers.clear();\n"
        	"\tfor (int i = 0; i < numPlayers; i++) { \n"
                "\t\tPlayer *p = new Player();\n"
		"\t\tp->name = string(\"Player \") + str(i+1);\n"
		"\t}\n\n", op);
}

static void expression_stmt_code_generation(void *trans_node)
{
	TOUCH();

	struct expression_stmt *e = trans_node;
	int action = e->type;
	switch ( action ) { 
	case EExp_stmt1: 	{
					e->d_type = unification(
						strdup("NULL_STMT"), NULL,
								EBasic);

					semantic_analysis[ EExp_stmt ](action,
								e->d_type);

					INDENT_ONCE();
					fputs(";\n", op);
					break;
				}
	case EExp_stmt2: 	
				{
					e->d_type = unification(
						strdup("EXPRESSION_STMT"),
								NULL, EBasic);
					semantic_analysis[ EExp_stmt ](action,
								e->d_type);

					EMBED_SRC_LINE();

					INDENT();
					trans_expression(e->u.e2.e);

					if ( !dont_put_semicolon ) {

						if ( put_close_paren ) {
							fputc(')', op);
							put_close_paren = false;
						}

						fputs(";\n", op);
				
						e->indentation =
							previous_indentation;	
						INDENT_ONCE();
						if ( winner_was_chosen ) {
							fputs(
							"\tdrawBoard(winner);\n",
									op);
							winner_was_chosen
									= false;
						}

					} else {
						dont_put_semicolon = false;
					}

					if ( num_players_var_seen ) {
						num_players_var_seen = false;

						dump_players_init_code();
					}
						
					// e->num_of_empty_lines
						// = newlines_needed;
					// newlines_needed = 0;
					INSERT_EMPTY_LINES();
					break;
				}
	}	
}

static void trans_expression_stmt(void *trans_node)
{
	HIT();

	struct expression_stmt *e = trans_node;
	switch ( e->type ) { 
	case EExp_stmt1:
	case EExp_stmt2:	{
					code_generation[ EExp_stmt ](e);
					break;
				}
	default:		{
					UNKNOWN_TYPE(e->type);
					break;
				}
	}	
}

static void block_stmt_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EBlk1:	{
				break;
			}
	case EBlk2:	{
				break;
			}
	case EBlk3:	{
				break;
			}
	case EBlk4:	{
				break;
			}
	}
}

static void block_stmt_code_generator1(void *trans_node)
{
	TOUCH();

	struct block_stmt *b = trans_node;

	active_block++;

	if ( active_block > EMax_nested_blocks ) {
		COMPILATION_ERROR("Sorry! Maximum nested blocks allowed : %d\n",
					EMax_nested_blocks);
	}

	if ( !if_seen && !else_seen ) {
		INDENT();
	} else {
		b->indentation = previous_indentation;
	}

	if ( else_seen ) {
		else_seen = false;
	}

	fputs("{\n", op);

	block_stmt_seen = true;

	if ( register_crash_handler ) {
		fputs("\n\tsignal(SIGSEGV, CrashHandler);\n\n", op);
		register_crash_handler = false;

		fputs("\n\tinit();\n", op);
        }

	trans_decl_list_or_stmt_list(b->u.b1.ds);

	if ( main_func_begun && active_block == 1 ) {
		fputs("\n\tcleanup();\n\n"
			"\treturn 0;\n", op);
	}

	if ( opening_brace_indentation ) {
		b->indentation = opening_brace_indentation;
		opening_brace_indentation = false;
	}
	if ( b->indentation && b->indentation[ 0 ] ) {
		INDENT_ONCE();
	} else {
		fputc('\t', op);
	}
	fputs("}\n", op);

	b->d_type = b->u.b1.ds->d_type;

	block_stmt_seen = false;

	if ( active_block ) {
		free_symbol_list(active_block);
		active_block--;
	}
}

static void block_stmt_code_generator2(void *trans_node)
{
	TOUCH();

	struct block_stmt *b = trans_node;

	trans_game_elem(b->u.b2.g);

	INDENT();
	fputs("{\n", op);

	bool not_setup = b->u.b2.g->type != EGame1;
	if ( not_setup ) {
		fputs("drawBoard(currPlayer);\n", op);
		active_block++;
	} else {
		previous_indentation = b->indentation = strdup("\t\t");
		INDENT();
	}

	if ( active_block > EMax_nested_blocks ) {
		COMPILATION_ERROR("Maximum nested blocks allowed : %d\n",
					EMax_nested_blocks);
	}

	if ( !not_setup ) {
		fputs("\n\tcurrPlayer = NULL;\n", op);
		fputs("\n\tPlayer *winner = NULL;\n", op);
	}

	trans_decl_list_or_stmt_list(b->u.b2.ds);
	
	b->d_type = b->u.b2.ds->d_type;

	if ( not_setup ) {
		b->indentation = round_game_elem_identation;
		round_game_elem_identation = NULL;
		INDENT_ONCE();

		fputs("}\n", op);

		if ( put_flower_bracket ) {
			fputs("\n}\n", op);
			put_flower_bracket = false;
		}

		if ( active_block ) {
			free_symbol_list(active_block);
			active_block--;
		}
	} else {
		INDENT_ONCE();

		put_flower_bracket = true;

		setup_seen = false;
		/*if ( enum_card_attr ) {
			fputs(enum_card_attr, intrmd);
			// Initially strdup()'d in cgdl.l
			// and realloc()'d thereafter in
			// this file.
			free(enum_card_attr);
			enum_card_attr = NULL;
			fputs("};\n", intrmd); 
			fprintf(intrmd,"\nint num_card_attributes "
				"= %d;\n\n", num_card_attr);
		}*//*
		if ( enum_player_attrib ) {
			fputs(enum_player_attrib, intrmd);
			// Initially strdup()'d in cgdl.l
			// and realloc()'d thereafter in
			// this file.
			free(enum_player_attrib);
			enum_player_attrib = NULL;
			fputs("};\n", intrmd);
			fprintf(intrmd,
				"\nint num_player_attributes "
				"= %d;\n", num_player_attr);
		}*/
	}
}

static void block_stmt_code_generator3(void *trans_node)
{
	TOUCH();

	struct block_stmt *b = trans_node;
	
	active_block++;

	if ( active_block > EMax_nested_blocks ) {
		COMPILATION_ERROR("Maximum nested blocks allowed : %d\n",
					EMax_nested_blocks);
	}

	b->d_type = unification(strdup("EMPTY_BLOCK_STMT"), NULL,
						EComp);

	fputs("\n{\n", op);

	if ( register_crash_handler ) {
		fputs("\n\tsignal(SIGSEGV, CrashHandler);\n\n", op);
		register_crash_handler = false;
        }

	if ( main_func_begun && active_block == 1 ) {
		fputs("\n\treturn 0;\n", op);
	} 

	fputs("\n}\n", op);

	active_block--;
}

static void block_stmt_code_generator4(void *trans_node)
{
	TOUCH();

	struct block_stmt *b = trans_node;

	active_block++;

	if ( active_block > EMax_nested_blocks ) {
		COMPILATION_ERROR("Maximum nested blocks allowed : %d\n",
					EMax_nested_blocks);
	}

	trans_game_elem(b->u.b4.g);

	b->d_type = unification(strdup("EMPTY_GAME_BLOCK_STMT"), NULL,
						EBasic);


	bool not_setup = b->u.b2.g->type != EGame1;
	if ( not_setup ) {
		fputs("{\n", op);
		fputs("\t\tdrawBoard(currPlayer);\n", op);
		fputs("}\n", op);
		if ( put_flower_bracket ) {
			fputs("\n}\n", op);
			put_flower_bracket = false;
		}
	} else {

		fputs("{\n", op);

		fputs("\n\tcurrPlayer = NULL;\n"
		"\tPlayer *winner = NULL;\n", op);
		
		put_flower_bracket = true;
	}


	active_block--;
}

static void block_stmt_code_generation(void *trans_node)
{
	TOUCH();

	struct block_stmt *b = trans_node;
	int action = b->type;
	switch ( action ) {
	case EBlk1:	{
				block_stmt_code_generator1(b);
			
				semantic_analysis[ EBlock_stmt ](action,
							b->u.b1.ds->d_type);
				break;
			}
	case EBlk2:	{
				block_stmt_code_generator2(b);

				semantic_analysis[ EBlock_stmt ](action,
							b->u.b2.g->d_type,
							b->u.b2.ds->d_type);
				break;
			}				
	case EBlk3:	{
				block_stmt_code_generator3(b);

				semantic_analysis[ EBlock_stmt ](action,
							b->d_type);
				break;
			}
	case EBlk4:	{
				block_stmt_code_generator4(b);

				semantic_analysis[ EBlock_stmt ](action,
							b->u.b4.g->d_type,
							b->d_type);
				break;
			}
	}
}

static void trans_block_stmt(void *trans_node)
{
	HIT();

	struct block_stmt *b = trans_node;
	switch ( b->type ) {
	case EBlk1:
	case EBlk2:
	case EBlk3:
	case EBlk4:	{
				code_generation[ EBlock_stmt ](b);
				break;
			}
	default:	{
				UNKNOWN_TYPE(b->type);
				break;
			}
	}
}

static void decl_list_or_stmt_list_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EDecl_list_stmt_list1:	{
						break;
					}
	case EDecl_list_stmt_list2:	{
						break;
					}
	case EDecl_list_stmt_list3:	{
						break;
					}
	case EDecl_list_stmt_list4:	{
						break;
					}	
	}
}

static void decl_list_or_stmt_list_code_generation(void *trans_node)
{
	TOUCH();

	struct decln_list_or_stmt_list *d = trans_node;
	int action = d->type;
	switch ( action ) {
	case EDecl_list_stmt_list1: 	{
						trans_declaration(d->u.d1.d);
						d->d_type = d->u.d1.d->d_type;

						semantic_analysis
						[ EDecl_list_stmt_list ](action,
							d->u.d1.d->d_type);
						break;
					}
	case EDecl_list_stmt_list2: 	{
						trans_decl_list_or_stmt_list(
								d->u.d2.d1);
						trans_declaration(d->u.d2.d2);
						d->d_type = d->u.d2.d2->d_type;

						semantic_analysis
						[ EDecl_list_stmt_list ](action,
							d->u.d2.d1->d_type,
							d->u.d2.d2->d_type);
						break;
					}
	case EDecl_list_stmt_list3: 	{
						trans_statement(d->u.d3.s);
						d->d_type = d->u.d3.s->d_type;

						semantic_analysis
						[ EDecl_list_stmt_list ](action,
							d->u.d3.s->d_type);
						break;
					}
	case EDecl_list_stmt_list4:	{
						trans_decl_list_or_stmt_list(
								d->u.d4.d);
						trans_statement(d->u.d4.s);
						d->d_type = d->u.d4.s->d_type;

						semantic_analysis
						[ EDecl_list_stmt_list ](action,
							d->u.d4.d->d_type,
							d->u.d4.s->d_type);
						break;
					}
	}
}

static void trans_decl_list_or_stmt_list(void *trans_node)
{
	HIT();

	struct decln_list_or_stmt_list *d = trans_node;
	switch ( d->type ) {
	case EDecl_list_stmt_list1:
	case EDecl_list_stmt_list2:
	case EDecl_list_stmt_list3:
	case EDecl_list_stmt_list4:	{
						code_generation
						[ EDecl_list_stmt_list ](d);
						break;
					}
	default:			{
						UNKNOWN_TYPE(d->type);
						break;
					}
	}
}

static void selection_stmt_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2,
						struct unified_type *t3)
{
	TOUCH();

	switch ( action ) {
	case ESelection1:	{
					break;
				}
	case ESelection2:	{
					break;
				}
	case ESelection3:	{
					break;
				}
	}	
}

static void selection_stmt_code_generation(void *trans_node)
{
	TOUCH();

	struct selection_stmt *s = trans_node;
	int action = s->type;
	switch ( s->type ) {
	case ESelection1:	{
					// Print "if (" at the
					// place where you have the
					// indentation stored.
					if_seen = true;
					if_printed = false;
					trans_expression(s->u.s1.e);
					fputs(") ", op);
					trans_statement(s->u.s1.s);
					s->d_type = s->u.s1.s->d_type;
					if_seen = false;

					if ( else_seen ) {
						else_seen = false;
					}

					semantic_analysis
					[ ESelection_stmt ](action,
						s->u.s1.e->d_type,
						s->u.s1.s->d_type);
					break;
				}
	case ESelection2: 	{
					if_seen = true;
					if_printed = false;
					trans_expression(s->u.s2.e);
					fputs(") ", op);
					trans_statement(s->u.s2.s1);

					s->indentation = previous_indentation;
					INDENT_ONCE();	
					fputs("else ", op);
					else_seen = true;

					trans_statement(s->u.s2.s2);
					s->d_type = s->u.s2.s2->d_type;

					semantic_analysis
					[ ESelection_stmt ](action,
						s->u.s2.e->d_type,
						s->u.s2.s1->d_type,
						s->u.s2.s2->d_type);			
					break;
				}
	case ESelection3: 	{
					fprintf(op, "switch (");
					switch_seen = true;
					trans_expression(s->u.s3.e);
					switch_seen = false;
					fputs(") ", op);
					trans_statement(s->u.s3.s);
					s->d_type = s->u.s3.s->d_type;

					semantic_analysis
					[ ESelection_stmt ](action,
						s->u.s3.e->d_type,
						s->u.s3.s->d_type);
					break;
				}
	}
}

static void trans_selection_stmt(void *trans_node)
{
	HIT();

	struct selection_stmt *s = trans_node;
	switch ( s->type ) {
	case ESelection1:
	case ESelection2:
	case ESelection3:	{
					code_generation[ ESelection_stmt ](s);
					break;
				}
	default:		{
					UNKNOWN_TYPE(s->type);
					break;
				}
	}
}

static void iteration_stmt_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2,
						struct unified_type *t3)
{
	TOUCH();

	switch ( action ) {
	case EIteration1:	{
					break;
				}
	case EIteration2:	{
					break;
				}
	case EIteration3:	{
					break;
				}	
	case EIteration4:	{
					break;
				}
	case EIteration5:	{
					break;
				}

	}
}

static void iteration_stmt_code_generation(void *trans_node)
{
	TOUCH();

	struct iteration_stmt *i = trans_node;
	int action = i->type;
	switch ( action ) {
	case EIteration1: 	{
					INDENT_ONCE();
					fputs("while (!(", op);
					trans_expression(i->u.i1.e);
					fputs(")) ", op);

					trans_statement(i->u.i1.s);
					
					i->d_type = i->u.i1.s->d_type;

					semantic_analysis[ EIteration_stmt ](
							action,
							i->u.i1.s->d_type,
							i->u.i1.e->d_type);
					break;
				}
	case EIteration2: 	{
					if ( setup_seen ) {
						correct_indentation_for_setup();
					}

					char *t = previous_indentation;
					i->indentation = t;
					INDENT();

					bool player_it = false;
					bool card_it = false;

					if ( !strcmp(i->u.i2.i->u.i1.i,
							"players") ) {
						player_it = true;

						fputs(
						"for ("
						"vector<Player *>::iterator"
						" __it = players.begin();\n", op);
						INDENT();
						fputs("\t\t (void)(__it != players.end() &&"
						" (player = *__it)),"
						" __it != players.end();\n", op);
						INDENT();
						fputs("\t\t __it++) ", op);
					} else if ( !strcmp(i->u.i2.i->u.i1.i,
							"pile") ) {
						card_it = true;

						fputs(
						"for ("
						"deque<Card *>::iterator"
						" pileIt = pile->cards.begin();"
						"\n", op);
						INDENT();
						fputs("\t\t (void)(pileIt != "
							"pile->cards.end() &&"
							" (card = *pileIt)), "
							" pileIt != "
							"pile->cards.end();"
							"\n", op);
						INDENT();
						fputs("\t\t pileIt++) ", op);
					}

					free(i->u.i2.i->u.i1.i);
					i->u.i2.i->u.i1.i = NULL;

					if ( t ) {
						char ind[ strlen(t) * 2 + 1 ];
						strcpy(ind, t);
						strcat(ind, t);
						previous_indentation = strdup(ind);
					} else {
						free(previous_indentation);
						previous_indentation = strdup("\t\t");
					}

					trans_statement(i->u.i2.s);
					i->d_type = i->u.i2.s->d_type;

					fputc('\n', op);
					if ( t && t[ 0 ] ) {
						i->indentation = t;
						INDENT_ONCE();
					} else {
						correct_indentation_for_setup();
						fputc('\t', op);
					}

					if ( player_it ) {
						fputs("player = NULL;\n", op);
					} else if ( card_it ) {
						fputs("card = NULL;\n", op);
					}

					semantic_analysis[ EIteration_stmt ](
							action,
							i->u.i2.i->d_type,
							i->u.i2.s->d_type);
					break;
				}
	case EIteration3: 	{
					if ( setup_seen ) {
						correct_indentation_for_setup();
					}

					char *t = previous_indentation;
					i->indentation = t;
					INDENT();

					char *it = i->u.i3.i1->u.i1.i;
					if ( !strcmp(i->u.i3.i2->u.i1.i,
							"players") ) {
						fputs("{\n", op);
						INDENT();

						fprintf(op,
							"Player *%s = NULL;\n",
							i->u.i3.i1->u.i1.i);
						INDENT();

						fputs(
						"for ("
						"vector<Player *>::iterator"
						" __it = players.begin();\n", op);
						INDENT();
						fprintf(op,
						"\t\t (void)(__it != players.end() &&"
						" (%s = *__it)),"
						" __it != players.end();\n ", it);
						INDENT();
						fputs("\t\t __it++) ", op);
					} else if ( !strcmp(i->u.i3.i2->u.i1.i,
							"pile") ) {
						fputs("{\n", op);
						INDENT();

						fprintf(op,
							"Card *%s = NULL;\n",
							i->u.i3.i1->u.i1.i);
						INDENT();
						fputs(
						"for ("
						"deque<Card *>::iterator"
						" pileIt = pile->cards.begin();"
						"\n", op);
						INDENT();
						fprintf(op,
							"\t\t (void)(pileIt != "
							"pile->cards.end() &&"
							" (%s = *pileIt)), "
							" pileIt != "
							"pile->cards.end();"
							"\n", i->u.i3.i1->u.i1.i);
						INDENT();
						fputs("\t\t pileIt++) ", op);
					}

					free(i->u.i3.i1->u.i1.i);
					i->u.i3.i1->u.i1.i = NULL;
					free(i->u.i3.i2->u.i1.i);
					i->u.i3.i2->u.i1.i = NULL;

					if ( t ) {
						char ind[ strlen(t) * 2 + 1 ];
						strcpy(ind, t);
						strcat(ind, t);
						previous_indentation = strdup(ind);
					} else {
						free(previous_indentation);
						previous_indentation = strdup("\t\t");
					}

					trans_statement(i->u.i3.s);
					i->d_type = i->u.i3.s->d_type;

					INDENT();
					fputs("}\n", op);

					fputc('\n', op);
					if ( t && t[ 0 ] ) {
						i->indentation = t;
						INDENT_ONCE();
					} else {
						correct_indentation_for_setup();
						fputc('\t', op);
					}

					semantic_analysis[ EIteration_stmt ](
							action,
							i->u.i3.i1->d_type,
							i->u.i3.i2->d_type,
							i->u.i3.s->d_type);
					break;
				}
	case EIteration4: 	{
					INDENT_ONCE();

					for_loop_seen = true;
					char v[ strlen(i->u.i4.i1->u.i1.i) + 1 ];
					strcpy(v, i->u.i4.i1->u.i1.i);
					trans_identifier(i->u.i4.i1);
					for_loop_seen = false;
					fputs(" = ", op);
					fputs(" 0", op);
					//trans_constant_expression(i->u.i4.c);
					fputs(";\n", op);
					fputs(v, op);
					fputs(" < ", op);
					trans_constant_expression(i->u.i4.c);
					fputs(";\n", op);
					fputs(v, op);
					//fputs("-- ) ", op);
					fputs("++ ) ", op);
					trans_statement(i->u.i4.s);
					i->d_type = i->u.i4.s->d_type;

					semantic_analysis[ EIteration_stmt ](
							action,
							i->u.i4.i1->d_type,
							i->u.i4.c->d_type,
							i->u.i4.s->d_type);
					break;
				}
	case EIteration5: 	{
					INDENT_ONCE();
					while_seen = true;
					trans_constant_expression(i->u.i5.c);
					while_seen = false;
					fputs("; __i; __i--) ", op);
					trans_statement(i->u.i5.s);
					i->d_type = i->u.i5.s->d_type;

					semantic_analysis[ EIteration_stmt ](
							action,
							i->u.i5.c->d_type,
							i->u.i5.s->d_type);
					break;
				}
	}
}

static void trans_iteration_stmt(void *trans_node)
{
	HIT();

	struct iteration_stmt *i = trans_node;
	switch ( i->type ) {
	case EIteration1:
	case EIteration2:
	case EIteration3:
	case EIteration4:
	case EIteration5:	{
					code_generation[ EIteration_stmt ](i);
					break;
				}
	default:		{
					UNKNOWN_TYPE(i->type);
					break;
				}
	}
}

static void jump_stmt_semantic_analysis(int action, struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case EJump1:	{
				break;
			}
	case EJump2:	{
				break;
			}
	case EJump3:	{
				break;
			}
	case EJump4:	{
				break;
			}
	case EJump5:	{
				break;
			}
	}
}

static void jump_stmt_code_generation(void *trans_node)
{
	TOUCH();

	struct jump_stmt *j = trans_node;
	int action = j->type;
	switch ( action ) {
	case EJump1:	{
				j->d_type = unification(
					strdup("CONTINUE"), NULL, EBasic);
				semantic_analysis[ EJump_stmt ](action,
								j->d_type);

				INDENT_ONCE();
				fputs("continue;\n", op);
				break;
			}
	case EJump2:	{
				j->d_type = unification(
					strdup("FALLTHROUGH"), NULL, EBasic);
				semantic_analysis[ EJump_stmt ](action,
								j->d_type);				

				fall_through_seen = true;
				break;
			}
	case EJump3:	{
				j->d_type = unification(
						strdup("BREAK"), NULL, EBasic);
				semantic_analysis[ EJump_stmt ](action,
								j->d_type);				

				INDENT_ONCE();
				fputs("break;\n", op);
				break;
			}
	case EJump4:	{
				j->d_type = unification(
					strdup("RETURN EXPRESSION"), NULL,
									EBasic);
				j->indentation = previous_indentation;
				INDENT_ONCE();
				return_exp_seen = true;
				fputs("return ", op);
				trans_expression(j->u.j4.e);
				return_exp_seen = false;

				semantic_analysis[ EJump_stmt ](action,
								j->d_type,
							j->u.j4.e->d_type);
				fputs(";\n", op);
				break;
			}
	case EJump5:	{
				j->d_type = unification(
					strdup("RETURN;"), NULL, EBasic);
				semantic_analysis[ EJump_stmt ](action,
								j->d_type);

				INDENT_ONCE();
				fputs("return;\n", op);
				break;
			}
	}
}

static void trans_jump_stmt(void *trans_node)
{
	HIT();

	struct jump_stmt *j = trans_node;
	switch ( j->type ) {
	case EJump1:
	case EJump2:
	case EJump3:
	case EJump4:
	case EJump5:	{
				code_generation[ EJump_stmt ](j);
				break;
			}

	default:	{
				UNKNOWN_TYPE(j->type);
				break;
			}
	}
}

static void attribute_addition_spec_semantic_analysis(int action,
						struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EAttr_add1:	{
					break;
				}
	case EAttr_add2:	{
					break;
				}
	}
}						

static void attribute_addition_spec_code_generator1(void *trans_node)
{
	TOUCH();

	struct attribute_addition_spec *a = trans_node;

	struct identifier *i1 = a->u.a1.i1;
	const char *t = type_specifiers[ a->u.a1.t->type ];
	struct identifier *i2 = a->u.a1.i2;

	if ( !strcmp(i1->u.i1.i, "addPlayerField") ) {
			if ( !strcmp(t, "number") ) {
				char *e = realloc(enum_player_number,
						strlen(enum_player_number)
						+ strlen("PLAYER_")
						+ strlen(i2->u.i1.i)
						+ strlen(",")
						+ 1);
				if ( e ) {
					char *s = str_upper(i2->u.i1.i);
					sprintf(e + strlen(e),
						"PLAYER_%s, ", s);
					free(s);
					s = NULL;
					num_player_number++;
					enum_player_number = e;
				}
			} else if ( !strcmp(t, "string") ) {
				char *e = realloc(enum_player_string,
						strlen(enum_player_string)
						+ strlen("PLAYER_")
						+ strlen(i2->u.i1.i)
						+ strlen(",")
						+ 1);
				if ( e ) {
					char *s = str_upper(i2->u.i1.i);
					sprintf(e + strlen(e),
						"PLAYER_%s, ", s);
					free(s);
					s = NULL;
					num_player_string++;
					enum_player_string = e;
				}
			} else if ( !strcmp(t, "bool") ) {
				char *e = realloc(enum_player_bool,
						strlen(enum_player_bool)
						+ strlen("PLAYER_")
						+ strlen(i2->u.i1.i)
						+ strlen(",")
						+ 1);
				if ( e ) {
					char *s = str_upper(i2->u.i1.i);
					sprintf(e + strlen(e),
						"PLAYER_%s, ", s);
					free(s);
					s = NULL;
					num_player_bool++;
					enum_player_bool = e;
				}
			} else {
				printf("UNKNOWN TYPE FOR addPlayerField!!!!\n");
			}
	} 				
}

static void attribute_addition_spec_code_generator2(void *trans_node)
{
	TOUCH();

	struct attribute_addition_spec *a = trans_node;

	struct identifier *i1 = a->u.a2.i1;
	struct identifier *i2 = a->u.a2.i2;

	if ( !strcmp(i1->u.i1.i, 
			"addCardAttribute") ) {

		char *e = realloc(enum_card_attr,
					strlen(enum_card_attr)
					+ strlen("CARD_")
					+ strlen(i2->u.i1.i)
					+ strlen(", ")
					+ 1);
		if ( e ) {
			char *s = str_upper(i2->u.i1.i);
			sprintf(e + strlen(e), "CARD_%s, ", s);
			free(s);
			s = NULL;
			num_card_attributes++;
			enum_card_attr = e;
		}
	} 

	a->d_type = i2->d_type;
}

static void attribute_addition_spec_code_generation(void *trans_node)
{
	TOUCH();

	struct attribute_addition_spec *a = trans_node;
	int action = a->type;
	switch ( action ) {
	case EAttr_add1:	{
					attribute_addition_spec_code_generator1
									(a);

					semantic_analysis[ EAttr_add_spec ](
							action,
							a->type);				
					break;
				}
	case EAttr_add2:	{
					attribute_addition_spec_code_generator2
									(a);

					semantic_analysis[ EAttr_add_spec ](
							action,
							a->d_type);
					break;
				}
	}	
}

static void trans_attribute_addition_spec(void *trans_node)
{
	HIT();

	struct attribute_addition_spec *a = trans_node;
	switch ( a->type ) {
	case EAttr_add1:
	case EAttr_add2:	{
					code_generation[ EAttr_add_spec ](a);
					break;
				}
	default:		{
					UNKNOWN_TYPE(a->type);
					break;
				}
	}
}

static void translation_unit_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2)
{
	TOUCH();

	switch ( action ) {
	case ETranslation1:	{
					break;
				}
	case ETranslation2:	{
					break;
				}
	}
}

static void translation_unit_code_generation(void *trans_node)
{
	TOUCH();

	struct translation_unit *t = trans_node;
	int action = t->type;
	switch ( action ) {
	case ETranslation1: 	{
					trans_elements(t->u.t1.e); 
					t->d_type = t->u.t1.e->d_type;

					semantic_analysis[ ETranslation_unit ](
							action,
							t->u.t1.e->d_type);
					break;
				}
	case ETranslation2: 	{
					trans_translation_unit(t->u.t2.t);
					trans_elements(t->u.t2.e);
					t->d_type = t->u.t2.e->d_type;

					if ( t->u.t2.t ) {
					semantic_analysis[ ETranslation_unit ](
							action,
							t->u.t2.t->d_type,
							t->u.t2.e->d_type);
					}
					break;
				}
	}	
}

static void trans_translation_unit(void *trans_node)
{
	HIT();

	struct translation_unit *t = trans_node;
	switch ( t->type ) {
	case ETranslation1:
	case ETranslation2:	{
					code_generation[ ETranslation_unit ]
									(t);
					break;
				}
	default:		{
					UNKNOWN_TYPE(t->type);
					break;
				}
	}
}

static void elements_semantic_analysis(int action, struct unified_type *t1)
{
	TOUCH();

	switch ( action ) {
	case EElements1:	{
					break;
				}
	case EElements2:	{
					break;
				}
	case EElements3:	{
					break;
				}
	case EElements4:	{
					break;
				}
	}
}

static void elements_code_generation(void *trans_node)
{
	TOUCH();

	struct elements *e = trans_node;
	int action = e->type;
	switch ( action ) {
	case EElements1: 	{
					trans_function_definition(e->u.e1.f);
					e->d_type = e->u.e1.f->d_type;

					semantic_analysis[ EElements ](action,
							e->u.e1.f->d_type);
					break;
				}
	case EElements2: 	{
					trans_procedure_definition(e->u.e2.p);
					e->d_type = e->u.e2.p->d_type;

					semantic_analysis[ EElements ](action,
							e->u.e2.p->d_type);
					break;
				}
	case EElements3: 	{
					trans_declaration(e->u.e3.d);
					e->d_type = e->u.e3.d->d_type;

					semantic_analysis[ EElements ](action,
							e->u.e3.d->d_type);
					break;
				}
	case EElements4:	{
					trans_attribute_addition_spec(e->u.e4.a);
					e->d_type = e->u.e4.a->d_type;

					semantic_analysis[ EElements ](action,
							e->u.e4.a->d_type);
					break;
				}
	}
}

static void trans_elements(void *trans_node)
{
	HIT();

	struct elements *e = trans_node;
	switch ( e->type ) {
	case EElements1:
	case EElements2:
	case EElements3:
	case EElements4:	{
					code_generation[ EElements ](e);
					break;
				}
	default:		{
					UNKNOWN_TYPE(e->type);
					break;
				}
	}
}

static void dump_dynamic_header(void)
{
	TOUCH();

	fputs(dynamic_definitions, dynamic);
	free(dynamic_definitions);
	dynamic_definitions = NULL;

	fputs("\n\n", dynamic);
	fputs(attr_enums, dynamic);
	fputs("\n\n", dynamic);

	fputs(enum_card_attr, dynamic);
	fputs("NUM_CARD_ATTRIBUTES };\n", dynamic);
	fputs(enum_player_number, dynamic);
	fputs("NUM_PLAYER_NUMBERS };\n", dynamic);
	fputs(enum_player_string, dynamic);
	fputs("NUM_PLAYER_STRINGS };\n", dynamic);
	fputs(enum_player_bool, dynamic);
	fputs("NUM_PLAYER_BOOLS };\n", dynamic);

	fputs("\n\n"
		"void initFields() {\n", dynamic);
	char *t = enum_card_attr + strlen("enum CardAttribute { ");
	while ( num_card_attributes --> 0 ) {
		char a[ 100 ] = "";
		sscanf(t, "%[^,]", a);
		t += strlen(a) + 1/* Comma */ + 1/* Space*/;
		char *a_n = strchr(a, '_');
		if ( a_n ) {
			a_n = str_lower(a_n + 1);
			fputs("\tcardAttributeNames.push_back(\"", dynamic);
			fputs(a_n, dynamic);
			free(a_n);
			a_n = NULL;
			fputs("\");\n", dynamic);
		}
	}
	t = enum_player_number + strlen("enum PlayerNumber { ");
	while ( num_player_number --> 0 ) {
		char a[ 100 ] = "";
		sscanf(t, "%[^,]", a);
		t += strlen(a) + 1/* Comma */ + 1/* Space*/;
		char *a_n = strchr(a, '_');
		if ( a_n ) {
			a_n = str_lower(a_n + 1);
			fputs("\tplayerNumberFieldNames.push_back(\"", dynamic);
			fputs(a_n, dynamic);
			free(a_n);
			a_n = NULL;
			fputs("\");\n", dynamic);
		}
	}
	t = enum_player_string + strlen("enum PlayerString { ");
	while ( num_player_string --> 0 ) {
		char a[ 100 ] = "";
		sscanf(t, "%[^,]", a);
		t += strlen(a) + 1/* Comma */ + 1/* Space*/;
		char *a_n = strchr(a, '_');
		if ( a_n ) {
			a_n = str_lower(a_n + 1);
			fputs("\tplayerStringFieldNames.push_back(\"", dynamic);
			fputs(a_n, dynamic);
			free(a_n);
			a_n = NULL;
			fputs("\");\n", dynamic);
		}
	}
	t = enum_player_bool + strlen("enum PlayerBool { ");
	while ( num_player_bool --> 0 ) {
		char a[ 100 ] = "";
		sscanf(t, "%[^,]", a);
		t += strlen(a) + 1/* Comma */ + 1/* Space*/;
		char *a_n = strchr(a, '_');
		if ( a_n ) {
			a_n = str_lower(a_n + 1);
			fputs("\tplayerBoolFieldNames.push_back(\"", dynamic);
			fputs(a_n, dynamic);
			free(a_n);
			a_n = NULL;
			fputs("\");\n", dynamic);
		}
	}

	fputs("}\n", dynamic);
}

static void function_definition_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2,
						struct function_definition *f)
{
	TOUCH();

	switch ( action ) {
	case EFunction1:	{
					struct func_type *ft = malloc(sizeof(*ft));
					if ( ft ) {
						printf("func_name : %s \n", func_name);
						ft->name = func_name;
						ft->parameter_list =
							func_parameter_list;
						ft->ret_type = func_ret_type;
						// Don't free. Just reset!
						func_name = NULL;
						func_parameter_list = NULL;
						func_ret_type = NULL; 
						add_func_type(ft, f->src_line);
					}

					struct unified_type *u =
							malloc(sizeof(*u));
					if ( u ) {
						u->type = EFunc;
						u->u.f = ft;
					}
					f->d_type = u;
					break;
				}
	}
}

static void function_definition_code_generation(void *trans_node)
{
	TOUCH();

	struct function_definition *f = trans_node;
	int action = f->type;
	switch ( action ) {
	case EFunction1: 	{
					if ( first_func_or_proc ) {
						dump_dynamic_header();
						first_func_or_proc = false;
					}

					func_ret_type_seen = true;
					trans_decln_spec(f->u.f1.d1);
					func_ret_type_seen = false;

					func_name_seen = true;
					trans_declarator(f->u.f1.d2);
					func_name_seen = false;

					semantic_analysis[ EFunction_def ](
							action,
							f->u.f1.d1->d_type,
							f->u.f1.d2->d_type,
							f);

					trans_block_stmt(f->u.f1.b);
					break;
				}
	}
}

static void trans_function_definition(void *trans_node)
{
	HIT();

	struct function_definition *f = trans_node;
	switch ( f->type ) {
	case EFunction1:	{
					code_generation[ EFunction_def ](f);
					break;
				}
	default:		{
					UNKNOWN_TYPE(f->type);
					break;
				}
	}
}

static void procedure_defintion_semantic_analysis(int action,
						struct unified_type *t1,
						struct unified_type *t2,
					struct procedure_definition *p)
{
	TOUCH();

	switch ( action ) {
	case EProcedure1:	{
					func_parameter_list = strdup("void");
					
					struct func_type *pt =
							malloc(sizeof(*pt));
					if ( pt ) {
						pt->name = func_name;
						pt->ret_type = func_ret_type;
						pt->parameter_list =
							func_parameter_list;

						add_func_type(pt, p->src_line);

						struct unified_type *u
							= malloc(sizeof(*u));
						if ( u ) {
							u->type = EFunc;
							u->u.f = pt;
							p->d_type = u;
						}
					}

					func_name = NULL;
					func_ret_type = NULL;
					func_parameter_list = NULL;

					break;
				}
	}
}

static void procedure_definition_code_generation(void *trans_node)
{
	TOUCH();

	struct procedure_definition *p = trans_node;
	int action = p->type;
	switch ( action ) {
	case EProcedure1: 	{
					if ( first_func_or_proc ) {
						dump_dynamic_header();
						first_func_or_proc = false;
					}

					func_name = p->u.p1.i->u.i1.i;
					main_func_begun =
					 !strcmp(func_name, "main");

					if ( !main_func_begun ) {
						fputs("\n\nvoid ", op);
					} else {
						main_present = true;
						fputs("\n\nint ", op);
					}			

					fputs(func_name, op);

					fputs("(void) ", op);

					if ( !strcmp(func_name, "main") ) {
						register_crash_handler = true;
					}

					trans_block_stmt(p->u.p1.b);
					p->d_type = p->u.p1.b->d_type;

					if ( main_func_begun ) {
						func_ret_type = strdup("int");
						main_func_begun = false;
					} else {
						func_ret_type = strdup("void");
					}
					
					semantic_analysis[ EProcedure_def ](
							action,
							p->u.p1.i->d_type,
							p->u.p1.b->d_type,
							p);
					break;
				}
	}
}

static void trans_procedure_definition(void *trans_node)
{
	HIT();

	struct procedure_definition *p = trans_node;
	switch ( p->type ) {
	case EProcedure1:	{
					code_generation[ EProcedure_def ](p);
					break;
				}
	default:		{
					UNKNOWN_TYPE(p->type);
					break;
				}
	}
}


void (*translator[]) (void *trans_node)  = {
	trans_lines, trans_identifier, trans_constant,
	trans_visibility_const, trans_primary, trans_postfix,
	trans_arg_exp_list, trans_unary_expression,
	trans_unary_operator, trans_multiplicative_expression,
	trans_additive_expression, trans_relational_expression,
	trans_equality_expression, trans_logical_and_exp,
	trans_logical_or_exp, trans_logical_xor_exp,
	trans_constant_expression, trans_assignment_expression,
	trans_expression, trans_declaration, trans_decln_spec,
	trans_init_declr_list, trans_init_declr, trans_storage_spec,
	trans_type_spec, trans_game_elem, trans_attr_spec, trans_attr_val_list,
	trans_attr_const, trans_record_spec, trans_record_decln_list,
	trans_record_decln, trans_spec_qual_list,
	trans_record_declr_list, trans_record_declr, trans_catalog_spec,
	trans_catalog_list, trans_catalog_const, trans_type_qual,
	trans_declarator, trans_parameter_type_list,
	trans_parameter_list, trans_parameter_decln,
	trans_identifier_list, trans_initializer,
	trans_initializer_list, trans_statement, trans_label_stmt,
	trans_expression_stmt, trans_block_stmt,
	trans_decl_list_or_stmt_list, trans_selection_stmt,
	trans_iteration_stmt, trans_jump_stmt, trans_attribute_addition_spec,
	trans_translation_unit, trans_elements, trans_function_definition,
	trans_procedure_definition,
};

void (*semantic_analysis[]) () = { // Treated as variable number of arguments.
				// Because each function vary in their fixed
				// number of arguments but don't use
				// variable-arg-list technique on them.
	lines_semantic_analysis, identifier_semantic_analysis,
	constant_semantic_analysis, visibility_const_semantic_analysis,
	primary_semantic_analysis, postfix_semantic_analysis,
	arg_exp_list_semantic_analysis, unary_expression_semantic_analysis,
	unary_operator_semantic_analysis,
	multiplicative_expression_semantic_analysis,
	additive_expression_semantic_analysis,
	relational_expression_semantic_analysis,
	equality_expression_semantic_analysis,
	logical_and_exp_semantic_analysis, logical_or_exp_semantic_analysis,
	logical_xor_exp_semantic_analysis,
	constant_expression_semantic_analysis,
	assignment_expression_semantic_analysis, expression_semantic_analysis,
	declaration_semantic_analysis, decln_spec_semantic_analysis,
	init_declr_list_semantic_analysis, init_declr_semantic_analysis,
	storage_spec_semantic_analysis, type_spec_semantic_analysis,
	game_elem_semantic_analysis, attr_spec_semantic_analysis,
	attr_val_list_semantic_analysis, attr_const_semantic_analysis,
	record_spec_semantic_analysis, record_decln_list_semantic_analysis,
	record_decln_semantic_analysis, spec_qual_list_semantic_analysis,
	record_declr_list_semantic_analysis, record_declr_semantic_analysis,
	catalog_spec_semantic_analysis, catalog_list_semantic_analysis,
	catalog_const_semantic_analysis, type_qual_semantic_analysis,
	declarator_semantic_analysis, parameter_type_list_semantic_analysis,
	parameter_list_semantic_analysis, parameter_decln_semantic_analysis,
	identifier_list_semantic_analysis, initializer_semantic_analysis,
	initializer_list_semantic_analysis, statement_semantic_analysis,
	label_stmt_semantic_analysis, expression_stmt_semantic_analysis,
	block_stmt_semantic_analysis, decl_list_or_stmt_list_semantic_analysis,
	selection_stmt_semantic_analysis, iteration_stmt_semantic_analysis,
	jump_stmt_semantic_analysis, attribute_addition_spec_semantic_analysis,
	translation_unit_semantic_analysis, elements_semantic_analysis,
	function_definition_semantic_analysis,
	procedure_defintion_semantic_analysis,
};

void (*code_generation[]) (void *trans_node) = {
	lines_code_generation, identifier_code_generation,
	constant_code_generation, visibility_const_code_generation,
	primary_code_generation, postfix_code_generation,
	arg_exp_list_code_generation, unary_expression_code_generation,
	unary_operator_code_generation,
	multiplicative_expression_code_generation,
	additive_expression_code_generation,
	relational_expression_code_generation,
	equality_expression_code_generation, logical_and_exp_code_generation,
	logical_or_exp_code_generation, logical_xor_exp_code_generation,
	constant_expression_code_generation,
	assignment_expression_code_generation, expression_code_generation,
	declaration_code_generation, decln_spec_code_generation,
	init_declr_list_code_generation, init_declr_code_generation,
	storage_spec_code_generation, type_spec_code_generation,
	game_elem_code_generation, attr_spec_code_generation,
	attr_val_list_code_generation, attr_const_code_generation,
	record_spec_code_generation, record_decln_list_code_generation,
	record_decln_code_generation, spec_qual_list_code_generation,
	record_declr_list_code_generation, record_declr_code_generation,
	catalog_spec_code_generation, catalog_list_code_generation,
	catalog_const_code_generation, type_qual_code_generation,
	declarator_code_generation, parameter_type_list_code_generation,
	parameter_list_code_generation, parameter_decln_code_generation,
	identifier_list_code_generation, initializer_code_generation,
	initializer_list_code_generation, statement_code_generation,
	label_stmt_code_generation, expression_stmt_code_generation,
	block_stmt_code_generation, decl_list_or_stmt_list_code_generation,
	selection_stmt_code_generation, iteration_stmt_code_generation,
	jump_stmt_code_generation, attribute_addition_spec_code_generation,
	translation_unit_code_generation, elements_code_generation,
	function_definition_code_generation,
	procedure_definition_code_generation,
};

const size_t max_node_type = sizeof(translator) / sizeof(translator[ 0 ]);

