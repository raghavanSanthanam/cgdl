%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symbol_table.h"
#include "ast.h"
#include "semantic_actions.h"

extern bool main_present;

int yyerror(const char *s);
void display(char *s, int line);

extern unsigned long long int src_line_num;

extern char *yytext;

int yylex(void);

extern char *enum_player_attr;

extern char *strdup(const char *s);
extern void cleanup_and_exit(void);

extern char *string_literal;

#define YYSTYPE void *

extern int fileno(FILE *stream);

extern FILE *yyin;
extern FILE *ip;

extern bool Eof_already_seen;
extern bool Eof_already_seen_and_processed;

extern struct cgdl_node_list *cgdl_node_list_head;
%}

%token ID STRING_LITERAL
%token LE_OP GE_OP EQ_OP NE_OP 
%token AND_OP OR_OP XOR_OP
%token GLOBAL
%token BOOLEAN NONE NUMBER STRING RANK CARD PLAYER PILE CONST SUIT SET
%token SETUP ROUND
%token CATALOG RECORD ATTRIBUTE VISIBILITY
%token NUMBER_CONSTANT BOOLEAN_CONSTANT NULL_CONSTANT
%token FOREACH IN REPEAT UNTIL LOOP LOOPBACK BREAK SWITCH CASE DEFAULT
       FALLTHROUGH IF ELSE RETURN
%token UMINUS ALL NOONE SELF
%token PROC

%start lines


%%

lines
        : lines translation_unit '\n' {
        	$$ = semantic_actions[ ELines ](ELine1, $1, $2);
        }
        | lines '\n' {
		$$ = semantic_actions[ ELines ](ELine2, $1);
	}
        | /*empty*/
	;

identifier
	: ID {
		$$ = semantic_actions[ EIdentifier ](EIdentifier1, yytext);
	} 
       
constant
        : NUMBER_CONSTANT {
        	$$ = semantic_actions[ EConstant ](EConst1, yytext);        	
	}
	| STRING_LITERAL {
		$$ = semantic_actions[ EConstant ](EConst2, string_literal);	
	}
        | visibility_constant {
		$$ = semantic_actions[ EConstant ](EConst3, $1);
	}
	| BOOLEAN_CONSTANT {
		$$ = semantic_actions[ EConstant ](EConst4, yytext);
	}
	| NULL_CONSTANT {
		$$ = semantic_actions[ EConstant ](EConst5);	
	}
	;

visibility_constant
        : ALL {
		$$ = semantic_actions[ EVisibility ](EVisibility1);        
	}
        | NOONE {
		$$ = semantic_actions[ EVisibility ](EVisibility2);
	}
        | SELF {
		$$ = semantic_actions[ EVisibility ](EVisibility3);
	}
        ;

primary_expression
	: identifier {
		$$ = semantic_actions[ EPrimary ](EPrimary1, $1);			
	 }
	| constant { 
		$$ = semantic_actions[ EPrimary ](EPrimary2, $1);
	}
        | '|' expression '|' {
		$$ = semantic_actions[ EPrimary ](EPrimary3, $2);        
	}
        ;

postfix_expression
        : primary_expression {
		$$ = semantic_actions[ EPostfix ](EPostfix1, $1);        
	}
        | postfix_expression '[' expression ']' {
		$$ = semantic_actions[ EPostfix ](EPostfix2, $1, $3);
	}
        | postfix_expression '[' expression ':' expression ']' {
		$$ = semantic_actions[ EPostfix ](EPostfix3, $1, $3, $5);
	}
	| postfix_expression '(' ')' { 
		$$ = semantic_actions[ EPostfix ](EPostfix4, $1);
	}
        | postfix_expression '(' argument_expression_list ')' {
		$$ = semantic_actions[ EPostfix ](EPostfix5, $1, $3);
	}
        | postfix_expression '.' identifier { 
		$$ = semantic_actions[ EPostfix ](EPostfix6, $1, $3);
	}
        ;

argument_expression_list
        : assignment_expression { 
		$$ = semantic_actions[ EArg_exp_list ](EArg1, $1);        
	}
        | argument_expression_list ',' assignment_expression {
		$$ = semantic_actions[ EArg_exp_list ](EArg2, $1, $3);
	}
        ;

unary_expression
	: postfix_expression {
		$$ = semantic_actions[ EUnary_exp ](EUnary1, $1);	
	}
        | unary_operator unary_expression {
		$$ = semantic_actions[ EUnary_exp ](EUnary2, $1, $2);
	}
        ;

unary_operator
        : '@' {
		$$ = semantic_actions[ EUnary_op ](EUnary_op1);        
	}
        | UMINUS {
		$$ = semantic_actions[ EUnary_op ](EUnary_op2);
	}
        | '!' {
		$$ = semantic_actions[ EUnary_op ](EUnary_op3);
	}
        ;

multiplicative_expression
        : unary_expression {
		$$ = semantic_actions[ EMultiplicative_exp ](EMul1, $1);        
	}		
        | multiplicative_expression '*' unary_expression {
		$$ = semantic_actions[ EMultiplicative_exp ](EMul2, $1, $3);
	}
        | multiplicative_expression '/' unary_expression {
		$$ = semantic_actions[ EMultiplicative_exp ](EMul3, $1, $3);
	}
        | multiplicative_expression '%' unary_expression {
		$$ = semantic_actions[ EMultiplicative_exp ](EMul4, $1, $3);
	}
        ;

additive_expression
        : multiplicative_expression {
		$$ = semantic_actions[ EAdditive_exp ](EAdd1, $1);        
	 }
        | additive_expression '+' multiplicative_expression { 
		$$ = semantic_actions[ EAdditive_exp ](EAdd2, $1, $3);
	}
	| additive_expression '-' multiplicative_expression {
		$$ = semantic_actions[ EAdditive_exp ](EAdd3, $1, $3);
	}
        ;

relational_expression
        : additive_expression {
		$$ = semantic_actions[ ERelational_exp ](ERel1, $1);        
	}
        | relational_expression '<' additive_expression {
		$$ = semantic_actions[ ERelational_exp ](ERel2, $1, $3);
	}
        | relational_expression '>' additive_expression {
		$$ = semantic_actions[ ERelational_exp ](ERel3, $1, $3);        
	}		
        | relational_expression LE_OP additive_expression {
		$$ = semantic_actions[ ERelational_exp ](ERel4, $1, $3);
	}
        | relational_expression GE_OP additive_expression {
		$$ = semantic_actions[ ERelational_exp ](ERel5, $1, $3);
	}
        ;

equality_expression
        : relational_expression {
		$$ = semantic_actions[ EEquality_exp ](EEqual1, $1);        
	}
        | equality_expression EQ_OP relational_expression {
		$$ = semantic_actions[ EEquality_exp ](EEqual2, $1, $3);
	}        
        | equality_expression NE_OP relational_expression {
		$$ = semantic_actions[ EEquality_exp ](EEqual3, $1, $3);
	}        
        ;

logical_and_expression
        : equality_expression { 
		$$ = semantic_actions[ ELogical_and_exp ](ELand1, $1);        
	}
        | logical_and_expression AND_OP equality_expression {
		$$ = semantic_actions[ ELogical_and_exp ](ELand2, $1, $3);
	}             
        ;

logical_or_expression
        : logical_and_expression {
		$$ = semantic_actions[ ELogical_or_exp ](ELor1, $1);        
	}
        | logical_or_expression OR_OP logical_and_expression {
		$$ = semantic_actions[ ELogical_or_exp ](ELor2, $1, $3);
	}
        ;

logical_xor_expression
	    : logical_or_expression {
		$$ = semantic_actions[ ELogical_xor_exp ](ELxor1, $1);	    
	}
        | logical_xor_expression XOR_OP logical_or_expression {
		$$ = semantic_actions[ ELogical_xor_exp ](ELxor2, $1, $3);
	}
        ;

assignment_expression
        : logical_xor_expression { 
		$$ = semantic_actions[ EAssign_exp ](EAssign1, $1);        
	}
        | unary_expression '=' assignment_expression { 
		$$ = semantic_actions[ EAssign_exp ](EAssign2, $1, $3);
	}               
        ;

expression
        : assignment_expression {
		$$ = semantic_actions[ EExp ](EExp1, $1);        
	}
	| expression ',' assignment_expression {
		$$ = semantic_actions[ EExp ](EExp2, $1, $3);
	}
        ;

constant_expression
        : logical_xor_expression {
		$$ = semantic_actions[ EConst_exp ](EConst_exp1, $1);        
	}
	;

declaration
        : declaration_specifiers ';' { 
		$$ = semantic_actions[ EDeclaration ](EDeclaration1, $1);        
	}
        | declaration_specifiers init_declarator_list ';' {
		$$ = semantic_actions[ EDeclaration ](EDeclaration2, $1, $2);
	}      
        ;

declaration_specifiers
        : storage_class_specifier {
		$$ = semantic_actions[ EDeclaration_spec ](EDecln_spec1, $1);        
	}
        | storage_class_specifier declaration_specifiers {
		$$ = semantic_actions[ EDeclaration_spec ]
						(EDecln_spec2, $1, $2);
	}
        | type_specifier { 
		$$ = semantic_actions[ EDeclaration_spec ](EDecln_spec3, $1);
	}                                   
     	| type_specifier '[' ']' {
		$$ = semantic_actions[ EDeclaration_spec ](EDecln_spec4, $1);
	}
	| type_specifier '[' expression ']' {  
		$$ = semantic_actions[ EDeclaration_spec ]
						(EDecln_spec5, $1, $3);
	} 
       	| type_specifier declaration_specifiers {
		$$ = semantic_actions[ EDeclaration_spec ]
						(EDecln_spec6, $1, $2);
	}
      	| type_qualifier { 
		$$ = semantic_actions[ EDeclaration_spec ](EDecln_spec7, $1);
	}
	| type_qualifier declaration_specifiers {
		$$ = semantic_actions[ EDeclaration_spec ]
						(EDecln_spec8, $1, $2);
	}       
        ;

init_declarator_list
        : init_declarator { 
		$$ = semantic_actions[ EInit_declr_list ]
						(EInit_declr_list1, $1);        
	}
        | init_declarator_list ',' init_declarator {	
		$$ = semantic_actions[ EInit_declr_list ]
						(EInit_declr_list2, $1, $3);
	}
        ;               

init_declarator
        : declarator {
		$$ = semantic_actions[ EInit_declr ](EInit_declr1, $1);        
	}
        | declarator '=' initializer {
		$$ = semantic_actions[ EInit_declr ](EInit_declr2, $1, $3);
	}                           
        ;

storage_class_specifier
        : GLOBAL {
		$$ = semantic_actions[ EStorage_spec ](EStorage1);        
	}   
        ;

type_specifier
	: NONE {
		$$ = semantic_actions[ EType_spec ](EType1);
        }
	| STRING {
		$$ = semantic_actions[ EType_spec ](EType2);
	}
        | BOOLEAN {
		$$ = semantic_actions[ EType_spec ](EType3);
	}
        | NUMBER {
		$$ = semantic_actions[ EType_spec ](EType4);
	}
        | CARD {
		$$ = semantic_actions[ EType_spec ](EType5);
	}
        | PLAYER {
		$$ = semantic_actions[ EType_spec ](EType6);
	}
	| PILE {
		$$ = semantic_actions[ EType_spec ](EType7);
	}
	| VISIBILITY {
		$$ = semantic_actions[ EType_spec ](EType8);
	}
	| attribute_specifier {
		$$ = semantic_actions[ EType_spec ](EType9, $1);
	}
	| record_specifier {
		$$ = semantic_actions[ EType_spec ](EType10, $1);
	}
        | catalog_specifier {
		$$ = semantic_actions[ EType_spec ](EType11, $1);
	}
	| SET {
		$$ = semantic_actions[ EType_spec ](EType12);
	}		
        ;

game_element
	: SETUP {
		$$ = semantic_actions[ EGame_elem ](EGame1);	
	}
	| ROUND {
		$$ = semantic_actions[ EGame_elem ](EGame2);
	}
	;

attribute_specifier
	: ATTRIBUTE identifier '{' attribute_value_list '}' {
		$$ = semantic_actions[ EAttr_spec ](EAttr1, $2, $4);	
	}
	;

attribute_value_list
	: attribute_constant {
		$$ = semantic_actions[ EAttr_val_list ](EAttr_val_list1, $1);	
	}
	| attribute_value_list ',' attribute_constant { 
		$$ = semantic_actions[ EAttr_val_list ]
						(EAttr_val_list2, $1, $3);
	} 
	;

attribute_constant
	: constant_expression {
		$$ = semantic_actions[ EAttr_const ](EAttr_const1, $1);
	} 
	;

record_specifier
        : RECORD identifier '{' record_declaration_list '}' { 
		$$ = semantic_actions[ ERecord_spec ](ERecord_spec1, $2, $4);        
	}
	| RECORD identifier { 
		$$ = semantic_actions[ ERecord_spec ](ERecord_spec2, $2);
	}
        ;

record_declaration_list
        : record_declaration { 
		$$ = semantic_actions[ ERecord_decln_list ]
						(ERecord_decln_list1, $1);        
	}                                      
        | record_declaration_list record_declaration { 
		$$ = semantic_actions[ ERecord_decln_list ]
						(ERecord_decln_list2, $1, $2);
	}
        ;

record_declaration
        : specifier_qualifier_list record_declarator_list ';' {
		$$ = semantic_actions[ ERecord_decln ](ERecord1, $1, $2);        
	}             
        ;

specifier_qualifier_list
        : type_specifier specifier_qualifier_list { 
		$$ = semantic_actions[ ESpec_qual_list ]
						(ESpec_qual_list1, $1, $2);        
	}                               
        | type_specifier { 
		$$ = semantic_actions[ ESpec_qual_list ]
						(ESpec_qual_list2, $1);
	}                          
        | type_qualifier specifier_qualifier_list { 
		$$ = semantic_actions[ ESpec_qual_list ]
						(ESpec_qual_list3, $1, $2);
	}                                 
        | type_qualifier { 
		$$ = semantic_actions[ ESpec_qual_list ](ESpec_qual_list4, $1);
	}                          
        ;

record_declarator_list
        : record_declarator { 
		$$ = semantic_actions[ ERecord_declr_list ]
						(ERecord_declr_list1, $1);        
	}                                  
        | record_declarator_list ',' record_declarator {  
		$$ = semantic_actions[ ERecord_declr_list ]
						(ERecord_declr_list2, $1, $3);
	}                                       
        ;

record_declarator
        : declarator {
		$$ = semantic_actions[ ERecord_declr ](ERecord_declr1, $1);
	}
        ;

catalog_specifier
        : CATALOG '{' catalog_list '}' { 
		$$ = semantic_actions[ ECatalog_spec ](ECatalog_spec1, $3);        
	}
        | CATALOG identifier '{' catalog_list '}' { 
		$$ = semantic_actions[ ECatalog_spec ](ECatalog_spec2, $2, $4);
	}
	| CATALOG identifier {  
		$$ = semantic_actions[ ECatalog_spec ](ECatalog_spec3, $2);
	}
        ; 

catalog_list
        : catalog_constant {
		$$ = semantic_actions[ ECatalog_list ](ECatalog_list1, $1);        
	}    
        | catalog_list ',' catalog_constant {
		$$ = semantic_actions[ ECatalog_list ](ECatalog_list2, $1, $3);
	}
        ;

catalog_constant
        : identifier {
		$$ = semantic_actions[ ECatalog_const ](ECatalog_const1, $1);        
	}
        | identifier '=' constant_expression {
		$$ = semantic_actions[ ECatalog_const ]
						(ECatalog_const2, $1, $3);
 	}  
        ;

type_qualifier
        : CONST { 
		$$ = semantic_actions[ EType_qual ](EType_qual1);        
	}
        ;

declarator
        : identifier {  
		$$ = semantic_actions[ EDeclarator ](EDecl1, $1);        
	}
       	| '(' declarator ')' {
		$$ = semantic_actions[ EDeclarator ](EDecl2, $2);
	}
        | declarator '(' parameter_type_list ')' {
		$$ = semantic_actions[ EDeclarator ](EDecl3, $1, $3);
	}
        | declarator '(' identifier_list ')' {
		$$ = semantic_actions[ EDeclarator ](EDecl4, $1, $3);
	}
        | declarator '(' ')' {
		$$ = semantic_actions[ EDeclarator ](EDecl5, $1);
	}
        ;

parameter_type_list
        : parameter_list { 
		$$ = semantic_actions[ EParameter_type_list ]
						(EParameter_type_list1, $1);        
	}
        ;

parameter_list
        : parameter_declaration { 
		$$ = semantic_actions[ EParameter_list ](EParameter_list1, $1);
	}
        | parameter_list ',' parameter_declaration {
		$$ = semantic_actions[ EParameter_list ]
						(EParameter_list2, $1, $3);
 	}
        ;


parameter_declaration
        : declaration_specifiers declarator { 
		$$ = semantic_actions[ EParameter_decln ]
						(EParameter_decln1, $1, $2);
	}
	| declaration_specifiers {
		$$ = semantic_actions[ EParameter_decln ]
						(EParameter_decln2, $1);
	}
        ;

identifier_list
        : identifier {
		$$ = semantic_actions[ EIdentifier_list ]
						(EIdentifier_list1, $1);        
	}    
       	| identifier_list ',' identifier { 
		$$ = semantic_actions[ EIdentifier_list ]
						(EIdentifier_list2, $1, $3);
	}
        ;

initializer
        : assignment_expression {
		$$ = semantic_actions[ EInitializer ](EInitializer1, $1);
	}
       	| '{' initializer_list '}' { 
		$$ = semantic_actions[ EInitializer ](EInitializer2, $2);
	}
        ;

initializer_list
        : initializer {
		$$ = semantic_actions[ EInitializer_list ]
						(EInitializer_list1, $1);        
	}
        | initializer_list ',' initializer { 
		$$ = semantic_actions[ EInitializer_list ]
						(EInitializer_list2, $1, $3);
	}  
        ;

statement
        : expression_statement { 
		$$ = semantic_actions[ EStatement ](EStatement1, $1);
	}
        | block_statement { 
		$$ = semantic_actions[ EStatement ](EStatement2, $1);
	}
        | selection_statement {
		$$ = semantic_actions[ EStatement ](EStatement3, $1);
	}
       	| labeled_statement {
		$$ = semantic_actions[ EStatement ](EStatement4, $1);
	}
       	| iteration_statement {
		$$ = semantic_actions[ EStatement ](EStatement5, $1);
	}
        | jump_statement {
		$$ = semantic_actions[ EStatement ](EStatement6, $1);
	}
        ;

labeled_statement
        : CASE constant_expression ':' statement { 
		$$ = semantic_actions[ ELabel_stmt ](ELabel1, $2, $4);        
	}
        | DEFAULT ':' statement {
		$$ = semantic_actions[ ELabel_stmt ](ELabel2, $3);
	}
        ;

expression_statement
        : ';' { 
		$$ = semantic_actions[ EExp_stmt ](EExp_stmt1);        
	}    
        | expression ';' { 
		$$ = semantic_actions[ EExp_stmt ](EExp_stmt2, $1);
	}	    
        ;

block_statement
	: '{' declaration_list_or_statement_list '}' {
		$$ = semantic_actions[ EBlock_stmt ](EBlk1, $2);	
	} 
	| game_element '{' declaration_list_or_statement_list '}' {
		$$ = semantic_actions[ EBlock_stmt ](EBlk2, $1, $3);
	}
       	| '{' '}' {
		$$ = semantic_actions[ EBlock_stmt ](EBlk3);
	}
        | game_element '{' '}' {
		$$ = semantic_actions[ EBlock_stmt ](EBlk4, $1);
	}
        ;

declaration_list_or_statement_list
        : declaration { 
		$$ = semantic_actions[ EDecl_list_stmt_list ]
					(EDecl_list_stmt_list1, $1);        
	}
        | declaration_list_or_statement_list declaration {
		$$ = semantic_actions[ EDecl_list_stmt_list ]
					(EDecl_list_stmt_list2, $1, $2);
	}	
        | statement { 
		$$ = semantic_actions[ EDecl_list_stmt_list ]
					(EDecl_list_stmt_list3, $1);
	}
        | declaration_list_or_statement_list statement {
		$$ = semantic_actions[ EDecl_list_stmt_list ]
					(EDecl_list_stmt_list4, $1, $2);
	}
        ;

selection_statement
        : IF '(' expression ')' statement {
		$$ = semantic_actions[ ESelection_stmt ](ESelection1, $3, $5);        
	}
       	| IF '(' expression ')' statement ELSE statement {
		$$ = semantic_actions[ ESelection_stmt ]
						(ESelection2, $3, $5, $7);
	}
        | SWITCH '(' expression ')' statement {
		$$ = semantic_actions[ ESelection_stmt ](ESelection3, $3, $5);
	} 
        ;


iteration_statement
        : REPEAT statement UNTIL '(' expression ')' ';' {
		$$ = semantic_actions[ EIteration_stmt ](EIteration1, $2, $5);        
	}
        | FOREACH identifier statement { 
		$$ = semantic_actions[ EIteration_stmt ](EIteration2, $2, $3);
	}
        | FOREACH identifier IN identifier statement { 
		$$ = semantic_actions[ EIteration_stmt ]
					(EIteration3, $2, $4, $5);
	}
	| LOOP identifier IN constant_expression statement { 
		$$ = semantic_actions[ EIteration_stmt ]
					(EIteration4, $2, $4, $5);
	}
        | LOOP constant_expression statement { 
		$$ = semantic_actions[ EIteration_stmt ](EIteration5, $2, $3);
	}
        ;

jump_statement
        : LOOPBACK ';' {
		$$ = semantic_actions[ EJump_stmt ](EJump1);        
	}
        | FALLTHROUGH ';' {
		$$ = semantic_actions[ EJump_stmt ](EJump2);
	}
        | BREAK ';' {
		$$ = semantic_actions[ EJump_stmt ](EJump3);
	}
        | RETURN expression ';' {
		$$ = semantic_actions[ EJump_stmt ](EJump4, $2);
	}
        | RETURN ';' {
		$$ = semantic_actions[ EJump_stmt ](EJump5);
	}
        ;

attribute_addition_specifier
	: identifier type_specifier ':' identifier {
		$$ = semantic_actions[ EAttr_add_spec ](EAttr_add1, $1, $2, $4);	
	}
	| identifier identifier {
		$$ = semantic_actions[ EAttr_add_spec ](EAttr_add2, $1, $2);
	}
	;
        
translation_unit
        : elements {
		$$ = semantic_actions[ ETranslation_unit ](ETranslation1, $1);        
	}
        | translation_unit elements {
		$$ = semantic_actions[ ETranslation_unit ]
						(ETranslation2, $1, $2);
	}
        ;

elements
        : function_definition {
		$$ = semantic_actions[ EElements ](EElements1, $1);
	}    
        | procedure_definition {
		$$ = semantic_actions[ EElements ](EElements2, $1);
	}
        | declaration {
		$$ = semantic_actions[ EElements ](EElements3, $1);
	}
	| attribute_addition_specifier {
		$$ = semantic_actions[ EElements ](EElements4, $1);
	}
        ;

function_definition
        : declaration_specifiers declarator block_statement {
		$$ = semantic_actions[ EFunction_def ](EFunction1, $1, $2, $3);        
	}
        ;  

procedure_definition
        : identifier block_statement {
		$$ = semantic_actions[ EProcedure_def ](EProcedure1, $1, $2);        
	} 
        ;   

%%

#include "lex.yy.c"

int yyerror(const char *s)
{
	struct term_or_nonterm dummy = { 0 };
	dummy.src_line = src_line_num;
	void *trans_node = &dummy;

	COMPILATION_ERROR("Syntax error. "
			"Please look at this symbol/text : %s\n", yytext);

	return 0; // Keep the compiler happy!
}


int yywrap(void)
{
	printf("In %s!!! Exiting!!\n", __func__);

	if ( !main_present ) {
		struct term_or_nonterm dummy = { 0 };
		void *trans_node = &dummy;
		COMPILATION_ERROR
		("Procedure main must be defined\n");
	}

	combine_intermediate_files_to_form_src_and_exit(0); 

	return 0;
}

void display(char *s, int line)
{
        printf("%s : %d\n", s, line);
}

