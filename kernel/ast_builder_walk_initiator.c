#include <stdio.h>
#include <stdlib.h>

#define __DEBUG__

#ifdef __DEBUG__
#include <signal.h>
#endif

#include "ast.h"

#define __DEBUG__

#ifdef __DEBUG__
extern char *enum_player_attr;
#define TOUCH()	do {\
			printf("%s()\n", __func__);\
		} while ( 0 )
#else
#define TOUCH()
#endif

extern FILE *op;

extern void combine_intermediate_files_to_form_src_and_exit(int exit_code);

extern void (*translator[]) (void *trans_node);

extern char *indentation;
extern long long int num_of_empty_lines;
extern const size_t max_node_type;
extern unsigned long long int src_line_num;
extern char *src_file;
extern unsigned long long int cur_src_line;

struct cgdl_node_list *cgdl_node_list_head = NULL;
static struct cgdl_node_list *tail = NULL;

#ifdef __DEBUG__
extern const char *translator_func[];
#endif

#ifdef __DEBUG__
static void print_cgdl_nodes_list(void)
{
	TOUCH();

	struct cgdl_node_list *t = cgdl_node_list_head;
	int n = 0;
	while ( t ) {
		printf("[ %d ] : node_addr : %p type : %s internal type : %d\n",
			n++, t->n->cg.handle, translator_func[ t->n->type ],
						t->n->cg.term_or_nonterm->type);
		t = t->next;
//		getchar();
	}
}
#endif

// Returns true on success and false otherwise. And in case of
// failure, frees the given node fully by itself.
bool add_node(struct cgdl_node *n)
{
	TOUCH();

	if ( n ) {
		struct cgdl_node_list *c = malloc(sizeof(*c));
		if ( c ) {
			if ( indentation ) {
				n->cg.term_or_nonterm->indentation =
								indentation;
				indentation = NULL;
			} else {
				n->cg.term_or_nonterm->indentation = NULL;
			}
			if ( num_of_empty_lines > 1 ) {
				n->cg.term_or_nonterm->num_of_empty_lines =
							num_of_empty_lines - 1;
			} else {
				n->cg.term_or_nonterm->num_of_empty_lines = 0;
			}

			n->cg.term_or_nonterm->src_line = src_line_num;
			n->cg.term_or_nonterm->d_type = NULL;

			printf("num_of_empty_lines : %llu\n",
							num_of_empty_lines);
			printf("n->cg.term_or_nonterm->num_of_empty_lines "
				": %llu\n",
				n->cg.term_or_nonterm->num_of_empty_lines);

			if ( n->type == ETranslation_unit ) {
				printf("TRANSLATION-UNIT : "
					"n->cg.term_or_nonterm->type : %d\n",
						n->cg.term_or_nonterm->type);
			}

			num_of_empty_lines = 0;
			c->n = n;
			c->next = NULL;

#ifdef __DEBUG__
			static unsigned long long int num_nodes = 0;
			if ( !tail ) {
				num_nodes = 0;
			}
			num_nodes++;
			n->cg.term_or_nonterm->node_number = num_nodes;
			printf("Node(%llu) inserted of addr:%p and type: %s\n",
					num_nodes, (void *)c->n,
						translator_func[ c->n->type ]);
			printf("And of internal type: %d\n",
						c->n->cg.term_or_nonterm->type);
#endif
			if ( !cgdl_node_list_head ) {
				cgdl_node_list_head = tail = c;
			} else {
				tail->next = c;
				tail = c;
			}

		} else {
			free(n->cg.handle);
			n->cg.handle = NULL;
			free(n);
			n = NULL;
			return false;
		}
	}

	return true;
}

static inline void free_symbol(struct symbol **s)
{
	TOUCH();

	if ( s && *s ) {
		free((*s)->t);
		(*s)->t = NULL;
	}
}

static inline void free_comp_type_member(struct comp_type_member **m);
static inline void free_comp_type(struct comp_type **c)
{
	TOUCH();

	if ( c && *c ) {
		free((*c)->tag);
		(*c)->tag = NULL;
		free((*c)->inst);
		(*c)->inst = NULL;
		free_comp_type_member(&(*c)->m);
		(*c)->m = NULL;
	}	
}

static inline void free_comp_type_member(struct comp_type_member **m)
{
	TOUCH();

	if ( m && *m ) {
		while ( *m ) {
			if ( (*m)->type == ESymbol ) {
				free_symbol(&(*m)->f.s);
				(*m)->f.s = NULL;
			} else if ( (*m)->type == EComps ) {
				free_comp_type(&(*m)->f.c);
				(*m)->f.c = NULL;
			}
			struct comp_type_member *m1 = *m;
			*m = (*m)->next;
			free(m1);
		}
	}	
}

struct freed_addresses {
	void *a;
	struct freed_addresses *next;
};

static struct freed_addresses *f_head = NULL;
static struct freed_addresses *f_tail = NULL;

static bool freed(void *f)
{
	TOUCH();

	bool a_freed = false;

	struct freed_addresses *t = f_head;
	while ( t ) {
//		printf("%s : %p\n", __func__, (void *)t);
		if ( t->a == f ) {
			a_freed = true;
			break;
		}
		t = t->next;
	}

	return a_freed;
}

static void free_list_of_freed_addresses(void)
{
	TOUCH();

	while ( f_head ) {
		struct freed_addresses *t = f_head;
		t = t->next;
//		printf("f_head : %p\n", (void *)f_head);
		free(f_head);
		f_head = t;
	}
	f_tail = f_head;
}


static void add_to_freed_addresses_list(void *a)
{
	TOUCH();

	struct freed_addresses *f = malloc(sizeof(*f));
	if ( f ) {
		f->a = a;
		f->next = NULL;
		if ( f_tail ) {
			f_tail->next = f;
		} else {
			f_head = f;
		}
		f_tail = f;
//		printf("%s : %p\n", __func__, a);
	}
}

void free_nodes(void)
{
	TOUCH();

	struct cgdl_node_list *t = cgdl_node_list_head;

	// Free inside-out!
	while ( t ) {
		if ( t->n->cg.term_or_nonterm->type == ELines ) {
			break; // Let's stop here! It's okay for time being! :P
		}

		if ( t->n->cg.term_or_nonterm->type < 0 ||
			t->n->cg.term_or_nonterm->type > max_node_type ) {
			printf("Node out of the range : type - %d!\n",
					t->n->cg.term_or_nonterm->type);

			t->n->cg.term_or_nonterm->type = -1;

			if ( !freed(t->n->cg.handle) ) {
				free(t->n->cg.handle); // Free non-terminal
				add_to_freed_addresses_list(t->n->cg.handle);
				t->n->cg.handle = NULL;
			}

			free(t->n); // Free cgdl_node
			t->n = NULL;

			struct cgdl_node_list *s = t;
			t = t->next; // Know where to go next!
			s->next = NULL; // Destroy the link for double safety!
			free(s); // Free cgdl_node_list
			continue;
		}

#ifdef __DEBUG__
		printf("Freeing node number : %llu\n",
				t->n->cg.term_or_nonterm->node_number);
#endif

		// Propogated one!
		char **i = &t->n->cg.term_or_nonterm->indentation;
		if ( *i && !freed(*i) ) {
			free(*i); // Free the memory allocated in cgdl.l
			add_to_freed_addresses_list(*i);
			*i = NULL;
		}	

		printf("Freeing d_type(%p)",
				(void *)t->n->cg.term_or_nonterm->d_type);
		printf("t->n->cg.term_or_nonterm->type : %d\n",
					t->n->cg.term_or_nonterm->type);
		printf(" of the node of type : %s\n",
			translator_func[ t->n->cg.term_or_nonterm->type ]);
		struct unified_type **u = &t->n->cg.term_or_nonterm->d_type;

		// Due to the propogation nature of
		// d_type, we shouldn't free every
		// other d_type as they might have
		// the same addresses and hence you
		// free the same memory more than once! ;)
		//
		// Keep track of the nodes freed and
		// hence free if not freed already! \m/

		bool notLines = t->n->cg.term_or_nonterm->type != ELines;
		if  ( *u && !freed(*u) && notLines ) {
			if ( (*u)->type == EBasic ) {
				free_symbol(&(*u)->u.s);
				(*u)->u.s = NULL;
			} else if ( (*u)->type == EComp ) {
				if ( (*u)->u.c ) {
					free_comp_type_member(&(*u)->u.c->m);
					(*u)->u.c->m = NULL;
				}
			}
			(*u)->type = ENone1;

			free(*u);
			add_to_freed_addresses_list(*u);
			*u = NULL;
		}
		t->n->cg.term_or_nonterm->type = -1;

		if ( !freed(t->n->cg.handle) ) {
			free(t->n->cg.handle); // Free non-terminal
			add_to_freed_addresses_list(t->n->cg.handle);
			t->n->cg.handle = NULL;
		}

		free(t->n); // Free cgdl_node
		t->n = NULL;

		struct cgdl_node_list *s = t;
		t = t->next; // Know where to go next!
		s->next = NULL; // Destroy the link for double safety!
		free(s); // Free cgdl_node_list
	}	

	cgdl_node_list_head = tail = NULL;

	free_list_of_freed_addresses();
}

#ifdef __DEBUG__
static void handle_death(int sig);
#endif

void walk_ast(void)
{
	TOUCH();

#ifdef __DEBUG__
	signal(SIGSEGV, handle_death);
	signal(SIGINT, handle_death);
#endif

#ifdef __DEBUG__
	print_cgdl_nodes_list();
#endif

	// Pick the walking stick(last node inserted - root)
	//  and go on a walk!
	struct cgdl_node_list *t = tail;

	// Within range?
	if (	t && t->n->type >= 0 ) {
		if ( t->n->type < max_node_type ) {
			if ( translator[ t->n->type ] ) {
				printf("Walking started at node %p\n",
							t->n->cg.handle);
				translator[ t->n->type ](t->n->cg.handle);
				free_nodes(); // -- when to free?!
			}
		}
	}
}

#ifdef __DEBUG__
static void handle_death(int sig)
{
	TOUCH();

	if ( op ) {
		fclose(op);
		op = NULL;
	}

	fflush(NULL);
	// Not good to call printf(). But let the user
	// know we are catching these!
	if ( sig == SIGSEGV ) {
		printf("I was looking at %llu line in %s . . .!\n",
							cur_src_line, src_file);
		printf("SegFault - Something weird happended!\n");
#if defined CALLSTACK && __linux__
		printf("\nHere is what I was doing!\n\n");
		int num_last_calls = 1000;
		void *func_addresses[ num_last_calls ];
		num_last_calls = backtrace(func_addresses, num_last_calls);
		char **func_names = backtrace_symbols(func_addresses,
							num_last_calls);
		if ( func_names ) {
			int i = 0;
			while ( i < num_last_calls ) {
				printf("%s\n", func_names[ i ]);
				i++;
			}
			free(func_names);
			func_names = NULL;
		}
#endif		
	} else if ( sig == SIGINT ) {
		printf("You pressed CTRL+C!\n");
	}

	combine_intermediate_files_to_form_src_and_exit(0);
}
#endif

