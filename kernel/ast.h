#ifndef _AST_H
#define _AST_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

void *malloc_helper;

// Zero down the allocated chunk and then give the address to
// the needy.
#define malloc(size) ( (void)( ( (malloc_helper = malloc(size))	\
					&&			\
		  	memset(malloc_helper, 0, size) )	\
					|| 			\
			(malloc_helper = NULL) ), 		\
			malloc_helper )


#define __DEBUG__

enum comp_type_nesting { EMax_comp_type_nesting_limit = 63 };
enum max_nested_blocks { EMax_nested_blocks = 127 }; // Seems enough.

enum member_type { ESymbol, EComps, ENone1 };
struct comp_type;
struct comp_type_member {
	int type;
	union {
		struct comp_type *c;
		struct symbol *s;
	} f;
	struct comp_type_member *next;
};
struct comp_type {
	char *tag;
	struct comp_type_member *m;
	char *inst;
};

struct comp_type_list {
	struct comp_type *c;
	struct comp_type_list *next;
};

struct func_type {
	char *name;
	char *parameter_list; // Single string with types delimited by '|'
	char *ret_type;
};

struct func_type_list {
	struct func_type *f;
	struct func_type_list *next;
};

enum identifier_length { ESignificant_identifier_len = 63 };
struct symbol {
	char n[ ESignificant_identifier_len + 1 ];
	char *t;
};

struct symbol_list {
	struct symbol *s;
	struct symbol_list *next;
};

struct cgdl_node_list {
	struct cgdl_node *n;
	struct cgdl_node_list *next;
};

enum cgdl_node_types {
			ELines, EIdentifier, EConstant,
			EVisibility, EPrimary, EPostfix,
			EArg_exp_list, EUnary_exp, EUnary_op,
			EMultiplicative_exp, EAdditive_exp,
			ERelational_exp, EEquality_exp,
			ELogical_and_exp, ELogical_or_exp,
			ELogical_xor_exp, EConst_exp, EAssign_exp,
			EExp, EDeclaration,
			EDeclaration_spec, EInit_declr_list,
			EInit_declr, EStorage_spec, EType_spec,
			EGame_elem, EAttr_spec, EAttr_val_list,
			EAttr_const, ERecord_spec, ERecord_decln_list,
			ERecord_decln, ESpec_qual_list,
			ERecord_declr_list, ERecord_declr,
			ECatalog_spec, ECatalog_list, ECatalog_const,
			EType_qual, EDeclarator, EParameter_type_list,
			EParameter_list, EParameter_decln,
			EIdentifier_list, EInitializer,
			EInitializer_list, EStatement, ELabel_stmt,
			EExp_stmt, EBlock_stmt, EDecl_list_stmt_list,
			ESelection_stmt, EIteration_stmt, EJump_stmt,
			EAttr_add_spec, ETranslation_unit, EElements,
			EFunction_def, EProcedure_def, ENumber_of_non_terminals,
};

// Just for helping add_node() to access indentation-field
// of any terminal or non-terminal struct.
struct term_or_nonterm {
	int type;
	char *indentation;
	long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
};

struct cgdl_node {
	int type;
	union cgdl_grammar {
		struct lines *ln;
		struct identifier *id;
		struct constant *c;
		struct visibility_const *v;
		struct primary_expression *pr;
		struct postfix_expression *po;
		struct arg_exp_list *ar;
		struct unary_expression *un;
		struct unary_operator *uo;
		struct multiplicative_expression *me;
		struct additive_expression *ad;
		struct relational_expression *rl;
		struct equality_expression *eq;
		struct logical_and_exp *land;
		struct logical_or_exp *lor;
		struct logical_xor_exp *lxor;
		struct constant_expression *ce;
		struct assignment_expression *ae;
		struct expression *ex;
		struct declaration *dn;
		struct decln_spec *ds;
		struct init_declr_list *idrl;
		struct init_declr *idr;
		struct storage_spec *ss;
		struct type_spec *t;
		struct game_elem *g;
		struct attr_spec *as;
		struct attr_val_list *avl;
		struct attr_const *atc;
		struct record_spec *rs;
		struct record_decln_list *rdnl;
		struct record_decln *rdn;
		struct spec_qual_list *sql;
		struct record_declr_list *rdl;
		struct record_declr *rd;
		struct catalog_spec *cs;
		struct catalog_list *cl;
		struct catalog_const *cc;
		struct type_qual *tq;
		struct declarator *d;
		struct parameter_type_list *ptl;
		struct parameter_list *pl;
		struct parameter_decln *pdn;
		struct identifier_list *il;
		struct initializer *izr;
		struct initializer_list *izl;
		struct statement *st;
		struct label_stmt *ls;
		struct expression_stmt *es;
		struct block_stmt *bs;
		struct decln_list_or_stmt_list *dlsl;
		struct selection_stmt *sl;
		struct iteration_stmt *i;
		struct jump_stmt *j;
		struct attribute_addition_spec *at;
		struct translation_unit *tu;
		struct elements *el;
		struct function_definition *f;
		struct procedure_definition *pd;
		struct term_or_nonterm *term_or_nonterm; // For add_node() 
								// in ast.c
		void *handle; // Generic one - Useful
		// especially, while free()ing!
		// -- And since all these within
		// this union are data pointers,
		// they must be of same size. Hence,
		// these can be accesed interchangeably
		// with absolutely no problem!
	} cg;
};	

struct symbol;
struct comp_type;

enum unified_types { EBasic, EComp, EFunc, ENone2, };
struct unified_type {
	int type;
	union {
		struct symbol *s;
		struct comp_type *c;
		struct func_type *f;
	} u;
};

bool add_node(struct cgdl_node *n);
void walk_ast(void);
void free_nodes(void);
		
enum lines_types { ELine1, ELine2, ELine3 };
struct lines {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct lines *l;
			struct translation_unit *t;
		} l1;
		struct {
			struct lines *l;
		} l2;
		struct {
			int dummy;
		} l3;
	} u;
};

enum identifier_types { EIdentifier1 };
struct identifier {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			char *i;
		} i1;
	} u;
};

enum constant_types { EConst1, EConst2, EConst3, EConst4, EConst5 };
struct constant {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			char *r;
		} c1;
		struct {
			char *s;
		} c2;
		struct {
			struct visibility_const *v;
		} c3;
		struct {
			bool b;
		} c4;
		struct {
			int dummy;
		} c5;						
	} u;
};

enum visibility_const_types { EVisibility1, EVisibility2, EVisibility3};
struct visibility_const {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
};	

enum primary_expression_types { EPrimary1, EPrimary2, EPrimary3 };
struct primary_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i;
		} p1;
		struct {
			struct constant *c;
		} p2;
		struct {
			struct expression *e;
		} p3;
	} u;
};
		
enum postfix_expression_types { EPostfix1, EPostfix2, EPostfix3,
				EPostfix4, EPostfix5, EPostfix6 };
struct postfix_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct primary_expression *p;
		} p1;
		struct {
			struct postfix_expression *p;
			struct expression *e;
		} p2;
		struct {
			struct postfix_expression *p;
			struct expression *e1;
			struct expression *e2;
		} p3;
		struct {
			struct postfix_expression *p;
		} p4;
		struct {
			struct postfix_expression *p;
			struct arg_exp_list *a;
		} p5;
		struct {
			struct postfix_expression *p;
			struct identifier *i;
		} p6;
	} u;
};

enum arg_exp_list_types { EArg1, EArg2 };
struct arg_exp_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct assignment_expression *as;
		} a1;
		struct {
			struct arg_exp_list *al; 
			struct assignment_expression *as;
		} a2;
	} u; 
};

enum unary_expression_types { EUnary1, EUnary2 };
struct unary_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct postfix_expression *p;
		} u1;
		struct {
			struct unary_operator *uo;
			struct unary_expression *ue;
		} u2;
	} u;
};

enum unary_operator_types { EUnary_op1, EUnary_op2, EUnary_op3 };
struct unary_operator {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
};

enum multiplicative_expression_types { EMul1, EMul2, EMul3, EMul4 };
struct multiplicative_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct unary_expression *u;
		} m1;
		struct {
			struct multiplicative_expression *m;
			struct unary_expression *u;
		} m2;
		struct {
			struct multiplicative_expression *m;
			struct unary_expression *u;
		} m3;
		struct {
			struct multiplicative_expression *m;
			struct unary_expression *u;
		} m4;
	} u;
};

enum additive_expression_types { EAdd1, EAdd2, EAdd3 };
struct additive_expression {
        int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
        union {
                struct {
			struct multiplicative_expression* m;
		} a1;
                struct {
			struct additive_expression *a;
                        struct multiplicative_expression *m;
		} a2;
		struct {
			struct additive_expression *a;
			struct multiplicative_expression *m;
		} a3;
        } u;
};

enum relational_expression_types { ERel1, ERel2, ERel3, ERel4, ERel5 };
struct relational_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct additive_expression *a;
		} r1;
		struct {
			struct relational_expression *r;
			struct additive_expression *a;
		} r2;
		struct {
			struct relational_expression *r;
			struct additive_expression *a;
		} r3;
		struct {
			struct relational_expression *r;
			struct additive_expression *a;
		} r4;
		struct {
			struct relational_expression *r;
			struct additive_expression *a;
		} r5;
	} u;
};

enum equality_expression_types { EEqual1, EEqual2, EEqual3 };
struct equality_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct relational_expression *r;
		} e1;
		struct {
			struct equality_expression *e;
			struct relational_expression *r;
		} e2;
		struct {
			struct equality_expression *e;
			struct relational_expression *r;
		} e3;
        } u;
};

enum logical_and_exp_types { ELand1, ELand2 };
struct logical_and_exp {
        int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
        union {
                struct {
			struct equality_expression *e;
		} l1;
                struct {
			struct logical_and_exp *l;
                        struct equality_expression *e;
		} l2;
        } u;
};

enum logical_or_exp_types { ELor1, ELor2 };
struct logical_or_exp {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct logical_and_exp *a;
		} l1;
		struct {
			struct logical_or_exp *o;
			struct logical_and_exp *a;
		} l2;
	} u;
};

enum logical_xor_expression { ELxor1, ELxor2 };
struct logical_xor_exp {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct logical_or_exp *o;
		} l1;
		struct {
			struct logical_xor_exp *x;
			struct logical_or_exp *o;
		} l2;
	} u;
};

enum assignment_expression_types { EAssign1, EAssign2 };
struct assignment_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct logical_xor_exp *x;
		} a1;
		struct {
			struct unary_expression *u;
			struct assignment_expression *a;
		} a2;
	} u;
};

enum expression_types { EExp1, EExp2 };
struct expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct assignment_expression *a;
		} e1;
		struct {
			struct expression *e;
			struct assignment_expression *a;
		} e2;
	} u;
};

enum constant_expression_types { EConst_exp1 };
struct constant_expression {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct logical_xor_exp *x;
		} c1;
	} u;
};

enum declaration_types { EDeclaration1, EDeclaration2 };
struct declaration {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct decln_spec *d;
		} d1;
		struct {
			struct decln_spec *d;
			struct init_declr_list *i;
		} d2;
	} u;
};

enum decln_spec_types { EDecln_spec1, EDecln_spec2, EDecln_spec3,
			EDecln_spec4, EDecln_spec5, EDecln_spec6,
			EDecln_spec7, EDecln_spec8 };
struct decln_spec {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct storage_spec *s;
		} d1;
		struct {
			struct storage_spec *s;
			struct decln_spec *d;
		} d2;
		struct {
			struct type_spec *t;
		} d3;
		struct {
			struct type_spec *t;
		} d4;
		struct {
			struct type_spec *t;
			struct expression *e;
		} d5;
		struct {
			struct type_spec *t;
			struct decln_spec *d;
		} d6;
		struct {
			struct type_qual *t;
		} d7;
		struct {
			struct type_qual *t;
			struct decln_spec *d;
		} d8;
	} u;
};

enum init_declr_list_types { EInit_declr_list1, EInit_declr_list2 };
struct init_declr_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct init_declr *i;
		} i1;
		struct {
			struct init_declr_list *d;
			struct init_declr *i;
		} i2;
	} u;
};

enum init_declr_types { EInit_declr1, EInit_declr2 };
struct init_declr {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct declarator *d;
		} i1;
		struct {
			struct declarator *d;
			struct initializer *i;
		} i2;
	} u;
};

enum storage_spec_types { EStorage1 };
struct storage_spec {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
};

enum type_spec_types { EType1, EType2, EType3, EType4, EType5, EType6,
			EType7, EType8, EType9, EType10, EType11, EType12 };
struct type_spec {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			unsigned char dummy;	
		} t1;
		struct {
			unsigned char dummy;
		} t2;
		struct {
			unsigned char dummy;
		} t3;
		struct {
			unsigned char dummy;
		} t4;
		struct {
			unsigned char dummy;
		} t5;
		struct {
			unsigned char dummy;
		} t6;
		struct {
			unsigned char dummy;
		} t7;
		struct {
			unsigned char dummy;
		} t8;
		struct {
			struct attr_spec *a;
		} t9;
		struct {
			struct record_spec *r;
		} t10;
		struct {
			struct catalog_spec *c;
		} t11;
		struct {
			unsigned char dummy;
		} t12;
	} u;
};
		
enum game_elem_types { EGame1, EGame2 };
struct game_elem {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
};

enum attr_spec_types { EAttr1 };
struct attr_spec {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i;
		 	struct attr_val_list *a;
		} a1;
	} u;
};

enum attr_val_list_types { EAttr_val_list1, EAttr_val_list2 };
struct attr_val_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct attr_const *a;
		} a1;
		struct {
			struct attr_val_list *l;
			struct attr_const *a;
		} a2;
	} u;
};

enum attr_const_types { EAttr_const1 };
struct attr_const {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct constant_expression *c;
		} a1;
	} u;
};

enum record_spec_types { ERecord_spec1, ERecord_spec2 };
struct record_spec {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i;
			struct record_decln_list *r;
		} r1;
		struct {
			struct identifier *i;
		} r2;
	} u;
};

enum record_decln_list_types { ERecord_decln_list1,
				ERecord_decln_list2 };
struct record_decln_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct record_decln *r;
		} r1;
		struct {
			struct record_decln_list *r1;
			struct record_decln *r2;
		} r2;
	} u;
};

enum record_decln_types { ERecord1 };
struct record_decln {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct spec_qual_list *s;
			struct record_declr_list *r;
		} r1;

	} u;
};

enum spec_qual_list_types { ESpec_qual_list1, ESpec_qual_list2,
			    ESpec_qual_list3, ESpec_qual_list4};
struct spec_qual_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct type_spec *t;
			struct spec_qual_list *s;
		} s1;
		struct {
			struct type_spec *t;
		} s2;
		struct {
			struct type_qual *t;
			struct spec_qual_list *s;
		} s3;
		struct {
			struct type_qual *t;
		} s4;
	} u;
};

enum record_declr_list_types { ERecord_declr_list1,
				ERecord_declr_list2 };	
struct record_declr_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct record_declr *r;
		} r1;
		struct {
			struct record_declr_list *r1;
			struct record_declr *r2;
		} r2;
	} u;
};

enum record_declr_types { ERecord_declr1 };
struct record_declr {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct declarator *d;
		} r1;
	} u;
};

enum catalog_spec_types { ECatalog_spec1, ECatalog_spec2,
				ECatalog_spec3 };
struct catalog_spec {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct catalog_list *c;
		} c1;
		struct {
			struct identifier *i;
			struct catalog_list *c;
		} c2;
		struct {
			struct identifier *i;
		} c3;
	} u;
};

enum catalog_list_types { ECatalog_list1, ECatalog_list2 };
struct catalog_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct catalog_const *c;
		} c1;
		struct {
			struct catalog_list *c1;
			struct catalog_const *c2;
		} c2;
	} u;
};

enum catalog_const_types { ECatalog_const1, ECatalog_const2 };
struct catalog_const {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i;
		} c1;
		struct {
			struct identifier *i;
			struct constant_expression *c;
		} c2;
	} u;
};

enum type_qual_types { EType_qual1 };
struct type_qual {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
};

enum declarator_types { EDecl1, EDecl2, EDecl3, EDecl4, EDecl5 };
struct declarator {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i;
		} d1;
		struct {
			struct declarator *d;
		} d2;
		struct {
			struct declarator *d;
			struct parameter_type_list *p;
		} d3;
		struct {
			struct declarator *d;
			struct identifier_list *i;
		} d4;
		struct {
			struct declarator *d;
		} d5;
	} u;
};	

enum parameter_type_list_types { EParameter_type_list1 };
struct parameter_type_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct parameter_list *p;
		} p1;
	} u;
};

enum parameter_list_types { EParameter_list1, EParameter_list2 };
struct parameter_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct parameter_decln *p;
		} p1;
		struct {
			struct parameter_list *p1;
			struct parameter_decln *p2;
		} p2;
	} u;
};

enum parameter_decln_types { EParameter_decln1, EParameter_decln2 };
struct parameter_decln {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct decln_spec *d1;
			struct declarator *d2;
		} p1;
		struct {
			struct decln_spec *d;
		} p2;
	} u;
};

enum identifier_list_types { EIdentifier_list1, EIdentifier_list2 };
struct identifier_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i;
		} i1;
		struct {
			struct identifier_list *i1;
			struct identifier *i2;
		} i2;
	} u;
};

enum initializer_types { EInitializer1, EInitializer2 };
struct initializer {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct assignment_expression *e;
		} i1;
		struct {
			struct initializer_list *i;
		} i2;
	} u;
};

enum initializer_list_types { EInitializer_list1, EInitializer_list2 };
struct initializer_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct initializer *i;
		} i1;
		struct {
			struct initializer_list *i1;
			struct initializer *i2;
		} i2;
	} u;
};

enum statement_types { EStatement1, EStatement2, EStatement3,
			EStatement4, EStatement5, EStatement6, };
struct statement {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct expression_stmt *e;
		} s1;
		struct {
			struct block_stmt *b;
		} s2;
		struct {
			struct selection_stmt *s;
		} s3;
		struct {
			struct label_stmt *l;
		} s4;
		struct {
			struct iteration_stmt *i;
		} s5;
		struct {
			struct jump_stmt *j;
		} s6;
	} u;
};

enum label_stmt_types { ELabel1, ELabel2 };
struct label_stmt {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct constant_expression *c;
			struct statement *s;
		} l1;
		struct {
			struct statement *s;
		} l2;
	} u;
};

enum expression_stmt_types { EExp_stmt1, EExp_stmt2 };
struct expression_stmt {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			int dummy;
		} e1;
		struct {
			struct expression *e;
		} e2;
	} u;
};

enum block_stmt_types { EBlk1, EBlk2, EBlk3, EBlk4 };
struct block_stmt {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct decln_list_or_stmt_list *ds;
		} b1;
		struct {
			struct game_elem *g;
			struct decln_list_or_stmt_list *ds;
		} b2;
		struct {
			int dummy;
		} b3;
		struct {
			struct game_elem *g;
		} b4;
	} u;
};

enum decl_list_or_stmt_list_types { EDecl_list_stmt_list1,
					EDecl_list_stmt_list2,	
					EDecl_list_stmt_list3,
					EDecl_list_stmt_list4 };
struct decln_list_or_stmt_list {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct declaration *d;
		} d1;
		struct {
			struct decln_list_or_stmt_list *d1;
			struct declaration *d2;
		} d2;
		struct {
			struct statement *s;
		} d3;
		struct {
			struct decln_list_or_stmt_list *d;
			struct statement *s;
		} d4;
	} u;
};

enum selection_stmt_types { ESelection1, ESelection2, ESelection3 };
struct selection_stmt {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct expression *e;
			struct statement *s;
		} s1;
		struct {
			struct expression *e;
			struct statement *s1;
			struct statement *s2;
		} s2;
		struct {
			struct expression *e;
			struct statement *s;
		} s3;
	} u;
};
								
enum iteration_stmt_types { EIteration1, EIteration2, EIteration3,
				EIteration4, EIteration5 };
struct iteration_stmt {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct statement *s;
			struct expression *e;
		} i1;
		struct {
			struct identifier *i;
			struct statement *s;
		} i2;
		struct {
			struct identifier *i1;
			struct identifier *i2;
			struct statement *s;
		} i3;
		struct {
			struct identifier *i1;
			struct constant_expression *c;
			struct statement *s;
		} i4;
		struct {
			struct constant_expression *c;
			struct statement *s;
		} i5;
	} u;
};

enum jump_stmt_types { EJump1, EJump2, EJump3, EJump4, EJump5 };
struct jump_stmt {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			unsigned char dummy;
		} j1;
		struct {
			unsigned char dummy;
		} j2;
		struct {
			unsigned char dummy;
		} j3;
		struct {
			struct expression *e;
		} j4;
		struct {
			unsigned char dummy;
		} j5;
	} u;
};

enum attribute_addition_specifier_types { EAttr_add1, EAttr_add2 };
struct attribute_addition_spec {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i1;
			struct type_spec *t;
			struct identifier *i2;
		} a1;
		struct {
			struct identifier *i1;
			struct identifier *i2;
		} a2;
	} u;
};

enum translation_unit_types { ETranslation1, ETranslation2, };
struct translation_unit {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct elements *e;
		} t1;
		struct {
			struct translation_unit *t;
			struct elements *e;
		} t2;
	} u;
};

enum elements_types { EElements1, EElements2, EElements3, EElements4 };
struct elements {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct function_definition *f;
		} e1;
		struct {
			struct procedure_definition *p;
		} e2;
		struct {
			struct declaration *d;
		} e3;
		struct {
			struct attribute_addition_spec *a;
		} e4;
	} u;
};

enum function_definition_types { EFunction1 };
struct function_definition {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct decln_spec *d1;
			struct declarator *d2;
			struct block_stmt *b;
		} f1;
	} u;
};

enum procedure_definition_types { EProcedure1 };
struct procedure_definition {
	int type;
	char *indentation;
	unsigned long long int num_of_empty_lines;
	unsigned long long int src_line;
	struct unified_type *d_type;
#ifdef __DEBUG__
	unsigned long long int node_number;
#endif
	union {
		struct {
			struct identifier *i;
			struct block_stmt *b;
		} p1;
	} u;
};

#endif //_AST_H

