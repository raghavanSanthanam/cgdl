%{
#include <stdio.h>
#include <stdlib.h>
#ifdef __linux__
#include <linux/limits.h>  
#else
#include <limits.h>
#endif

#define __DEBUG__

#ifdef __DEBUG__
	#define printf(...) printf(__VA_ARGS__)
#else
	#define printf(...)
#endif

#ifdef __DEBUG__
static bool any_errors_so_far;
#endif

extern char *t;
FILE *op;
FILE *intrmd;
FILE *dynamic;

unsigned long long int src_line_num = 1;

char *indentation;
char *string_literal;
long long int num_of_empty_lines;

const char *src_file = "";
const char *scratch_file = "./__scratch__";
const char *dynamic_header = "../cpplib/dynamic.h";

char *enum_card_attr;
char *enum_player_attr;
char *enum_player_field;
char *enum_card_field;
char *enum_player_number;
char *enum_player_string;
char *enum_player_bool;
char *enum_card_number;
char *enum_card_string;
char *enum_card_bool;
char *new_attr_vec_push;
char *dynamic_definitions;
char *attr_enums;
int num_card_attr;
int num_player_attr;
int num_player_field;
int num_card_field;

extern FILE *yyin;

FILE *ip;

extern void combine_intermediate_files_to_form_src_and_exit(int exit_code);
%}

%%

\n			{
				src_line_num++;
				printf("@ LINE : %llu\n", src_line_num);
				num_of_empty_lines++;
				printf("num_of_empty_lines : %llu\n",
				num_of_empty_lines);

			}
\*\*.*\*\*       	{ printf("\n\nComment\n");
				yytext[ 0 ] = '/';
				yytext[ 1 ] = '/';
				yytext[ strlen(yytext) - 2 ] = '\0';
				fprintf(op, "%s\n", yytext); }
^[ \t]+                	{ printf("\n\nIndentation\n");
				indentation = strdup(yytext); }
[ \t]*			{ printf("\n\nIntermediate whitespace or "
					"trailing ones\n"); }
"all"                   { printf("\n\nall\n"); strcpy(t, yytext); return ALL; }
"self"                  { printf("\n\nself\n"); strcpy(t, yytext);
								return SELF; }
"noOne"                 { printf("\n\nnoOne\n"); strcpy(t, yytext);
								return NOONE; }
"or"                    { printf("\n\nor\n"); strcpy(t, yytext); return OR_OP; }
"and"                   { printf("\n\nand\n"); strcpy(t, yytext);
								return AND_OP; }
"xor"                   { printf("\n\nxor\n"); strcpy(t, yytext);
								return XOR_OP; }
">="                    { printf("\n\n>=\n"); strcpy(t, yytext); return GE_OP; }
"<="                    { printf("\n\n<=\n"); strcpy(t, yytext); return LE_OP; }
"=="                    { printf("\n\n==\n"); strcpy(t, yytext); return EQ_OP; }
"!="                    { printf("\n\n!=\n"); strcpy(t, yytext); return NE_OP; }
"set"			{ printf("\n\nset\n"); strcpy(t, yytext); return SET; }
"string"                { printf("\n\nstring\n"); strcpy(t, yytext);
								return STRING; }
"number"                { printf("\n\nnumber\n"); strcpy(t, yytext);
								return NUMBER;}
"none"                  { printf("\n\nnone\n"); strcpy(t, yytext);
								return NONE; }
"bool"                  { printf("\n\nbool\n"); strcpy(t, yytext);
							return BOOLEAN; }
"catalog"               { printf("\n\ncatalog\n"); strcpy(t, yytext);
							return CATALOG; }
"Game"			{ printf("\n\nGame\n"); strcpy(t, yytext); }
"Setup"			{ printf("\n\nSetup\n"); strcpy(t, yytext);
								return SETUP; }
"Round"			{ printf("\n\nRound\n"); strcpy(t, yytext);
								return ROUND; }
"forEach"               { printf("\n\nforEach\n"); strcpy(t, yytext);
							return FOREACH; }
"in"                    { printf("\n\nin\n"); strcpy(t, yytext);
					return IN; }
"case"			{ printf("\n\ncase\n"); strcpy(t, yytext);
					return CASE; }
"default"		{ printf("\n\ndefault\n"); strcpy(t, yytext);
							return DEFAULT; }
"fallThrough"           { printf("\n\nfallThrough\n"); strcpy(t, yytext);
							return FALLTHROUGH; }
"switch"                { printf("\n\nswitch\n"); strcpy(t, yytext);
								return SWITCH; }
"loop"                  { printf("\n\nloop\n"); strcpy(t, yytext);
					return LOOP; }
"break"                 { printf("\n\nbreak\n"); strcpy(t, yytext);
								return BREAK; }
"repeat"                { printf("\n\nrepeat\n"); strcpy(t, yytext);
								return REPEAT; }
"until"                 { printf("\n\nuntil\n"); strcpy(t, yytext);
								return UNTIL; }
"loopback"              { printf("\n\nloopback\n"); strcpy(t, yytext);
					return LOOPBACK; }
"if"                    { printf("\n\nif\n"); strcpy(t, yytext);
					return IF; }
"else"                  { printf("\n\nelse\n"); strcpy(t, yytext);
					return ELSE; }
"return"                { printf("\n\nreturn\n"); strcpy(t, yytext);
					return RETURN; }
"Player"                { printf("\n\nPlayer\n"); strcpy(t, yytext);
					return PLAYER; }
"Pile"			{ printf("\n\nPile\n"); strcpy(t, yytext);
					return PILE; }
"Card"                  { printf("\n\nCard\n"); strcpy(t, yytext);
					return CARD; }
"Rank"                  { printf("\n\nRank\n"); strcpy(t, yytext);
					return RANK; }
"Suit"			{ printf("\n\nSuit\n"); strcpy(t, yytext);
					return SUIT; }
"record"                { printf("\n\nrecord\n"); strcpy(t, yytext);
								return RECORD; }
"attribute"             { printf("\n\nattribute\n"); strcpy(t, yytext);
							return ATTRIBUTE; }
"null"                  { printf("\n\nnull\n"); strcpy(t, yytext);
							return NULL_CONSTANT; }
"visibility"		{ printf("\n\nvisibility\n"); strcpy(t, yytext);
							return VISIBILITY; }
"const"			{ printf("\n\nconst\n"); strcpy(t, yytext);
							return CONST; }
"~"                     { printf("\n\n~\n"); strcpy(t, yytext);
								return UMINUS; }
"true"|"false"          { printf("\n\n%s\n", yytext); strcpy(t, yytext);
						return BOOLEAN_CONSTANT; }
"global"                  { printf("\n\nglobal\n"); strcpy(t, yytext);
								return GLOBAL; }
[0-9]+(\.[0-9]+)?  { printf("\n\nNUMBER_CONSTANT: %s\n", yytext);
					strcpy(t, yytext);
                          		return NUMBER_CONSTANT; }
[_a-zA-Z]+[_a-zA-Z0-9]* { printf("\n\nID: %s\n", yytext);
					/* strcpy(t, yytext); -- Crashes
						due to buffer overflow! */
					return ID; }
\"[^\"]*\"		{ printf("\n\nSTRING_LITERAL: %s\n", yytext);
				// strcpy(t, yytext);  -- t is a pointer to
				// yylval. And yylval seems to be a small
				// buffer and it crashes when we copy relatively
				// longer string literals to it.
				//
				// Anyways, I don't think this can be of any
				// use either! So, let's have it commented.
			  string_literal = strdup(yytext);
			  printf("string_literal : %s\n", string_literal);
                          return STRING_LITERAL; }
.                       { printf("\n\nUnknown or newline: \"%s\"\n", yytext);
				strcpy(t, yytext);
			  	fflush(NULL);
                          return yytext[0];}

%%

char *t = (char *)&yylval;
char op_file[ PATH_MAX + 1 ] = "";

void combine_intermediate_files_to_form_src_and_exit(int exit_code)
{
#ifdef __DEBUG__
	if ( exit_code ) {
		any_errors_so_far = true;
		return; // To help uninterrupted debugging.
	}
#endif

	fflush(NULL);

	op = fopen(op_file, "r");
	if ( op ) {
		int ch = 0;
		while ( (ch = fgetc(op)) != EOF ) {
			fputc(ch, intrmd);
		}
	}

	fclose(op);
	fclose(intrmd);

	fputs("\n\nvoid addNewAttributes(void)\n"
		"{\n", dynamic);
	fputs(new_attr_vec_push, dynamic);
	new_attr_vec_push = NULL;
	fputs("\n}\n\n", dynamic);

	fputs("\n#endif // _DYNAMIC_H\n", dynamic);
	fclose(dynamic);
	printf("Removing : %s\n", op_file);
	remove(op_file);
	printf("Renaming : %s to %s\n", scratch_file, op_file);
	rename(scratch_file, op_file);

//#ifdef __DEBUG__
//	if ( !any_errors_so_far ) {
//#endif
		exit(exit_code); // One-time exit.
//#ifdef __DEBUG__
//	} else {
//		exit(-1); // Exiting finally in a debugging
//		// scenario after ignroing any intermediate
//		// errors.
//	}
//#endif
}

static inline void init_default_info(void)
{
        enum_card_attr = strdup("\nenum CardAttribute { ");
	// enum_card_field = strdup("\nenum CardField { ");
	// enum_player_attr = strdup("\nenum PlayerAttribute { ");
	enum_player_field = strdup("\nenum PlayerField { ");

	// enum_card_number = strdup("\nenum CardNumber { ");
	// enum_card_string = strdup("\nenum CardString { ");
	// enum_card_bool = strdup("\nenum CardBool { ");

	enum_player_number = strdup("\nenum PlayerNumber { ");
	enum_player_string = strdup("\nenum PlayerString { ");
	enum_player_bool = strdup("\nenum PlayerBool { ");

	new_attr_vec_push = strdup(""); // To help further realloc().
	dynamic_definitions = strdup("");

	attr_enums = strdup("");
}

int main(int argc, char *argv[])
{
	bool src_file_given = false;

	if ( argc > 1 && argv[ 1 ][ 0 ] != '<' ) { // Only if it's not a
							// redirection.
		src_file = argv[ 1 ];
		src_file_given = true;
	}

	if ( src_file_given ) {
		ip = fopen(src_file, "r");
		if ( !ip ) {
			fprintf(stderr, "%s is not found or inaccessible\n",
								src_file);
			exit(-1);
		}
	}

	if ( src_file_given ) {
		char *p = strstr(argv[ 1 ], ".cgdl");
		bool cpp_extn_present = !!p;
		if ( cpp_extn_present ) {
			strncpy(op_file, argv[ 1 ], p - argv[ 1 ]);
			strcat(op_file, ".cpp");
		} else {
			strcpy(op_file, argv[ 1 ]);
			strcat(op_file, "H");
		}
		op = fopen(op_file, "w");
	} else {
		strcpy(op_file, "./cgdl.cpp");
		op = fopen("./cgdl.cpp", "w");
	}		

	if ( !op )
	{
		fprintf(stderr, "Error: Failed to create the target C++ "
				"code\n");
		exit(-1);
	}

	intrmd = fopen(scratch_file, "w");
	if ( !intrmd ) {
		fprintf(stderr, "Error: Failed to create intermediate "
				"file\n");
		exit(-1);
	}

	dynamic = fopen(dynamic_header, "w");
	if ( !dynamic ) {
		fprintf(stderr, "Error: Failed to create intermediate "
				"file\n");
		fprintf(stderr, "cgdlc needs to be run from a folder which"
				" has the cgdl library folder(cpplib) inside"
				" it!\n");
		exit(-1);
	}

	fputs(	"#ifndef _DYNAMIC_H\n"
		"#define _DYNAMIC_H\n\n"
		"using namespace std;\n\n"
		"extern vector<string> arrayOfNames;\n"
		"extern vector<string> cardAttributeNames;\n"
		"extern vector<string> playerNumberFieldNames;\n"
		"extern vector<string> playerStringFieldNames;\n"
		"extern vector<string> playerBoolFieldNames;\n\n", dynamic);

	init_default_info();

	// Default headers
	fputs("#include <iostream>\n"
		"#include <cstdio>\n"
		"#include <cstdlib>\n"
		"#include <vector>\n"
		"#include <string>\n"
		"#include <algorithm>\n"
		"#include <cmath>\n"
		"#include <csignal>\n"
		"\n"
		"#include \"cgdl.h\"\n"
		"\n"
		"using namespace std;\n"
		"\n", intrmd);
	// Standard CGDL constants
	//fputs("int numPlayers;\n", intrmd);
	fputs("unsigned long long int SrcLine;\n", intrmd);
	fputs("\n\n// Game specific\n"
		"Player *currPlayer;\n"
		"Player *player;\n"
		"Card *card;\n\n", intrmd);

	fprintf(op, "\nvoid CrashHandler(int sig)"
			"\n{"
			"\n	cout << \"Crash when executing \""
			"\n	<< SrcLine << \" line in %s\" << endl;"
			"\n	exit(-1);"
			"\n}\n\n", src_file);

	(void)yyunput;
	(void)input;

	if ( ip ) {
		yyin = ip;
	}

	yyparse();

	fclose(ip);
	fclose(op);

	combine_intermediate_files_to_form_src_and_exit(0);

	return 0;
}
                          
