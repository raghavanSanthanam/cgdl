#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <csignal>

#include "cgdl.h"

using namespace std;

unsigned long long int SrcLine;


// Game specific
Player *currPlayer;
Player *player;
Card *card;


void CrashHandler(int sig)
{
	cout << "Crash when executing "
	<< SrcLine << " line in ../cpplib/cgdl_headers/attribute" << endl;
	exit(-1);
}

enum { NONE = (long double)0 };
enum StdRanks { ACE = (long double)1, TWO, THREE, FOUR, FIVE, SIX, SEVEN, 			EIGHT, NINE, TEN, JACK, QUEEN, KING } ;
