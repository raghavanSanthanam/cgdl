rm lex.yy.c
echo Building lexer . . .
lex ./cgdl.l
rm y.output
rm y.tab.c
echo Building parser . . .
yacc -vt ./cgdl.y
rm cgdlc
echo Building CGDL compiler . . .
#gcc -ansi -std=c99 -pedantic -g -Wall y.tab.c ast.c -ll -o ./cgdlc
gcc -ansi -pedantic -std=c99 -pedantic-errors -Wall -Wshadow -Wpointer-arith -Wwrite-strings -g y.tab.c semantic_actions.c ast_builder_walk_initiator.c symbol_table.c ast_translator.c -ll -o ./cgdlc
echo cgdlc is ready!                                                   
