#include "ast.h"
#include "semantic_actions.h"

#define __DEBUG__

#ifdef __DEBUG__
#define TOUCH()	do {\
			printf("%s() : %d\n", __func__, prod);\
		} while ( 0 )
#else
#define TOUCH()
#endif

void *lines_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ELines;
		c->cg.ln = malloc(sizeof(*c->cg.ln));
		if ( c->cg.ln ) {
			c->cg.ln->type = prod;			
			switch ( prod ) {
			case ELine1:
				c->cg.ln->u.l1.l = (struct lines *)d1;
				c->cg.ln->u.l1.t =
					(struct translation_unit *)d2;
				break;
			case ELine2:
				c->cg.ln->u.l2.l = (struct lines *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.ln;
			}			
		} else {
			free(c);
		}
	}

	return dd;
}

void *identifier_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	char *yytext_p = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EIdentifier;
		c->cg.id = malloc(sizeof(*c->cg.id));
		if ( c->cg.id ) {
			c->cg.id->type = prod;
			switch ( prod ) {
			case EIdentifier1:
				c->cg.id->u.i1.i = strdup(yytext_p);
				if ( !c->cg.id->u.i1.i ) {
					printf("Identifier failed to "
								"replicate!\n");
					printf("yytext : %s\n", yytext_p);
					abort();
				}
				break;
			}	
			if ( add_node(c) ) {
				dd = c->cg.id;
			}
		} else {
			free(c);
		}
	}	

	return dd;
}


void *constant_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	char *yytext_p = a1;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EConstant;
		c->cg.c = malloc(sizeof(*c->cg.c));
		if ( c->cg.c ) {
			c->cg.c->type = prod;
			switch ( prod ) {
			case EConst1:
				c->cg.c->u.c1.r = strdup(yytext_p);
				break;
			case EConst2:
				c->cg.c->u.c2.s = string_literal;
				printf("%s - string_literal : %s\n",
						__FILE__, string_literal);				
				break;
			case EConst3:
				c->cg.c->u.c3.v =
				(struct visibility_const *)d1;	 
				break;		
			case EConst4:
				c->cg.c->u.c4.b = !strcmp(yytext_p, "true");
				break;
			case EConst5:
				c->cg.c->u.c5.dummy = 0;	
				break;								
			}
			if ( add_node(c) ) {
				dd = c->cg.c;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *visibility_const_semantic_actions(int prod)
{
	TOUCH();

	void *dd = NULL;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EVisibility;
		c->cg.v = malloc(sizeof(*c->cg.v));
		if ( c->cg.v ) {
			c->cg.v->type = prod;
			switch ( prod ) {
			case EVisibility1:
			case EVisibility2:
			case EVisibility3:
					break; // Just for symmetry!
			}
			if ( add_node(c) ) {
				dd = c->cg.v;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *primary_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EPrimary;
		c->cg.pr = malloc(sizeof(*c->cg.pr));
		if ( c->cg.pr ) {
			c->cg.pr->type = prod;
			switch ( prod ) {
			case EPrimary1:
				c->cg.pr->u.p1.i =
					(struct identifier *)d1;
				break;
			case EPrimary2:
				c->cg.pr->u.p2.c =
					(struct constant *)d1;
				break;				
			case EPrimary3:
				c->cg.pr->u.p3.e =
					(struct expression *)d2;
				break;
			}				
			if ( add_node(c) ) {
				dd = c->cg.pr;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *postfix_semantic_actions(int prod, void *a1, void *a2, void *a3)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;
	void *d5 = a3;
	
	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EPostfix;
		c->cg.po = malloc(sizeof(*c->cg.po));
		if ( c->cg.po ) {
			c->cg.po->type = prod;
			switch ( prod ) {
			case EPostfix1:
				c->cg.po->u.p1.p =
					(struct primary_expression *)d1;
				break;
			case EPostfix2:
				c->cg.po->u.p2.p =
					(struct postfix_expression *)d1;
				c->cg.po->u.p2.e =
					(struct expression *)d3;
				break;
			case EPostfix3:
				c->cg.po->u.p3.p =
					(struct postfix_expression *)d1;
				c->cg.po->u.p3.e1 =
					(struct expression *)d3;
				c->cg.po->u.p3.e2 =
					(struct expression *)d5;
				break;
			case EPostfix4:
				c->cg.po->u.p4.p =
					(struct postfix_expression *)d1;
				break;
			case EPostfix5:
				c->cg.po->u.p5.p =
					(struct postfix_expression *)d1;
				c->cg.po->u.p5.a =
					(struct arg_exp_list *)d3;
				break;
			case EPostfix6:
				c->cg.po->u.p6.p =
					(struct postfix_expression *)d1;
				c->cg.po->u.p6.i =
					(struct identifier *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.po;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *arg_exp_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EArg_exp_list;
		c->cg.ar = malloc(sizeof(*c->cg.ar));
		if ( c->cg.ar ) {
			c->cg.ar->type = prod;
			switch ( prod ) {
			case EArg1:
				c->cg.ar->u.a1.as =
				(struct assignment_expression *)d1;
				break;
			case EArg2:
				c->cg.ar->u.a2.al =
					(struct arg_exp_list *)d1;
				c->cg.ar->u.a2.as =
				(struct assignment_expression *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.ar;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *unary_expression_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EUnary_exp;
		c->cg.un = malloc(sizeof(*c->cg.un));
		if ( c->cg.un ) {
			c->cg.un->type = prod;
			switch ( prod ) {
			case EUnary1:
				c->cg.un->u.u1.p =
				(struct postfix_expression *)d1;
				break;
			case EUnary2:
				c->cg.un->u.u2.uo =
					(struct unary_operator *)d1;
				c->cg.un->u.u2.ue =
					(struct unary_expression *)d2;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.un;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *unary_operator_semantic_actions(int prod)
{
	TOUCH();

	void *dd = NULL;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EUnary_op;
		c->cg.uo = malloc(sizeof(*c->cg.uo));
		if ( c->cg.uo ) {
			c->cg.uo->type = prod;
			switch ( prod ) {
			case EUnary_op1:
			case EUnary_op2:
			case EUnary_op3:
					break; // Just for symmetry!
			}
			if ( add_node(c) ) {
				dd = c->cg.uo;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *multiplicative_expression_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EMultiplicative_exp;
		c->cg.me = malloc(sizeof(*c->cg.me));
		if ( c->cg.me ) {
			c->cg.me->type = prod;
			switch ( prod ) {
			case EMul1:
				c->cg.me->u.m1.u =
					(struct unary_expression *)d1;
				break;
			case EMul2:
				c->cg.me->u.m2.m =
				(struct multiplicative_expression *)d1;
				c->cg.me->u.m2.u =
				(struct unary_expression *)d3;
				break;
			case EMul3:
				c->cg.me->u.m3.m =
				(struct multiplicative_expression *)d1;
				c->cg.me->u.m3.u =
				(struct unary_expression *)d3;
				break;
			case EMul4:
				c->cg.me->u.m4.m =
				(struct multiplicative_expression *)d1;
				c->cg.me->u.m4.u =
				(struct unary_expression *)d3;
				break;
			}									
			if ( add_node(c) ) {
				dd = c->cg.me;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *additive_expression_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EAdditive_exp;
		c->cg.ad = malloc(sizeof(*c->cg.ad));
		if ( c->cg.ad ) {
			c->cg.ad->type = prod;
			switch ( prod ) {
			case EAdd1:
				c->cg.ad->u.a1.m =
				(struct multiplicative_expression *)d1;
				break;
			case EAdd2:
				c->cg.ad->u.a2.a =
				(struct additive_expression *)d1;
				c->cg.ad->u.a2.m =
				(struct multiplicative_expression *)d3;
				break;
			case EAdd3:
				c->cg.ad->u.a3.a =
				(struct additive_expression *)d1;
				c->cg.ad->u.a3.m =
				(struct multiplicative_expression *)d3;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.ad;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *relational_expression_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ERelational_exp;
		c->cg.rl = malloc(sizeof(*c->cg.rl));
		if ( c->cg.rl ) {
			c->cg.rl->type = prod;
			switch ( prod ) {
			case ERel1:
				c->cg.rl->u.r1.a =
				(struct additive_expression *)d1;
				break;
			case ERel2:
				c->cg.rl->u.r2.r =
				(struct relational_expression *)d1;
				c->cg.rl->u.r2.a =
				(struct additive_expression *)d3;
				break;
			case ERel3:
				c->cg.rl->u.r3.r =
				(struct relational_expression *)d1;
				c->cg.rl->u.r3.a =
				(struct additive_expression *)d3;
				break;
			case ERel4:
				c->cg.rl->u.r4.r =
				(struct relational_expression *)d1;
				c->cg.rl->u.r4.a =
				(struct additive_expression *)d3;
				break;
			case ERel5:
				c->cg.rl->u.r5.r =
				(struct relational_expression *)d1;
				c->cg.rl->u.r5.a =
				(struct additive_expression *)d3;
				break;
			}													
			if ( add_node(c) ) {
				dd = c->cg.rl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *equality_expression_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EEquality_exp;
		c->cg.eq = malloc(sizeof(*c->cg.eq));
		if ( c->cg.eq ) {
			c->cg.eq->type = prod;
			switch ( prod ) {
			case EEqual1:
				c->cg.eq->u.e1.r =
				(struct relational_expression *)d1;
				break;
			case EEqual2:
				c->cg.eq->u.e2.e =
				(struct equality_expression *)d1;
				c->cg.eq->u.e2.r =
				(struct relational_expression *)d3;
				break;
			case EEqual3:
				c->cg.eq->u.e2.e =
				(struct equality_expression *)d1;
				c->cg.eq->u.e2.r =
				(struct relational_expression *)d3;
				break;			
			}			
			if ( add_node(c) ) {
				dd = c->cg.eq;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *logical_and_exp_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ELogical_and_exp;
		c->cg.land = malloc(sizeof(*c->cg.land));
		if ( c->cg.land ) {
			c->cg.land->type = prod;
			switch ( prod ) {
			case ELand1:
				c->cg.land->u.l1.e =
				(struct equality_expression *)d1;
				break;
			case ELand2:
				c->cg.land->u.l2.l =
				(struct logical_and_exp *)d1;
				c->cg.land->u.l2.e =
				(struct equality_expression *)d3;
				break;
			}				
			if ( add_node(c) ) {
				dd = c->cg.land;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *logical_or_exp_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ELogical_or_exp;
		c->cg.lor = malloc(sizeof(*c->cg.lor));
		if ( c->cg.lor ) {
			c->cg.lor->type = prod;
			switch ( prod ) {
			case ELor1:
				c->cg.lor->u.l1.a =
					(struct logical_and_exp *)d1;
				break;
			case ELor2:
				c->cg.lor->u.l2.o =
				(struct logical_or_exp *)d1;
				c->cg.lor->u.l2.a =
				(struct logical_and_exp *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.lor;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *logical_xor_exp_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ELogical_xor_exp;
		c->cg.lxor = malloc(sizeof(*c->cg.lxor));
		if ( c->cg.lxor ) {
			c->cg.lxor->type = prod;
			switch ( prod ) {
			case ELxor1:
				c->cg.lxor->u.l1.o =
					(struct logical_or_exp *)d1;
				break;
			case ELxor2:
				c->cg.lxor->u.l2.x =
					(struct logical_xor_exp *)d1;
				c->cg.lxor->u.l2.o =
					(struct logical_or_exp *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.lxor;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *assignment_expression_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EAssign_exp;
		c->cg.ae = malloc(sizeof(*c->cg.ae));
		if ( c->cg.ae ) {
			c->cg.ae->type = prod;
			switch ( prod ) {
			case EAssign1:
				c->cg.ae->u.a1.x =
					(struct logical_xor_exp *)d1;
				break;
			case EAssign2:
				c->cg.ae->u.a2.u =
					(struct unary_expression *)d1;
				c->cg.ae->u.a2.a =
				(struct assignment_expression *)d3;
				break;
			}				
			if ( add_node(c) ) {
				dd = c->cg.ae;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *expression_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EExp;
		c->cg.ex = malloc(sizeof(*c->cg.ex));
		if ( c->cg.ex ) {
			c->cg.ex->type = prod;
			switch ( prod ) {
			case EExp1:
				c->cg.ex->u.e1.a =
				(struct assignment_expression *)d1;
				break;
			case EExp2:
				c->cg.ex->u.e2.e =
				(struct expression *)d1;
				c->cg.ex->u.e2.a =
				(struct assignment_expression *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.ex;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *constant_expression_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EConst_exp;
		c->cg.ce = malloc(sizeof(*c->cg.ce));
		if ( c->cg.ce ) {
			c->cg.ce->type = prod;
			switch ( prod ) {
			case EConst_exp1:
				c->cg.ce->u.c1.x =
					(struct logical_xor_exp *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.ce;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *declaration_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EDeclaration;
		c->cg.dn = malloc(sizeof(*c->cg.dn));
		if ( c->cg.dn ) {
			c->cg.dn->type = prod;
			switch ( prod ) {
			case EDeclaration1:
				c->cg.dn->u.d1.d =
					(struct decln_spec *)d1;
				break;
			case EDeclaration2:
				c->cg.dn->u.d2.d =
					(struct decln_spec *)d1;
				c->cg.dn->u.d2.i =
					(struct init_declr_list *)d2;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.dn;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *decln_spec_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EDeclaration_spec;
		c->cg.ds = malloc(sizeof(*c->cg.ds));
		if ( c->cg.ds ) {
			c->cg.ds->type = prod;
			switch ( prod ) {
			case EDecln_spec1:
				c->cg.ds->u.d1.s =
					(struct storage_spec *)d1;
				break;
			case EDecln_spec2:
				c->cg.ds->u.d2.s =
					(struct storage_spec *)d1;
				c->cg.ds->u.d2.d =
					(struct decln_spec *)d2;
				break;
			case EDecln_spec3:
				c->cg.ds->u.d3.t =
					(struct type_spec *)d1;
				break;
			case EDecln_spec4:
				c->cg.ds->u.d4.t =
					(struct type_spec *)d1;
				break;
			case EDecln_spec5:
				c->cg.ds->u.d5.t =
					(struct type_spec *)d1;
				c->cg.ds->u.d5.e =
					(struct expression *)d3;
				break;
			case EDecln_spec6:
				c->cg.ds->u.d6.t =
					(struct type_spec *)d1;
				c->cg.ds->u.d6.d =
					(struct decln_spec *)d2;
				break;
			case EDecln_spec7:
				c->cg.ds->u.d7.t =
					(struct type_qual *)d1;
				break;
			case EDecln_spec8:
				c->cg.ds->u.d8.t =
					(struct type_qual *)d1;
				c->cg.ds->u.d8.d =
					(struct decln_spec *)d2;
				break;			
			}			
			if ( add_node(c) ) {
				dd = c->cg.ds;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *init_declr_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EInit_declr_list;
		c->cg.idrl = malloc(sizeof(*c->cg.idrl));
		if ( c->cg.idrl ) {
			c->cg.idrl->type = prod;
			switch ( prod ) {
			case EInit_declr_list1:
				c->cg.idrl->u.i1.i =
					(struct init_declr *)d1;
				break;
			case EInit_declr_list2:
				c->cg.idrl->u.i2.d =
					(struct init_declr_list *)d1;
				c->cg.idrl->u.i2.i =
					(struct init_declr *)d3;
				break;
			} 		
			if ( add_node(c) ) {
				dd = c->cg.idrl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *init_declr_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EInit_declr;
		c->cg.idr = malloc(sizeof(*c->cg.idr));
		if ( c->cg.idr ) {
			c->cg.idr->type = prod;
			switch ( prod ) {
			case EInit_declr1:
				c->cg.idr->u.i1.d =
					(struct declarator *)d1;
				break;
			case EInit_declr2:
				c->cg.idr->u.i2.d =
					(struct declarator *)d1;
				c->cg.idr->u.i2.i = (struct initializer *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.idr;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *storage_spec_semantic_actions(int prod)
{
	TOUCH();

	void *dd = NULL;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EStorage_spec;
		c->cg.ss = malloc(sizeof(*c->cg.ss));
		if ( c->cg.ss ) {
			c->cg.ss->type = prod;
			switch ( prod ) {
			case EStorage1:
					break; // Just for symmetry!
			}			
			if ( add_node(c) ) {
				dd = c->cg.ss;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *type_spec_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	
	struct cgdl_node * c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EType_spec;
		c->cg.t = malloc(sizeof(*c->cg.t));
		if ( c->cg.t ) {
			c->cg.t->type = prod;
			switch ( prod ) {
			case EType1:
			case EType2:
			case EType3:
			case EType4:
			case EType5:
			case EType6:
			case EType7:
			case EType8:
			case EType12:
					break; // Just for symmetry!
			case EType9:
				c->cg.t->u.t9.a = d1;
				break;
			case EType10:
				c->cg.t->u.t10.r = d1;
				break;
			case EType11:
				c->cg.t->u.t11.c = d1;
				break;
			}										
			if ( add_node(c) ) {
				dd = c->cg.t;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *game_elem_semantic_actions(int prod)
{
	TOUCH();

	void *dd = NULL;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EGame_elem;
		c->cg.g = malloc(sizeof(*c->cg.g));
		if ( c->cg.g ) {
			c->cg.g->type = prod;
			switch ( prod ) {
			case EGame1:
			case EGame2:
					break; // Just for symmetry!
			}
			if ( add_node(c) ) {
				dd = c->cg.g;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *attr_spec_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d2 = a1;
	void *d4 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EAttr_spec;
		c->cg.as = malloc(sizeof(*c->cg.as));
		if ( c->cg.as ) {
			c->cg.as->type = prod;
			switch ( prod ) {
			case EAttr1:
				c->cg.as->u.a1.i =
					(struct identifier *)d2;
				c->cg.as->u.a1.a =
					(struct attr_val_list *)d4;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.as;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *attr_val_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EAttr_val_list;
		c->cg.avl = malloc(sizeof(*c->cg.avl));
		if ( c->cg.avl ) {
			c->cg.avl->type = prod;
			switch ( prod ) {
			case EAttr_val_list1:
				c->cg.avl->u.a1.a =
					(struct attr_const *)d1;
				break;
			case EAttr_val_list2:
				c->cg.avl->u.a2.l =
					(struct attr_val_list *)d1;
				c->cg.avl->u.a2.a =
					(struct attr_const *)d3;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.avl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *attr_const_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EAttr_const;
		c->cg.atc = malloc(sizeof(*c->cg.atc));
		if ( c->cg.atc ) {
			c->cg.atc->type = prod;
			switch ( prod ) {
			case EAttr_const1:
				c->cg.atc->u.a1.c =
				(struct constant_expression *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.atc;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *record_spec_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d2 = a1;
	void *d4 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ERecord_spec;
		c->cg.rs = malloc(sizeof(*c->cg.rs));
		if ( c->cg.rs ) {
			c->cg.rs->type = prod;
			switch ( prod ) {
			case ERecord_spec1:
				c->cg.rs->u.r1.i =
					(struct identifier *)d2;
				c->cg.rs->u.r1.r =
					(struct record_decln_list *)d4;
				break;
			case ERecord_spec2:
				c->cg.rs->u.r1.i =
					(struct identifier *)d2;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.rs;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *record_decln_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ERecord_decln_list;
		c->cg.rdnl = malloc(sizeof(*c->cg.rdnl));
		if ( c->cg.rdnl ) {
			c->cg.rdnl->type = prod;
			switch ( prod ) {
			case ERecord_decln_list1:
				c->cg.rdnl->u.r1.r =
					(struct record_decln *)d1;
				break;
			case ERecord_decln_list2:
				c->cg.rdnl->u.r2.r1 =
					(struct record_decln_list *)d1;
				c->cg.rdnl->u.r2.r2 =
					(struct record_decln *)d2;
				break;
			}		
			if ( add_node(c) ) {
				dd = c->cg.rdnl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *record_decln_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ERecord_decln;
		c->cg.rdn = malloc(sizeof(*c->cg.rdn));
		if ( c->cg.rdn ) {
			c->cg.rdn->type = prod;
			switch ( prod ) {
			case ERecord1:
				c->cg.rdn->u.r1.s =
					(struct spec_qual_list *)d1;
				c->cg.rdn->u.r1.r =
					(struct record_declr_list *)d2;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.rdn;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *spec_qual_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ESpec_qual_list;
		c->cg.sql = malloc(sizeof(*c->cg.sql));
		if ( c->cg.sql ) {
			c->cg.sql->type = prod;
			switch ( prod ) {
			case ESpec_qual_list1:
				c->cg.sql->u.s1.t =
					(struct type_spec *)d1;
				c->cg.sql->u.s1.s =
					(struct spec_qual_list *)d2;
				break;
			case ESpec_qual_list2:
				c->cg.sql->u.s2.t =
					(struct type_spec *)d1;
				break;
			case ESpec_qual_list3:
				c->cg.sql->u.s3.t =
					(struct type_qual *)d1;
				c->cg.sql->u.s3.s =
					(struct spec_qual_list *)d2;
				break;
			case ESpec_qual_list4:
				c->cg.sql->u.s4.t =
					(struct type_qual *)d1;
				break;
			}									
			if ( add_node(c) ) {
				dd = c->cg.sql;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *record_declr_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ERecord_declr_list;
		c->cg.rdl = malloc(sizeof(*c->cg.rdl));
		if ( c->cg.rdl ) {
			c->cg.rdl->type = prod;
			switch ( prod ) {
			case ERecord_declr_list1:
				c->cg.rdl->u.r1.r =
					(struct record_declr *)d1;
				break;
			case ERecord_declr_list2:
				c->cg.rdl->u.r2.r1 =
					(struct record_declr_list *)d1;
				c->cg.rdl->u.r2.r2 =
					(struct record_declr *)d3;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.rdl;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *record_declr_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ERecord_declr;
		c->cg.rd = malloc(sizeof(*c->cg.rd));
		if ( c->cg.rd ) {
			c->cg.rd->type = prod;
			switch ( prod ) {
			case ERecord_declr1:
				c->cg.rd->u.r1.d =
					(struct declarator *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.rd;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *catalog_spec_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d2 = a1;
	void *d3 = a1;
	void *d4 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ECatalog_spec;
		c->cg.cs = malloc(sizeof(*c->cg.cs));
		if ( c->cg.cs ) {
			c->cg.cs->type = prod;
			switch ( prod ) {
			case ECatalog_spec1:
				c->cg.cs->u.c1.c =
					(struct catalog_list *)d3;
				break;
			case ECatalog_spec2:
				c->cg.cs->u.c2.i =
					(struct identifier *)d2;
				c->cg.cs->u.c2.c =
					(struct catalog_list *)d4;
				break;
			case ECatalog_spec3:
				c->cg.cs->u.c3.i =
					(struct identifier *)d2;
				break;			
			}			
			if ( add_node(c) ) {
				dd = c->cg.cs;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *catalog_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ECatalog_list;
		c->cg.cl = malloc(sizeof(*c->cg.cl));
		if ( c->cg.cl ) {
			c->cg.cl->type = prod;
			switch ( prod ) {
			case ECatalog_list1:
				c->cg.cl->u.c1.c =
					(struct catalog_const *)d1;
				break;
			case ECatalog_list2:
				c->cg.cl->u.c2.c1 =
					(struct catalog_list *)d1;
				c->cg.cl->u.c2.c2 =
					(struct catalog_const *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.cl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *catalog_const_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ECatalog_const;
		c->cg.cc = malloc(sizeof(*c->cg.cc));
		if ( c->cg.cc ) {
			c->cg.cc->type = prod;
			switch ( prod ) {
			case ECatalog_const1:
				c->cg.cc->u.c1.i =
					(struct identifier *)d1;
				break;
			case ECatalog_const2:
				c->cg.cc->u.c2.i =
					(struct identifier *)d1;
				c->cg.cc->u.c2.c =
				(struct constant_expression *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.cc;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *type_qual_semantic_actions(int prod)
{
	TOUCH();

	void *dd = NULL;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EType_qual;
		c->cg.t = malloc(sizeof(*c->cg.t));
		if ( c->cg.t ) {
			c->cg.cc->type = prod;
			switch ( prod ) {
			case EType_qual1:
					break; // Just for symmetry!
			}
			if ( add_node(c) ) {
				dd = c->cg.t;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *declarator_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EDeclarator;
		c->cg.d = malloc(sizeof(*c->cg.d));
		if ( c->cg.d ) {
			c->cg.d->type = prod;
			switch ( prod ) {
			case EDecl1:
				c->cg.d->u.d1.i =
					(struct identifier *)d1;
				break;
			case EDecl2:
				c->cg.d->u.d2.d =
					(struct declarator *)d2;
				break;
			case EDecl3:
				c->cg.d->u.d3.d =
					(struct declarator *)d1;
				c->cg.d->u.d3.p =
				(struct parameter_type_list *)d3;
				break;
			case EDecl4:
				c->cg.d->u.d4.d =
					(struct declarator *)d1;
				c->cg.d->u.d4.i =
					(struct identifier_list *)d3;
				break;
			case EDecl5:
				c->cg.d->u.d5.d =
					(struct declarator *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.d;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *parameter_type_list_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EParameter_type_list;
		c->cg.ptl = malloc(sizeof(*c->cg.ptl));
		if ( c->cg.ptl ) {
			c->cg.ptl->type = prod;
			switch ( prod ) {
			case EParameter_type_list1:
				c->cg.ptl->u.p1.p =
					(struct parameter_list *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.ptl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *parameter_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EParameter_list;
		c->cg.pl = malloc(sizeof(*c->cg.pl));
		if ( c->cg.pl ) {
			c->cg.pl->type = prod;
			switch ( prod ) {
			case EParameter_list1:
				c->cg.pl->u.p1.p =
					(struct parameter_decln *)d1;
				break;
			case EParameter_list2:
				c->cg.pl->u.p2.p1 =
					(struct parameter_list *)d1;
				c->cg.pl->u.p2.p2 =
					(struct parameter_decln *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.pl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *parameter_decln_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EParameter_decln;
		c->cg.pdn = malloc(sizeof(*c->cg.pdn));
		if ( c->cg.pdn ) {
			c->cg.pdn->type = prod;
			switch ( prod ) {
			case EParameter_decln1:
				c->cg.pdn->u.p1.d1 =
					(struct decln_spec *)d1;
				c->cg.pdn->u.p1.d2 =
					(struct declarator *)d2;
				break;
			case EParameter_decln2:
				c->cg.pdn->u.p2.d =
					(struct decln_spec *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.pdn;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *identifier_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EIdentifier_list;
		c->cg.il = malloc(sizeof(*c->cg.il));
		if ( c->cg.il ) {
			c->cg.il->type = prod;
			switch ( prod ) {
			case EIdentifier_list1:
				c->cg.il->u.i1.i =
					(struct identifier *)d1;
				break;
			case EIdentifier_list2:
				c->cg.il->u.i2.i1 =
					(struct identifier_list *)d1;
				c->cg.il->u.i2.i2 =
					(struct identifier *)d3;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.il;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *initializer_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EInitializer;
		c->cg.izr = malloc(sizeof(*c->cg.izr));

		if ( c->cg.izr ) {
			c->cg.izr->type = prod;
			switch ( prod ) {
			case EInitializer1:
				c->cg.izr->u.i1.e =
				(struct assignment_expression *)d1;
				break;
			case EInitializer2:
				c->cg.izr->u.i2.i =
					(struct initializer_list *)d2;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.izr;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *initializer_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EInitializer_list;
		c->cg.izl = malloc(sizeof(*c->cg.izl));
		if ( c->cg.izl ) {
			c->cg.izl->type = prod;
			switch ( prod ) {
			case EInitializer_list1:
				c->cg.izl->u.i1.i =
					(struct initializer *)d1;
				break;
			case EInitializer_list2:
				c->cg.izl->u.i2.i1 =
					(struct initializer_list *)d1;
				c->cg.izl->u.i2.i2 =
					(struct initializer *)d3;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.izl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *statement_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EStatement;
		c->cg.st = malloc(sizeof(*c->cg.st));
		if ( c->cg.st ) {
			c->cg.st->type = prod;
			switch ( prod ) {
			case EStatement1:
				c->cg.st->u.s1.e =
					(struct expression_stmt *)d1;
				break;
			case EStatement2:
				c->cg.st->u.s2.b =
					(struct block_stmt *)d1;
				break;			
			case EStatement3:
				c->cg.st->u.s3.s =
					(struct selection_stmt *)d1;
				break;			
			case EStatement4:
				c->cg.st->u.s4.l =
					(struct label_stmt *)d1;
				break;			
			case EStatement5:
				c->cg.st->u.s5.i =
					(struct iteration_stmt *)d1;
				break;			
			case EStatement6:
				c->cg.st->u.s6.j =
					(struct jump_stmt *)d1;
				break;			
			}
			if ( add_node(c) ) {
				dd = c->cg.st;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *label_stmt_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d2 = a1;
	void *d3 = a1;
	void *d4 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ELabel_stmt;
		c->cg.ls = malloc(sizeof(*c->cg.ls));
		if ( c->cg.ls ) {
			c->cg.ls->type = prod;
			switch ( prod ) {
			case ELabel1:
				c->cg.ls->u.l1.c =
				(struct constant_expression *)d2;
				c->cg.ls->u.l1.s =
				(struct statement *)d4;
				break;
			case ELabel2:
				c->cg.ls->u.l2.s =
					(struct statement *)d3;
				break;
			}				
			if ( add_node(c) ) {
				dd = c->cg.ls;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *expression_stmt_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EExp_stmt;
		c->cg.es = malloc(sizeof(*c->cg.es));
		if ( c->cg.es ) {
			c->cg.es->type = prod;
			switch ( prod ) {
			case EExp_stmt1:
				c->cg.es->u.e1.dummy = 0;			
				break;
			case EExp_stmt2:
				c->cg.es->u.e2.e =
					(struct expression *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.es;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *block_stmt_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a1;
	void *d3 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EBlock_stmt;
		c->cg.bs = malloc(sizeof(*c->cg.bs));
		if ( c->cg.bs ) {
			c->cg.bs->type = prod;
			switch ( prod ) {
			case EBlk1:
				c->cg.bs->u.b1.ds =
				(struct decln_list_or_stmt_list *)d2;
				break;
			case EBlk2:
				c->cg.bs->u.b2.g =
					(struct game_elem *)d1;
				c->cg.bs->u.b2.ds =
				(struct decln_list_or_stmt_list *)d3;
				break;
			case EBlk3:
				c->cg.bs->u.b3.dummy = 0;
				break;
			case EBlk4:
				c->cg.bs->u.b4.g =
					(struct game_elem *)d1;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.bs;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *decl_list_or_stmt_list_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EDecl_list_stmt_list;
		c->cg.dlsl = malloc(sizeof(*c->cg.dlsl));
		if ( c->cg.dlsl ) {
			c->cg.dlsl->type = prod;
			switch ( prod ) {
			case EDecl_list_stmt_list1:
				c->cg.dlsl->u.d1.d =
					(struct declaration *)d1;
				break;
			case EDecl_list_stmt_list2:
				c->cg.dlsl->u.d2.d1 =
				(struct decln_list_or_stmt_list *)d1;
				c->cg.dlsl->u.d2.d2 =
						(struct declaration *)d2;
				break;
			case EDecl_list_stmt_list3:
				c->cg.dlsl->u.d3.s = (struct statement *)d1;
				break;
			case EDecl_list_stmt_list4:
				c->cg.dlsl->u.d4.d =
				(struct decln_list_or_stmt_list *)d1;
				c->cg.dlsl->u.d4.s =
						(struct statement *)d2;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.dlsl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *selection_stmt_semantic_actions(int prod, void *a1, void *a2, void *a3)
{
	TOUCH();

	void *dd = NULL;
	void *d3 = a1;
	void *d5 = a2;
	void *d7 = a3;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ESelection_stmt;
		c->cg.sl = malloc(sizeof(*c->cg.sl));
		if ( c->cg.sl ) {
			c->cg.sl->type = prod;
			switch ( prod ) {
			case ESelection1:
				c->cg.sl->u.s1.e =
					(struct expression *)d3;
				c->cg.sl->u.s1.s =
					(struct statement *)d5;
				break;
			case ESelection2:
				c->cg.sl->u.s2.e =
					(struct expression *)d3;
				c->cg.sl->u.s2.s1 =
					(struct statement *)d5;
				c->cg.sl->u.s2.s2 =
					(struct statement *)d7;
				break;
			case ESelection3:
				c->cg.sl->u.s3.e =
					(struct expression *)d3;
				c->cg.sl->u.s3.s =
					(struct statement *)d5;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.sl;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *iteration_stmt_semantic_actions(int prod, void *a1, void *a2, void *a3)
{
	TOUCH();

	void *dd = NULL;
	void *d2 = a1;
	void *d3 = a2;
	void *d4 = a2;
	void *d5_1 = a2;
	void *d5_2 = a3;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EIteration_stmt;
		c->cg.i = malloc(sizeof(*c->cg.i));
		if ( c->cg.i ) {
			c->cg.i->type = prod;
			switch ( prod ) {
			case EIteration1:
				c->cg.i->u.i1.s =
					(struct statement *)d2;
				c->cg.i->u.i1.e =
					(struct expression *)d5_1;
				break;
			case EIteration2:
				c->cg.i->u.i2.i =
					(struct identifier *)d2;
				c->cg.i->u.i2.s =
					(struct statement *)d3;
				break;			
			case EIteration3:
				c->cg.i->u.i3.i1 =
					(struct identifier *)d2;
				c->cg.i->u.i3.i2 =
					(struct identifier *)d4;
				c->cg.i->u.i3.s =
					(struct statement *)d5_2;
				break;			
			case EIteration4:
				c->cg.i->u.i4.i1 =
					(struct identifier *)d2;
				c->cg.i->u.i4.c =
				(struct constant_expression *)d4;
				c->cg.i->u.i4.s =
					(struct statement *)d5_2;
				break;			
			case EIteration5:
				c->cg.i->u.i5.c =
				(struct constant_expression *)d2;
				c->cg.i->u.i5.s =
					(struct statement *)d3;
				break;			
			}
			if ( add_node(c) ) {
				dd = c->cg.i;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *jump_stmt_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d2 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EJump_stmt;
		c->cg.j = malloc(sizeof(*c->cg.j));
		if ( c->cg.j ) {
			c->cg.j->type = prod;
			switch ( prod ) {
			case EJump1:
			case EJump2:
			case EJump3:
				break; // Just for symmetry!
			case EJump4:
				c->cg.j->u.j4.e = d2;
				break;			
			case EJump5:
				break; // Just for symmetry!	
			}
			if ( add_node(c) ) {
				dd = c->cg.j;
			}
		} else {
			free(c);
		}
	}
	
	return dd;
}

void *attribute_addition_spec_semantic_actions(int prod, void *a1, void *a2,
								void *a3)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;
	void *d4 = a3;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EAttr_add_spec;
		c->cg.at = malloc(sizeof(*c->cg.at));
		if ( c->cg.at ) {
			c->cg.at->type = prod;
			switch ( prod ) {
			case EAttr_add1:
				c->cg.at->u.a1.i1 = (struct identifier *)d1;
				c->cg.at->u.a1.t = (struct type_spec *)d2;
				c->cg.at->u.a1.i2 = (struct identifier *)d4;
				break;
			case EAttr_add2:
				c->cg.at->u.a2.i1 = (struct identifier *)d1;
				c->cg.at->u.a2.i2 = (struct identifier *)d2;
				break;
			}			
			if ( add_node(c) ) {
				dd = c->cg.at;
			}
		} else {
			free(c);
		}
	}
		
	return dd;
}
								
void *translation_unit_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = ETranslation_unit;
		c->cg.tu = malloc(sizeof(*c->cg.tu));
		if ( c->cg.tu ) {
			c->cg.tu->type = prod;
			switch ( prod ) {
			case ETranslation1:
				c->cg.tu->u.t1.e =
					(struct elements *)d1;
				printf("%d : trans_unit : %p\n", __LINE__,
								(void *)d1);
				break;
			case ETranslation2:
				c->cg.tu->u.t2.t =
					(struct translation_unit *)d1;
				printf("%d : trans_unit : %p\n", __LINE__,
								(void *)d1);
				c->cg.tu->u.t2.e =
					(struct elements *)d2;
				break;
			}
			if ( add_node(c) ) {
				dd = NULL; // Below walk_ast() will free this
						// node anyway and hence it
						// shouldn't be returned to
						// the parent node of this
						// non-terminal.
				walk_ast();
			}
		} else {
			free(c);
		}
	}

	return dd; // Just for symmetry!
}

void *elements_semantic_actions(int prod, void *a1)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EElements;
		c->cg.el = malloc(sizeof(*c->cg.el));
		if ( c->cg.el ) {
			c->cg.el->type = prod;
			switch ( prod ) {
			case EElements1:
				c->cg.el->u.e1.f =
				(struct function_definition *)d1;
				break;
			case EElements2:
				c->cg.el->u.e2.p =
				(struct procedure_definition *)d1;
				break;
			case EElements3:
				c->cg.el->u.e3.d =
					(struct declaration *)d1;
				break;
			case EElements4:
				c->cg.el->u.e4.a = 
					(struct attribute_addition_spec *)d1;
				break;
			}						
			if ( add_node(c) ) {
				dd = c->cg.el;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *function_definition_semantic_actions(int prod, void *a1, void *a2,
								void *a3)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;
	void *d3 = a3;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EFunction_def;
		c->cg.f = malloc(sizeof(*c->cg.f));
		if ( c->cg.f ) {
			c->cg.f->type = prod;
			switch ( prod ) {
			case EFunction1:
				c->cg.f->u.f1.d1 =
					(struct decln_spec *)d1;
				c->cg.f->u.f1.d2 =
					(struct declarator *)d2;
				c->cg.f->u.f1.b =
					(struct block_stmt *)d3;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.f;
			}
		} else {
			free(c);
		}
	}

	return dd;	
}

void *procedure_definition_semantic_actions(int prod, void *a1, void *a2)
{
	TOUCH();

	void *dd = NULL;
	void *d1 = a1;
	void *d2 = a2;

	struct cgdl_node *c = malloc(sizeof(*c));
	if ( c ) {
		c->type = EProcedure_def;
		c->cg.pd = malloc(sizeof(*c->cg.pd));
		if ( c->cg.pd ) {
			c->cg.pd->type = prod;
			switch ( prod ) {
			case EProcedure1:
				c->cg.pd->u.p1.i =
					(struct identifier *)d1;
				c->cg.pd->u.p1.b =
					(struct block_stmt *)d2;
				break;
			}
			if ( add_node(c) ) {
				dd = c->cg.pd;
			}
		} else {
			free(c);
		}
	}

	return dd;
}

void *(*semantic_actions[ ENumber_of_non_terminals ])() = {
	lines_semantic_actions, identifier_semantic_actions,
	constant_semantic_actions, visibility_const_semantic_actions,
	primary_semantic_actions, postfix_semantic_actions,
	arg_exp_list_semantic_actions, unary_expression_semantic_actions,
	unary_operator_semantic_actions,
	multiplicative_expression_semantic_actions,
	additive_expression_semantic_actions,
	relational_expression_semantic_actions,
	equality_expression_semantic_actions, logical_and_exp_semantic_actions,
	logical_or_exp_semantic_actions, logical_xor_exp_semantic_actions,
	constant_expression_semantic_actions,
	assignment_expression_semantic_actions, expression_semantic_actions,
	declaration_semantic_actions, decln_spec_semantic_actions,
	init_declr_list_semantic_actions, init_declr_semantic_actions,
	storage_spec_semantic_actions, type_spec_semantic_actions,
	game_elem_semantic_actions, attr_spec_semantic_actions,
	attr_val_list_semantic_actions, attr_const_semantic_actions,
	record_spec_semantic_actions, record_decln_list_semantic_actions,
	record_decln_semantic_actions, spec_qual_list_semantic_actions,
	record_declr_list_semantic_actions, record_declr_semantic_actions,
	catalog_spec_semantic_actions, catalog_list_semantic_actions,
	catalog_const_semantic_actions, type_qual_semantic_actions,
	declarator_semantic_actions, parameter_type_list_semantic_actions,
	parameter_list_semantic_actions, parameter_decln_semantic_actions,
	identifier_list_semantic_actions, initializer_semantic_actions,
	initializer_list_semantic_actions, statement_semantic_actions,
	label_stmt_semantic_actions, expression_stmt_semantic_actions,
	block_stmt_semantic_actions, decl_list_or_stmt_list_semantic_actions,
	selection_stmt_semantic_actions, iteration_stmt_semantic_actions,
	jump_stmt_semantic_actions, attribute_addition_spec_semantic_actions,
	translation_unit_semantic_actions, elements_semantic_actions,
	function_definition_semantic_actions,
	procedure_definition_semantic_actions,
};

